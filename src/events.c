#include <X11/Xlib.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/sync.h>

#include <glib.h>

#include "display.h"
#include "window.h"

typedef void (*LmcEventFunc) (XEvent *xev, gpointer user_data);

typedef struct 
{
  GSource source;
  
  Display *display;
  GPollFD event_poll_fd;
} LmcEventSource;

static gboolean  
event_prepare (GSource  *source,
	       gint     *timeout)
{
  Display *display = ((LmcEventSource*)source)->display;
  
  *timeout = -1;

  return XPending (display);
}

static gboolean  
event_check (GSource *source) 
{
  LmcEventSource *display_source = (LmcEventSource*)source;
  gboolean retval;

  if (display_source->event_poll_fd.revents & G_IO_IN)
    retval = XPending (display_source->display);
  else
    retval = FALSE;

  return retval;
}

static gboolean  
event_dispatch (GSource    *source,
		GSourceFunc callback,
		gpointer    user_data)
{
  Display *display = ((LmcEventSource*)source)->display;
  LmcEventFunc event_func = (LmcEventFunc) callback;
  
  XEvent xev;

  if (XPending (display))
    {
      XNextEvent (display, &xev);

      if (event_func)
	(*event_func) (&xev, user_data);
    }

  return TRUE;
}

static const GSourceFuncs event_funcs = {
  event_prepare,
  event_check,
  event_dispatch,
  NULL
};

static GSource *
lmc_event_source_new (Display *display)
{
  int connection_number = ConnectionNumber (display);
  
  GSource *source = g_source_new ((GSourceFuncs *)&event_funcs, sizeof (LmcEventSource));
  LmcEventSource *display_source = (LmcEventSource *)source;

  display_source->event_poll_fd.fd = connection_number;
  display_source->event_poll_fd.events = G_IO_IN;
  
  display_source->display = display;
  
  g_source_add_poll (source, &display_source->event_poll_fd);
  g_source_set_can_recurse (source, TRUE);

  return source;
}

static void
dispatch_window_event (XEvent  *xevent,
		       gpointer data)
{
  LmcDisplay *ldisplay = data;
  Window window = None;
  Window secondary_window = None;
  const char *detail = NULL;
  LmcWindow *lwindow = NULL;

  /* Core events all have an event->xany.window field, but that's
   * not true for extension events
   */
  if (xevent->type >= KeyPress && xevent->type <= MappingNotify)
    {
      window = secondary_window = xevent->xany.window;
    }
  
  switch (xevent->type)
    {
    case KeyPress:
      detail = "KeyPress";
      break;
    case KeyRelease:
      detail = "KeyRelease";
      break;
    case ButtonPress:
      detail = "ButtonPress";
      break;
    case ButtonRelease:
      detail = "ButtonRelease";
      break;
    case MotionNotify:
      detail = "MotionNotify";
      break;
    case EnterNotify:
      detail = "EnterNotify";
      break;
    case LeaveNotify:
      detail = "LeaveNotify";
      break;
    case FocusIn:
      detail = "FocusIn";
      break;
    case FocusOut:
      detail = "FocusOut";
      break;
    case KeymapNotify:
      detail = "KeymapNotify";
      break;
    case Expose:
      detail = "Expose";
      break;
    case GraphicsExpose:
      detail = "GraphicsExpose";
      break;
    case NoExpose:
      detail = "NoExpose";
      break;
    case VisibilityNotify:
      detail = "VisibilityNotify";
      break;
    case CreateNotify:
      window = xevent->xcreatewindow.window;
      detail = "CreateNotify";
      break;
    case DestroyNotify:
      window = xevent->xdestroywindow.window;
      detail = "DestroyNotify";
      break;
    case UnmapNotify:
      window = xevent->xunmap.window;
      detail = "UnmapNotify";
      break;
    case MapNotify:
      window = xevent->xmap.window;
      detail = "MapNotify";
      break;
    case MapRequest:
      window = xevent->xmaprequest.window;
      detail = "MapRequest";
      break;
    case ReparentNotify:
      window = xevent->xreparent.window;
      detail = "ReparentNotify";
      break;
    case ConfigureNotify:
      window = xevent->xconfigure.window;
      detail = "ConfigureNotify";
      break;
    case ConfigureRequest:
      window = xevent->xconfigurerequest.window;
      detail = "ConfigureRequest";
      break; 
    case GravityNotify:
      window = xevent->xgravity.window;
      detail = "GravityNotify";
      break;
    case ResizeRequest:
      detail = "ResizeRequest";
      break;
    case CirculateNotify:
      window = xevent->xcirculate.window;
      detail = "CirculateNotify";
      break;
    case CirculateRequest:
      window = xevent->xcirculaterequest.window;
      detail = "CirculateRequest";
      break;
    case PropertyNotify:
      detail = "PropertyNotify";
      break;
    case SelectionClear:
      detail = "SelectionClear";
      break; 
    case SelectionRequest:
      detail = "SelectionRequest";
      break;
    case SelectionNotify:
      detail = "SelectionNotify";
      break;
    case ColormapNotify:
      detail = "ColormapNotify";
      break;
    case ClientMessage:
      detail = "ClientMessage";
      break;
    case MappingNotify:
      detail = "MappingNotify";
      break;
    default:
      if (xevent->type == ldisplay->damage_event_base + XDamageNotify)
	{
	  detail = "DamageNotify";
	  window = ((XDamageNotifyEvent *)xevent)->drawable;
	}
      else if (xevent->type == ldisplay->xfixes_event_base + XFixesSelectionNotify)
	{
	  detail = "FixesSelectionNotify";
	  window = ((XFixesSelectionNotifyEvent *)xevent)->window;
	}
      else if (xevent->type == ldisplay->xfixes_event_base + XFixesCursorNotify)
	{
	  detail = "FixesCursorNotify";
	  window = ((XFixesCursorNotifyEvent *)xevent)->window;
	}
      else if (xevent->type == ldisplay->sync_event_base + XSyncAlarmNotify)
	{
	  detail = "SyncAlarmNotify";
	}
      break;
    }

  if (window)
    {
      lwindow = lmc_display_lookup (ldisplay, window);
      if (!lwindow)
	lwindow = lmc_display_lookup (ldisplay, secondary_window);

      if (lwindow)
	lmc_window_do_event (lwindow, xevent, detail);
      
#if 0
  g_print ("%s %#lx %#lx => %#lx\n", detail, window, secondary_window,
	   lwindow ? lwindow->xwindow : 0);
#endif  
    }
  else if (detail)
    {
      /* This is specifically for MappingNotify events
       */
      lmc_display_do_event (ldisplay, xevent, detail);
    }
}

void
lmc_display_attach (LmcDisplay   *ldisplay,
		    GMainContext *context)
{
  GSource *source;
  
  g_return_if_fail (ldisplay->main_context == NULL);

  if (!context)
    context = g_main_context_default ();

  g_main_context_ref (context);
  ldisplay->main_context = context;
  source = lmc_event_source_new (ldisplay->xdisplay);
  g_source_set_callback (source, (GSourceFunc) dispatch_window_event, ldisplay, NULL);
  g_source_attach (source, context);
  g_source_unref (source);
}
