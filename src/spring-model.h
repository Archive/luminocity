#ifndef __SPRING_MODEL_H__
#define __SPRING_MODEL_H__

#include "toplevel.h"
#include "gui.h"

G_BEGIN_DECLS

void
lmc_wobble_setup (LmcToplevel *toplevel,
		  LmcGui      *gui);

G_END_DECLS

#endif /* __SPRING_MODEL_H__ */
