#ifndef __LMC_TOPLEVEL_H__
#define __LMC_TOPLEVEL_H__

#include <X11/Xutil.h>

#include "bits.h"
#include "window.h"

G_BEGIN_DECLS

#define LMC_TYPE_TOPLEVEL            (lmc_toplevel_get_type ())
#define LMC_TOPLEVEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_TOPLEVEL, LmcToplevel))
#define LMC_TOPLEVEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_TOPLEVEL, LmcToplevelClass))
#define LMC_IS_TOPLEVEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_TOPLEVEL))
#define LMC_IS_TOPLEVEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_TOPLEVEL))
#define LMC_TOPLEVEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_TOPLEVEL, LmcToplevelClass))

typedef struct _LmcToplevel       LmcToplevel;

GType	    lmc_toplevel_get_type  (void) G_GNUC_CONST;

LmcToplevel *lmc_toplevel_new (LmcDisplay *display,
			       Window      xwindow);

LmcBits     *lmc_toplevel_get_bits     (LmcToplevel *toplevel);

gboolean lmc_toplevel_is_mapped            (LmcToplevel *toplevel);
gboolean lmc_toplevel_is_override_redirect (LmcToplevel *toplevel);
gboolean lmc_toplevel_is_resizable         (LmcToplevel *toplevel);

void lmc_toplevel_get_position     (LmcToplevel *toplevel,
				    int         *x,
				    int         *y);
void lmc_toplevel_set_position     (LmcToplevel *toplevel,
				    int          x,
				    int          y);
void lmc_toplevel_get_set_position (LmcToplevel *toplevel,
				    int         *x,
				    int         *y);
void lmc_toplevel_get_size       (LmcToplevel *toplevel,
				  int         *width,
				  int         *height);
void lmc_toplevel_set_size       (LmcToplevel *toplevel,
				  int          width,
				  int          height);
void lmc_toplevel_get_set_size   (LmcToplevel *toplevel,
				  int         *width,
				  int         *height);
void lmc_toplevel_constrain_size (LmcToplevel *toplevel,
				  int          width,
				  int          height,
				  int         *new_width,
				  int         *new_height);
void lmc_toplevel_set_border_info (LmcToplevel         *toplevel,
				   const LmcBorderInfo *border_info);
void lmc_toplevel_get_border_info (LmcToplevel         *toplevel,
				   LmcBorderInfo       *border_info);

void
lmc_toplevel_get_mouse_position (LmcToplevel *toplevel,
				 int         *mouse_x,
				 int         *mouse_y);

void lmc_toplevel_move_resize (LmcToplevel *toplevel,
			       int          x,
			       int          y,
			       int          width,
			       int          height);
gboolean lmc_toplevel_get_fullscreen (LmcToplevel *toplevel);
void lmc_toplevel_set_fullscreen (LmcToplevel *toplevel,
				  gboolean fullscreen);

void         lmc_toplevel_map          (LmcToplevel *toplevel);
void         lmc_toplevel_raise        (LmcToplevel *toplevel);
void         lmc_toplevel_lower        (LmcToplevel *toplevel);
gboolean     lmc_toplevel_grab_focus   (LmcToplevel *toplevel);
void         lmc_toplevel_delete       (LmcToplevel *toplevel);

void lmc_toplevel_send_configure_notify (LmcToplevel *toplevel);

const XWMHints *  lmc_toplevel_get_wm_hints        (LmcToplevel *toplevel);
const XSizeHints *lmc_toplevel_get_wm_normal_hints (LmcToplevel *toplevel);
const gchar *     lmc_toplevel_get_wm_name         (LmcToplevel *toplevel);
gboolean          lmc_toplevel_has_protocol        (LmcToplevel *toplevel,
						    Atom         protocol);

G_END_DECLS

#endif /* __LMC_TOPLEVEL_H__ */
