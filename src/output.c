/*
 * GLX setup based on glxgears:
 * 
 * Copyright (C) 1999-2001  Brian Paul   All Rights Reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * BRIAN PAUL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>

#include "output.h"
#include "texture.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include "lmc-marshal.h"

typedef struct _LmcOutputClass LmcOutputClass;
typedef struct _LmcBreadCrumb LmcBreadCrumb;

struct _LmcOutput
{
  LmcWindow parent_instance;

  Colormap colormap;
  GLXContext context;

  int width;
  int height;
  Region invalid;
  GList *windows;

  LmcOutputViewport *main_viewport;
  GList *viewports;

  guint destroyed : 1;

  GThread *thread;
  GMutex *mutex;
  GCond *idle_cond;
  gboolean idle;
  gint freeze_count;

  LmcTexture *background_texture;

  LmcTexture *cursor_texture;
  int cursor_x;
  int cursor_y;

  XRectangle pager_rect;
  LmcTexture *pager_texture;
  LmcBorderInfo pager_border;

  GSList *to_delete;
  GSList *to_update;

  XRectangle visible;
  
  GQueue *bread_crumbs;

  gboolean show_pager;
};

struct _LmcBreadCrumb
{
  LmcOutputBreadCrumbFunc func;
  gpointer		  data;
};

struct _LmcOutputClass
{
  LmcWindowClass parent_class;
};

struct _LmcOutputViewport
{
  LmcOutput *output;
  
  int src_x, src_y, src_width, src_height;
  int dest_x, dest_y, dest_width, dest_height;

  double bg_red, bg_green, bg_blue;
};

struct _LmcOutputWindow
{
  LmcOutput *output;
  
  LmcTexture *texture;
  double alpha;
  int x, y, width, height;

  gpointer user_data;
    
  Region update;
};

/**********************************************************************/

static guint delete_window_signal;

G_DEFINE_TYPE (LmcOutput, lmc_output, LMC_TYPE_WINDOW)

#define LOCK(output) { g_mutex_lock (output->mutex); }
#define UNLOCK(output) { g_mutex_unlock (output->mutex); }

static void
lmc_output_finalize (GObject *object)
{
  LmcOutput *output = LMC_OUTPUT (object);
  
  g_error ("Finalization of LmcOutput not yet implemented");
  
  XDestroyRegion (output->invalid);

  G_OBJECT_CLASS (lmc_output_parent_class)->finalize (object);
}

static void
lmc_output_class_init (LmcOutputClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_output_finalize;
  
  delete_window_signal =  g_signal_new ("delete-window",
					G_OBJECT_CLASS_TYPE (object_class),
					G_SIGNAL_RUN_LAST,
					0,
					NULL, NULL,
					lmc_marshal_NONE__NONE,
					G_TYPE_NONE, 0);
}

static void
lmc_output_init (LmcOutput *output)
{
  output->bread_crumbs = g_queue_new ();
}

gboolean
make_window (Display    *xdisplay,
	     int         screen,
	     const char *name,
	     int         width,
	     int         height,
	     Window     *window,
	     Colormap   *colormap,
	     GLXContext *context)
{
   int attrib[] = { GLX_RGBA,
                   GLX_RED_SIZE, 1,
                   GLX_GREEN_SIZE, 1,
                   GLX_BLUE_SIZE, 1,
                   GLX_DOUBLEBUFFER,
                   GLX_DEPTH_SIZE, 1,
                   None };
   XSetWindowAttributes attr;
   unsigned long mask;
   Window root;
   XVisualInfo *visinfo;

   root = RootWindow (xdisplay, screen);

   visinfo = glXChooseVisual( xdisplay, screen, attrib );
   if (!visinfo)
     {
       g_printerr ("Error: couldn't get an RGB, Double-buffered visual.\n");
       return False;
     }

   attr.background_pixel = 0;
   attr.border_pixel = 0;

   *colormap = attr.colormap = XCreateColormap (xdisplay, root, visinfo->visual, AllocNone);
   attr.event_mask = StructureNotifyMask | ExposureMask | KeyPressMask;
   mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;

   *window = XCreateWindow (xdisplay, root, 0, 0, width, height,
			    0, visinfo->depth, InputOutput,
			    visinfo->visual, mask, &attr);

   {
     XSizeHints size_hints;
     Atom protocol = XInternAtom (xdisplay, "WM_DELETE_WINDOW", False);
     int screen_width = WidthOfScreen (ScreenOfDisplay (xdisplay, screen));
     int screen_height = HeightOfScreen (ScreenOfDisplay (xdisplay, screen));
     gboolean fullscreen = width == screen_width && height == screen_height;

     /* If the window is as large as the screen, we make it resizable
      * partly because there is no room to deal with a non-resizable
      * window, and partly because Metacity won't let us full-screen
      * a non-resizable window
      */
     if (fullscreen)
       {
	 size_hints.max_width = width;
	 size_hints.max_height = height;
	 size_hints.flags = PSize | PMaxSize;
       }
     else
       {
	 size_hints.max_width = size_hints.min_width = width;
	 size_hints.max_height = size_hints.min_height = height;
	 size_hints.flags = PSize | PMinSize | PMaxSize;
       }

     XSetWMNormalHints (xdisplay, *window, &size_hints);
     
     XSetStandardProperties (xdisplay, *window, name, name,
			     None, (char **)NULL, 0, &size_hints);
     
     XSetWMProtocols (xdisplay, *window, &protocol, 1);
   }


   *context = glXCreateContext( xdisplay, visinfo, NULL, True );
   if (!*context)
     {
       g_printerr ("glXCreateContext failed.\n");
       XDestroyWindow (xdisplay, *window);
       XFreeColormap (xdisplay, *colormap);
       return FALSE;
     }

   return TRUE;
}

typedef struct
{
  enum {
    ACTION_DELETE_TEXTURE,
    ACTION_UPDATE_TEXTURE,
    ACTION_DRAW_TEXTURE,
    ACTION_BREAD_CRUMB
  } type;
  
  union
  {
    struct {
      LmcTexture *texture;
    } delete_texture;

    struct {
      LmcTexture *texture;
      XRectangle rect;
    } update_texture;
    
    struct {
      LmcTexture *texture;
      double alpha;
      int x;
      int y;
    } draw_texture;
    
    struct {
      LmcBreadCrumb *crumb;
    } bread_crumb;
  } d;
} Action;

static void
execute_update_action (Action *action)
{
  switch (action->type)
    {
    case ACTION_DELETE_TEXTURE:
      break;
    case ACTION_UPDATE_TEXTURE:
      {
	LmcTexture *texture = action->d.update_texture.texture;
	XRectangle *rect = &action->d.update_texture.rect;

	lmc_texture_update_rect (texture, rect);
	
	break;
      }
    case ACTION_DRAW_TEXTURE:
      break;
    case ACTION_BREAD_CRUMB:
      break;
    }
}

static void
execute_draw_action (Action *action)
{
  switch (action->type)
    {
    case ACTION_DELETE_TEXTURE:
      break;
    case ACTION_UPDATE_TEXTURE:
      break;
    case ACTION_DRAW_TEXTURE:
      {
	lmc_texture_draw (action->d.draw_texture.texture,
			  action->d.draw_texture.alpha,
			  action->d.draw_texture.x,
			  action->d.draw_texture.y);
	break;
      }
    case ACTION_BREAD_CRUMB:
      break;
    }
}

static gboolean
call_bread_crumb (gpointer data)
{
  LmcBreadCrumb *crumb = data;

  crumb->func (crumb->data);

  g_free (crumb);
  
  return FALSE;
}

static void
execute_bread_crumb_action (Action *action)
{
  switch (action->type)
    {
    case ACTION_DELETE_TEXTURE:
      break;
    case ACTION_UPDATE_TEXTURE:
      break;
    case ACTION_DRAW_TEXTURE:
      break;
    case ACTION_BREAD_CRUMB:
      {
	g_idle_add (call_bread_crumb, action->d.bread_crumb.crumb);
	break;
      }
    }
}

static void
free_action (Action *action)
{
  switch (action->type)
    {
    case ACTION_DELETE_TEXTURE:
      {
	lmc_texture_delete (action->d.delete_texture.texture);
	lmc_texture_unref (action->d.delete_texture.texture);
	break;
      }
    case ACTION_UPDATE_TEXTURE:
      {
	LmcTexture *texture = action->d.update_texture.texture;

	lmc_bits_unlock (texture->bits);
	lmc_texture_unref (texture);
	break;
      }
    case ACTION_DRAW_TEXTURE:
      {
	lmc_texture_unref (action->d.draw_texture.texture);
	break;
      }
    case ACTION_BREAD_CRUMB:
      break;
    }

  g_free (action);
}

static Action *
action_delete_texture (LmcTexture *texture)
{
  Action *action = g_new (Action, 1);

  action->type = ACTION_DELETE_TEXTURE;
  action->d.delete_texture.texture = lmc_texture_ref (texture);

  return action;
}

static Action *
action_update_texture (LmcTexture *texture,
		       XRectangle *rect)
{
  Action *action = g_new (Action, 1);

  action->type = ACTION_UPDATE_TEXTURE;
  action->d.update_texture.texture = lmc_texture_ref (texture);
  action->d.update_texture.rect = *rect;

  return action;
}

static Action *
action_draw_texture (LmcTexture *texture,
		     double      alpha,
		     int         x,
		     int         y)
{
  Action *action = g_new (Action, 1);

  action->type = ACTION_DRAW_TEXTURE;
  action->d.draw_texture.texture = lmc_texture_ref (texture);
  action->d.draw_texture.alpha = alpha;
  action->d.draw_texture.x = x;
  action->d.draw_texture.y = y;

  return action;
}

static Action *
action_bread_crumb (LmcBreadCrumb *crumb)
{
  Action *action = g_new (Action, 1);

  action->type = ACTION_BREAD_CRUMB;
  action->d.bread_crumb.crumb = crumb;

  return action;
}

static gpointer
output_thread_func (gpointer data)
{
  GQueue *action_queue = g_queue_new ();
  LmcOutput *output = data;
  LmcWindow *lwindow;
  static const gboolean do_scissor = FALSE;

  LOCK (output);

  lwindow = LMC_WINDOW (output);

  glXMakeCurrent (lwindow->display->xdisplay, lwindow->xwindow, output->context);

  glShadeModel (GL_FLAT);
  glViewport (0, 0, output->width, output->height);
  
  while (!output->destroyed)
    {
      gboolean need_redraw;
      XRectangle scissor;
      GSList *sl;
      GList *l;
      LmcTexture *cursor_texture;
      int cursor_x, cursor_y;
      LmcTexture *pager_texture;
      LmcTexture *background_texture;
      LmcBorderInfo pager_border;
      XRectangle pager_rect;
      GList *viewports = NULL;

      if (!output->to_delete && !output->to_update && g_queue_is_empty (output->bread_crumbs) &&
	  XEmptyRegion (output->invalid))
	output->idle = TRUE;

      if (output->idle || output->freeze_count)
	{
	  do
	    g_cond_wait (output->idle_cond, output->mutex);
	  while (output->idle || output->freeze_count);
	}
      g_assert (!output->freeze_count);
      
      if (output->to_delete)
	{
	  for (sl = output->to_delete; sl; sl = sl->next)
	    {
	      LmcTexture *texture = sl->data;
	      g_queue_push_tail (action_queue,
				 action_delete_texture (texture));
	      lmc_texture_unref (texture);
	    }
	  g_slist_free (output->to_delete);
	  output->to_delete = NULL;
	}

      if (output->to_update)
	{
	  for (sl = output->to_update; sl;)
	    {
	      GSList *next = sl->next;
	      LmcTexture *texture = sl->data;
	      XRectangle rect;

	      if (lmc_bits_lock (texture->bits, FALSE))
		{
		  XClipBox (texture->update, &rect);
		  XDestroyRegion (texture->update);
		  texture->update = XCreateRegion ();
		  
		  g_queue_push_tail (action_queue,
				     action_update_texture (texture, &rect));
		  
		  output->to_update = g_slist_delete_link (output->to_update, sl);
		}

	      sl = next;
	    }
	  output->to_update = NULL;
	}

      while (!g_queue_is_empty (output->bread_crumbs))
	{
	  LmcBreadCrumb *crumb = g_queue_pop_head (output->bread_crumbs);
	  g_queue_push_tail (action_queue, action_bread_crumb (crumb));
	}
      
      need_redraw = !XEmptyRegion (output->invalid);
      
      if (need_redraw)
	{
	  Region scissor_region;

	  if (do_scissor)
	    {
	      XClipBox (output->invalid, &scissor);
	      scissor_region = XCreateRegion ();
	      XUnionRectWithRegion (&scissor, scissor_region, scissor_region);
	    }

	  for (l = g_list_last (output->windows); l; l = l->prev)
	    {
	      LmcOutputWindow *owindow = l->data;
	      if (owindow->texture &&
		  (!do_scissor ||
		   XRectInRegion (scissor_region,
				  owindow->x, owindow->y, owindow->width, owindow->height)))
		{
		  g_queue_push_tail (action_queue,
				     action_draw_texture (owindow->texture,
							  owindow->alpha,
							  owindow->x, owindow->y));
		}
	    }

	  if (do_scissor)
	    XDestroyRegion (scissor_region);
	  
	  XDestroyRegion (output->invalid);
	  output->invalid = XCreateRegion ();
	}

      background_texture = output->background_texture;

      cursor_texture = output->cursor_texture;
      cursor_x = output->cursor_x;
      cursor_y = output->cursor_y;
      
      pager_texture = output->pager_texture;
      pager_rect = output->pager_rect;
      pager_border = output->pager_border;
      
      for (l = output->viewports; l; l = l->next)
	viewports = g_list_prepend (viewports, l->data);
      
      UNLOCK (output);

      g_queue_foreach (action_queue, (GFunc)execute_update_action, NULL);
	  
      if (need_redraw)
	{
	  if (do_scissor)
	    {
	      if (scissor.x == 0 && scissor.y == 0 && scissor.width == output->width && scissor.height == output->height)
		{
		  glDisable (GL_SCISSOR_TEST);
		}
	      else
		{
		  glEnable (GL_SCISSOR_TEST);
		  glScissor (scissor.x, output->height - scissor.y - scissor.height, scissor.width, scissor.height);
		}
	    }

	  /* Setup for drawing the main viewport */
	  
	  glLoadIdentity();
	  
	  gluOrtho2D (output->visible.x, output->visible.x + output->visible.width,
		      output->visible.y + output->visible.height,
		      output->visible.y);
	  
	  glDisable (GL_SCISSOR_TEST);

	  glClearColor (0.0, 0.0, 0.0, 1.0);
	  glClear (GL_COLOR_BUFFER_BIT);

	  glDisable (GL_TEXTURE_2D);
	  for (l = output->viewports; l; l = l->next)
	    {
	      LmcOutputViewport *viewport = l->data;

	      glColor3f (viewport->bg_red,
			 viewport->bg_green,
			 viewport->bg_blue);
	      glBegin (GL_POLYGON);
	      glVertex2i (viewport->src_x, viewport->src_y);		       
	      glVertex2i (viewport->src_x + viewport->src_width, viewport->src_y);		       
	      glVertex2i (viewport->src_x + viewport->src_width, viewport->src_y + viewport->src_height);
	      glVertex2i (viewport->src_x, viewport->src_y + viewport->src_height);
#if 0
	      g_print ("\n");
#endif
	      glEnd ();
	   }
	  glEnable (GL_TEXTURE_2D);
	  
	  /* Draw the background image */

	  if (background_texture)
	    {
	      lmc_texture_draw (background_texture, 1.0,
				(output->width - output->background_texture->width) / 2,
				(output->height - output->background_texture->height) / 2);
	    }

	  /* Draw the windows in the main viewport */
	  
	  g_queue_foreach (action_queue, (GFunc)execute_draw_action, NULL);

	  if (output->show_pager)
	    {
	      /* Draw the pager border */
	      glPushMatrix ();
	      glLoadIdentity ();
	      gluOrtho2D (0, output->width, output->height, 0);
	      if (pager_texture)
		lmc_texture_draw_border (pager_texture, 1.0, &pager_rect, &pager_border);
	      glPopMatrix();
	      
	      /* Draw pager viewports */
	      
	      for (l = viewports; l; l = l->next)
		{
		  LmcOutputViewport *viewport = l->data;
		  double scale_x, scale_y;
		  double left, right, bottom, top;
		  
		  /* Let  f(x) = mx + b
		     want f(src_x) = dest_x, f(src_x + src_width) = dest_x + dest_width
		     m * src_x + b = dest_x;
		     m * (src_x + src_width) + b = dest_x + dest_width
		     m * src_width = dest_width;
		     m = dest_width / src_width;
		     b = dest_x - src_x * (dest_width / src_width);
		     
		     m * x0 + b = 0 => x0 = - b / m = - dest_x * src_width / dest_width + src_x
		     m * x1 + b = width => x1 = - (b - width) / m = x0 + width / m */
		  
		  scale_x = viewport->src_width / viewport->dest_width;
		  left = viewport->src_x - viewport->dest_x * scale_x;
		  right = left + output->width * scale_x;
		  
		  scale_y = viewport->src_height / viewport->dest_height;
		  top = viewport->src_y - viewport->dest_y * scale_y;
		  bottom = top + output->height * scale_y;
		  
		  glPushMatrix ();
		  glLoadIdentity();
		  
		  gluOrtho2D (left, right, bottom, top);
		  
		  glEnable (GL_SCISSOR_TEST);
		  glScissor (viewport->dest_x, output->height - viewport->dest_y - viewport->dest_height,
			     viewport->dest_width, viewport->dest_height);
		  
		  glClearColor (viewport->bg_red, viewport->bg_green, viewport->bg_blue, 1.0);
		  glClear (GL_COLOR_BUFFER_BIT);
		  
		  g_queue_foreach (action_queue, (GFunc)execute_draw_action, NULL);
		  glPopMatrix();
		}
	    }

	  /* Draw the cursor */

	  glPushMatrix ();
	  glLoadIdentity ();
	  gluOrtho2D (0, output->width, output->height, 0);
	  if (cursor_texture)
	    {
	      glDisable (GL_SCISSOR_TEST);

	      lmc_texture_draw (cursor_texture, 1.0, cursor_x, cursor_y);
	    }
	  glPopMatrix();
	  
	  glXSwapBuffers (lwindow->display->xdisplay, lwindow->xwindow);
	}

      glFinish();

      g_queue_foreach (action_queue, (GFunc)execute_bread_crumb_action, NULL);
      
      g_list_free (viewports);
	  
      if (!g_queue_is_empty (action_queue))
	{
	  g_queue_foreach (action_queue, (GFunc)free_action, NULL);
		  
	  g_queue_free (action_queue);
	  action_queue = g_queue_new ();
	}

      LOCK (output);
    }
       
  g_queue_free (action_queue);
  
  UNLOCK (output);

  return NULL;
}

static void
output_idle_wakeup (LmcOutput *output)
{
  if (output->idle)
    {
      output->idle = FALSE;
      if (output->freeze_count == 0)
	g_cond_broadcast (output->idle_cond);
    }
}

static gboolean
clamp_rect (XRectangle *rect,
	    int         width,
	    int         height)
{
  if (rect->x < 0)
    {
      rect->width = MAX (0, (int)rect->width + rect->x);
      rect->x = 0;
    }
  if (rect->y < 0)
    {
      rect->height = MAX (0, (int)rect->height + rect->y);
      rect->y = 0;
    }
  if (rect->x + rect->width > width)
    rect->width = MAX (0, width - rect->x);
  if (rect->y + rect->height > height)
    rect->height = MAX (0, height - rect->y);

  return rect->width != 0 || rect->height != 0;
}

static void
output_invalidate_rect (LmcOutput *output,
			int        x,
			int        y,
			int        width,
			int        height)
{
  XRectangle rect;

  rect.x = x;
  rect.y = y;
  rect.width = width;
  rect.height = height;

  /* With multiple viewports we currently redraw the entire scene
   * if anything changes anywhere. We really need to rework output->invalid
   * to be in window coordinates, not world coordinates
   */
#if 0  
  if (!clamp_rect (&rect, output->width, output->height))
    return;
#endif  
      
  XUnionRectWithRegion (&rect, output->invalid, output->invalid);

  output_idle_wakeup (output);
}

static gboolean
on_expose (LmcOutput    *output,
	   XExposeEvent *xev)
{
  LOCK (output);

  output_invalidate_rect (output, xev->x, xev->y, xev->width, xev->height);
  
  UNLOCK (output);

  return FALSE;
}

static gboolean
on_client_message (LmcOutput           *output,
		   XClientMessageEvent *xev)
{
  Display *xdisplay = LMC_WINDOW (output)->display->xdisplay;
  
  if (xev->message_type == XInternAtom (xdisplay, "WM_PROTOCOLS", False) &&
      xev->data.l[0] == XInternAtom (xdisplay, "WM_DELETE_WINDOW", False))
    {
      g_signal_emit (output, delete_window_signal, 0);
      return TRUE;
    }

  return FALSE;
}

static gboolean
on_configure_notify (LmcOutput       *output,
		     XConfigureEvent *xev)
{
  /* This is a hack for the Savage DRI driver, where SAVAGEDRIMoveBuffers
   * is a no-op, so when a toplevel is moved, the backbuffer / depth
   * buffer doesn't move along with the toplevel.
   * (https://freedesktop.org/bugzilla/show_bug.cgi?id=1735)
   *
   * ConfigureNotify could be restacking as well as moving, instead
   * of trying to figure out which, we always redraw everything.
   */
  output_invalidate_rect (output, 0, 0, output->width, output->height);

  return FALSE;
}

static LmcOutputViewport *
output_viewport_new (LmcOutput *output,
		     int        src_x,
		     int        src_y,
		     int        src_width,
		     int        src_height,
		     int        dest_x,
		     int        dest_y,
		     int        dest_width,
		     int        dest_height)
{
  LmcOutputViewport *viewport = g_new (LmcOutputViewport, 1);

  viewport->output = output;

  viewport->src_x = src_x;
  viewport->src_y = src_y;
  viewport->src_width = src_width;
  viewport->src_height = src_height;
  viewport->dest_x = dest_x;
  viewport->dest_y = dest_y;
  viewport->dest_width = dest_width;
  viewport->dest_height = dest_height;

  viewport->bg_red = viewport->bg_blue = viewport->bg_green = 0.0;

  return viewport;
}

LmcOutput *
lmc_output_new (LmcDisplay *ldisplay,
		int         screen,
		int         width,
		int         height)
{
  LmcOutput *output;
  Window xwindow;
  Colormap colormap;
  GLXContext context;
  GError *error = NULL;

  if (!make_window (ldisplay->xdisplay, screen, "luminocity", width, height,
		    &xwindow, &colormap, &context))
    return NULL;

  output = g_object_new (LMC_TYPE_OUTPUT,
			 "display", ldisplay,
			 "xid",     xwindow,
			 NULL);

  output->width = width;
  output->height = height;

  output->colormap = colormap;
  output->context = context;

  output->invalid = XCreateRegion ();

  output->mutex = g_mutex_new ();
  output->thread = g_thread_create (output_thread_func, output, TRUE, &error);

  output->idle = FALSE;
  output->idle_cond = g_cond_new ();
    
  output->main_viewport = output_viewport_new (output,
					       0, 0, width, height,
					       0, 0, width, height);
  output_invalidate_rect (output, 0, 0, output->width, output->height);

  if (!output->thread)
    {
      g_error ("Error creating thread: %s\n", error->message);
      g_error_free (error);
      lmc_output_destroy (output);
      g_object_unref (output);

      return NULL;
    }

  lmc_window_select_input (LMC_WINDOW (output), ExposureMask);
  g_signal_connect (output, "event::Expose", G_CALLBACK (on_expose), NULL);
  
  g_signal_connect (output, "event::ClientMessage", G_CALLBACK (on_client_message), NULL);
  g_signal_connect (output, "event::ConfigureNotify", G_CALLBACK (on_configure_notify), NULL);

  output->visible.x = 0;
  output->visible.y = 0;
  output->visible.width = output->width;
  output->visible.height = output->height;

  output->show_pager = TRUE;
  
  return output;
}

void
lmc_output_get_size (LmcOutput *output,
		     int	*width,
		     int	*height)
{
    if (width)
	*width = output->width;
    if (height)
	*height = output->height;
}

void
lmc_output_queue_bread_crumb (LmcOutput                *output,
			      LmcOutputBreadCrumbFunc   func,
			      gpointer                  data)
{
  LmcBreadCrumb *crumb = g_new0 (LmcBreadCrumb, 1);

  LOCK (output);

  crumb->func = func;
  crumb->data = data;

  g_queue_push_tail (output->bread_crumbs, crumb);

  output_idle_wakeup (output);
  
  UNLOCK (output);
}

LmcOutputViewport *
lmc_output_get_main_viewport (LmcOutput *output)
{
  return output->main_viewport;
}

LmcOutputViewport *
lmc_output_add_viewport (LmcOutput *output,
			 int        src_x,
			 int        src_y,
			 int        src_width,
			 int        src_height,
			 int        dest_x,
			 int        dest_y,
			 int        dest_width,
			 int        dest_height)
{
  LmcOutputViewport *viewport = output_viewport_new (output,
						     src_x, src_y, src_width, src_height,
						     dest_x, dest_y, dest_width, dest_height);

  LOCK (output);
  
  output->viewports = g_list_prepend (output->viewports, viewport);
  
  output_invalidate_rect (output, 0, 0, output->width, output->height);

  UNLOCK (output);
  
  return viewport;
}

void
lmc_output_set_visible_rect (LmcOutput		*output,
			     XRectangle		*rect)
{
  LOCK (output);

  output->visible = *rect;
  
  output_invalidate_rect (output, 0, 0, output->width, output->height);

  UNLOCK (output);
}

void
lmc_output_get_visible_rect (LmcOutput *output,
			     XRectangle *rect)
{
  LOCK (output);

  if (rect)
    *rect = output->visible;
  
  UNLOCK (output);
}

void
lmc_output_viewport_offset (LmcOutputViewport *viewport,
			    int                x_offset,
			    int                y_offset)
{
  LmcOutput *output = viewport->output;

  LOCK (output);
  
  viewport->src_x += x_offset;
  viewport->src_y += y_offset;

  output_invalidate_rect (output, 0, 0, output->width, output->height);
  
  UNLOCK (output);
}

void
lmc_output_viewport_set_bgcolor (LmcOutputViewport *viewport,
				 double             red,
				 double             green,
				 double             blue)
{
  LmcOutput *output = viewport->output;
  
  LOCK (output);
  
  viewport->bg_red = red;
  viewport->bg_green = green;
  viewport->bg_blue = blue;

  output_invalidate_rect (output, 0, 0, output->width, output->height);
  
  UNLOCK (output);
}

void
lmc_output_destroy (LmcOutput *output)
{
  LmcWindow *lwindow;
  
  LOCK (output);

  lwindow = LMC_WINDOW (output);

  /* Wait for display thread to terminate */
  output->destroyed = TRUE;
  UNLOCK (output);

  if (output->thread)
    {
      g_thread_join (output->thread);
      output->thread = NULL;
    }

  XFreeColormap (lwindow->display->xdisplay, output->colormap);
  output->colormap = None;
}

void
lmc_output_freeze (LmcOutput *output)
{
  LOCK (output);

  output->freeze_count++;

  UNLOCK (output);
}

void
lmc_output_thaw (LmcOutput *output)
{
  LOCK (output);
  
  output->freeze_count--;

  g_assert (output->freeze_count >= 0);
  
  if (output->freeze_count == 0)
    {
      if (!output->idle)
	g_cond_broadcast (output->idle_cond);
    }
  
  UNLOCK (output);
}

static void
output_modify_texture (LmcOutput  *output,
		       LmcTexture *texture,
		       int         x,
		       int         y,
		       int         width,
		       int         height)
{
  XRectangle rect;
      
  rect.x = x;
  rect.y = y;
  rect.width = width;
  rect.height = height;
  
  if (!clamp_rect (&rect, texture->width, texture->height))
    return;
  
  XUnionRectWithRegion (&rect, texture->update, texture->update);
  
  if (!g_slist_find (output->to_update, texture))
    {
      output->to_update = g_slist_prepend (output->to_update,texture);
      output_idle_wakeup (output);
    }
}

void
lmc_output_set_background (LmcOutput  *output,
			   LmcBits    *bits)
{
  if ((output->background_texture ? output->background_texture->bits : NULL) != bits)
    {
      if (output->background_texture)
	{
	  output->to_update = g_slist_remove (output->to_update, output->background_texture);
	  output->to_delete = g_slist_prepend (output->to_delete, output->background_texture);
	  output->background_texture = NULL;
	}
      
      if (bits)
	{
	  output->background_texture = lmc_texture_new (bits);
	  output_modify_texture (output, output->background_texture,
				 0, 0, bits->width, bits->height);
	}
      
      output_invalidate_rect (output, 0, 0, output->width, output->height);
    }
}

void
lmc_output_set_cursor (LmcOutput *output,
		       LmcBits   *bits,
		       int        x,
		       int        y)
{
  LmcBits *old_bits;
  
  gboolean changed;
  
  LOCK (output);

  old_bits = output->cursor_texture ? output->cursor_texture->bits : NULL;

  changed = (bits != old_bits || x != output->cursor_x || y != output->cursor_y);

  if (changed)
    {
      if (output->cursor_texture)
	output_invalidate_rect (output,
				output->cursor_x, output->cursor_y,
				output->cursor_texture->width, output->cursor_texture->height);
    }
  
  if (bits != old_bits)
    {
      if (output->cursor_texture)
	{
	  output->to_update = g_slist_remove (output->to_update, output->cursor_texture);
	  output->to_delete = g_slist_prepend (output->to_delete, output->cursor_texture);
	  output->cursor_texture = NULL;
	}
      
      if (bits)
	{
	  output->cursor_texture = lmc_texture_new (bits);
	  output_modify_texture (output, output->cursor_texture,
				 0, 0, bits->width, bits->height);
	}
    }

  output->cursor_x = x;
  output->cursor_y = y;

  if (changed)
    {
      if (output->cursor_texture)
	output_invalidate_rect (output,
				output->cursor_x, output->cursor_y,
				output->cursor_texture->width, output->cursor_texture->height);
    }

  UNLOCK (output);
}

void
lmc_output_set_pager (LmcOutput           *output,
		      int                  x,
		      int                  y,
		      int                  width,
		      int                  height,
		      LmcBits             *bits,
		      const LmcBorderInfo *border_info)
{
  LOCK (output);

  if (output->pager_texture)
    {
      output->to_update = g_slist_remove (output->to_update, output->pager_texture);
      output->to_delete = g_slist_prepend (output->to_delete, output->pager_texture);
      output->pager_texture = NULL;
    }

  output->pager_rect.x = x;
  output->pager_rect.y = y;
  output->pager_rect.width = width;
  output->pager_rect.height = height;
  output->pager_border = *border_info;
  
  if (bits)
    {
      output->pager_texture = lmc_texture_new (bits);
      output_modify_texture (output, output->pager_texture,
			     0, 0, bits->width, bits->height);
    }

  output_invalidate_rect (output, 0, 0, output->width, output->height);

  UNLOCK (output);
}

static void
output_window_invalidate (LmcOutputWindow *owindow)
{
  if (owindow->texture)
    output_invalidate_rect (owindow->output,
			    owindow->x,
			    owindow->y,
			    owindow->width,
			    owindow->height);
}

static void
output_window_modify  (LmcOutputWindow *owindow,
		       int              x,
		       int              y,
		       int              width,
		       int              height)
{
  LmcOutput *output = owindow->output;

  if (owindow->texture)
    {
      output_modify_texture (output, owindow->texture, x, y, width, height);
      output_invalidate_rect (output,
			      owindow->x + x, owindow->y + y,
			      width, height);
    }
}

LmcOutputWindow *
lmc_output_create_window (LmcOutput *output)
{
  LmcOutputWindow *owindow;
  
  LOCK (output);

  owindow = g_new0 (LmcOutputWindow, 1);

  owindow->output = output;
  owindow->alpha = 1.0;

  output->windows = g_list_prepend (output->windows, owindow);

  UNLOCK (output);

  return owindow;
}

void
output_window_delete_texture (LmcOutputWindow *owindow)
{
  LmcOutput *output = owindow->output;
  
  if (owindow->texture)
    {
      output->to_update = g_slist_remove (output->to_update, owindow->texture);
      output->to_delete = g_slist_prepend (output->to_delete, owindow->texture);
      owindow->texture = NULL;

      output_idle_wakeup (output);
    }
}

void
lmc_output_window_destroy (LmcOutputWindow *owindow)
{
    int i;
    LmcOutput *output = owindow->output;

  LOCK (output);

  output_window_invalidate (owindow);

  output->windows = g_list_remove (output->windows, owindow);
  
  output_window_delete_texture (owindow);
  
  for (i = 0; i < sizeof (LmcOutputWindow) / 4; i++)
  {
      *(((int *)owindow) + i) =  0xdeadbeef;
  }
#if 0
  g_free (owindow);
#endif
		 
  UNLOCK (output);
}

void
lmc_output_window_set_bits (LmcOutputWindow *owindow,
			    LmcBits         *bits)
{
  LmcOutput *output = owindow->output;

  LOCK (output);

  if (bits != (owindow->texture ? owindow->texture->bits : NULL))
    {
      output_window_invalidate (owindow);
      
      output_window_delete_texture (owindow);
      
      if (bits)
	{
	  owindow->texture = lmc_texture_new (bits);
	  owindow->width = bits->width;
	  owindow->height = bits->height;

	  output_window_modify (owindow, 0, 0, bits->width, bits->height);
      
	  output_window_invalidate (owindow);
	}
    }
      
  UNLOCK (output);
}

void
lmc_output_window_set_alpha (LmcOutputWindow *owindow,
			     double           alpha)
{
  LmcOutput *output = owindow->output;

  LOCK (output);
  
  owindow->alpha = alpha;
  output_window_invalidate (owindow);

  UNLOCK (output);
}

void
lmc_output_window_set_deformation (LmcOutputWindow    *owindow,
				   LmcDeformationFunc  func,
				   void               *data)
{
  LmcOutput *output = owindow->output;

  LOCK (output);

  if (owindow->texture != NULL)
    {
      output_window_invalidate (owindow);

      lmc_texture_set_deformation (owindow->texture, func, data);

      output_window_invalidate (owindow);
    }

  UNLOCK (output);
}

void
lmc_output_window_move (LmcOutputWindow *owindow,
			int              x,
			int              y)
{
  LmcOutput *output = owindow->output;
  
  LOCK (output);

  if (x != owindow->x || y != owindow->y)
    {
      output_window_invalidate (owindow);
      
      owindow->x = x;
      owindow->y = y;
      
      output_window_invalidate (owindow);
    }
  
  UNLOCK (output);
}

void
lmc_output_window_restack (LmcOutputWindow *owindow,
			   LmcOutputWindow *above)
{
  LmcOutput *output = owindow->output;
  GList *l, *l_above;

  g_return_if_fail (owindow != above);
  
  LOCK (output);

  output_window_invalidate (owindow);
  
  l = g_list_find (output->windows, owindow);
  g_assert (l);

  if (above)
    {
      l_above = g_list_find (output->windows, above);
      g_assert (l_above);
    }
  else
    l_above = NULL;

  if (l->next != l_above)
    {
      output->windows = g_list_delete_link (output->windows, l);
      output->windows = g_list_insert_before (output->windows, l_above, owindow);

      output_window_invalidate (owindow);
    }
      
  UNLOCK (output);
}

void
lmc_output_window_modify  (LmcOutputWindow *owindow,
			   int              x,
			   int              y,
			   int              width,
			   int              height)
{
  LmcOutput *output = owindow->output;
  
  LOCK (output);

  output_window_modify (owindow, x, y, width, height);
  
  UNLOCK (output);
}

void
lmc_output_set_show_pager (LmcOutput *output,
			   gboolean show_pager)
{
  LOCK (output);

  show_pager = show_pager != FALSE;
  
  if (show_pager != output->show_pager)
    {
      output->show_pager = show_pager;

      output_invalidate_rect (output, 0, 0, output->width, output->height);
    }
  
  UNLOCK (output);
}
