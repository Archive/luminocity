#include <X11/Xlibint.h>

#include "async.h"
#include "types.h"

typedef struct _GetPropertyState GetPropertyState;

struct _GetPropertyState
{
  _XAsyncHandler async;
  unsigned get_property_req;
  
  LmcAsyncGetPropertyCallback callback;
  LmcAsyncErrorCallback error_callback;
  void *user_data;
};

static int
read_property_data (Display          *dpy,
		    xReply           *rep,
		    char             *buf,
		    int               len,
		    LmcPropertyValue *value)
{
  unsigned long nbytes, netbytes;
  unsigned char *data;
  int bytes_read;

  /* This logic comes from XGetWindowProperty() by way of Metacity's
   * async-getprop.c.
   */

  xGetPropertyReply replbuf;
  xGetPropertyReply *reply;
	  
  reply = (xGetPropertyReply *)
    _XGetAsyncReply(dpy, (char *)&replbuf, rep, buf, len,
		    (sizeof(xGetPropertyReply) - sizeof(xReply)) >> 2,
		    False);

  value->type = reply->propertyType;
  value->format = reply->format;
  value->n_items = reply->nItems;
  value->data.b = NULL;
  
  if (reply->propertyType == None)
    {
      if (reply->length != 0)
	goto bad_implementation;
      else
	return Success;
    }

  switch (reply->format)
    {
    case 8:
      nbytes = reply->nItems;
      netbytes = reply->nItems;
      break;
    case 16:
      nbytes = reply->nItems * sizeof (short);
      if (nbytes / sizeof (short) != reply->nItems)
	goto bad_implementation;
      netbytes = reply->nItems * 2;
      break;
    case 32:
      nbytes = reply->nItems * sizeof (long);
      if (nbytes / sizeof (long) != reply->nItems)
	goto bad_implementation;
      netbytes = reply->nItems * 4;
      break;
    default:
      /* * The server sent back a property with an invalid format.
       * This is a BadImplementation error.
       */
      goto bad_implementation;
    }

  netbytes = (netbytes + 3) & ~3;
  if (netbytes == 0 || netbytes / 4 != reply->length)
    goto bad_implementation;

  /*
   * One extra byte is malloced than is needed to contain the property
   * data, but this last byte is null terminated and convenient for
   * returning string properties, so the client doesn't then have to
   * recopy the string to make it null terminated.
   *
   * Note that nbytes + 1 is guaranteed not to overflow from above
   * calculations; the check here is for sizeof(long) > sizeof(size_t).
   */
  if ((size_t)(nbytes + 1) != nbytes + 1)
    goto bad_implementation;

  data = g_try_malloc (nbytes + 1);
  if (!data)
    {
      _XEatData(dpy, netbytes);
      return BadAlloc;
    }
  
  data[nbytes] = '\0';

  bytes_read = sizeof (xGetPropertyReply);

  switch (reply->format)
    {
    case 8:
    case 16:
      _XGetAsyncData (dpy, data, buf, len,
		      bytes_read, nbytes, netbytes);
      break;
  
    case 32:
      /* We match XGetWindowProperty() and return format 32 as long, even on
       * 64-bit platforms.
       */
      if (sizeof (long) == 8)
	{
	  unsigned char *netdata;
	  unsigned char *lptr;
	  unsigned char *end_lptr;
	  
	  /* Store the 32-bit values in the end of the array */
	  netdata = data + nbytes / 2;
	  
	  _XGetAsyncData (dpy, netdata, buf, len,
			  bytes_read, nbytes, netbytes);
	  
	  /* Now move the 32-bit values to the front */
	  
	  lptr = data;
	  end_lptr = data + nbytes;
	  while (lptr != end_lptr)
	    {
	      *(long*) lptr = *(CARD32*) netdata;
	      lptr += sizeof (long);
	      netdata += sizeof (CARD32);
	    }
	}
      else
	{
	  /* Here the wire format matches our actual format */
	  _XGetAsyncData (dpy, data, buf, len,
			  bytes_read, nbytes, netbytes);
	}
      break;
    }

  value->data.b = data;

  return Success;

 bad_implementation:
  /* The server has sent us values that are invalid, inconsistent, or that
   * overflow integers. We'll probably never get the protocol stream back
   * in sync, but try using the length from the reply to know how much to read.
   */
  _XEatData(dpy, reply->length << 2);
  return BadImplementation;
}

static Bool
get_property_handler (Display *dpy,
		      xReply  *rep,
		      char    *buf,
		      int      len,
		      XPointer data)
{
  GetPropertyState *state = (GetPropertyState *)data;
  Bool result = False;
  
  if (dpy->last_request_read == state->get_property_req)
    {
      if (rep->generic.type == X_Error)
	{
	  result = state->error_callback (rep->error.errorCode, state->user_data) != False;
	}
      else
	{
	  LmcPropertyValue value;
	  int errorCode;
	  
	  errorCode = read_property_data (dpy, rep, buf, len, &value);
	  if (errorCode == Success)
	    {
	      state->callback (value.type, value.format, value.n_items, value.data.b,
			       state->user_data);
	      result = True;
	    }
	  else			/* BadAlloc or BadImplementation */
	    {
	      result = state->error_callback (rep->error.errorCode, state->user_data) != False;
	      if (!result && errorCode == BadImplementation)
		{
		  xError error;
		  
		  error.sequenceNumber = state->get_property_req;
		  error.type = X_Error;
		  error.majorCode = X_GetProperty;
		  error.minorCode = 0;
		  error.errorCode = BadImplementation;
		  
		  _XError (dpy, &error);
		}
	    }
	}

      DeqAsyncHandler (dpy, &state->async);
      g_free (state);
    }

  return result;
}

/**
 * LmcAsyncGetProperty:
 * @dpy: an X display
 * @window: a window on @dpy
 * @property: name of a property
 * @callback: callback on success
 * @error_callback: callback on error
 * @user_data: User data to pass to @callback or @error_callback
 * 
 * Retrieves the entire contents of the specified property from the
 * X server, calling @callback on success with the result and
 * @error_callback if an error occurs. Note that the callbacks are
 * called with the Xlib lock, and must not call any Xlib functions.
 * If X is being used from multiple threads, the callback will be
 * run in one of the threads in an unpredictable fashion.
 **/
void
LmcAsyncGetProperty (Display                    *dpy,
		     Window                      window,
		     Atom                        property,
		     LmcAsyncGetPropertyCallback callback,
		     LmcAsyncErrorCallback       error_callback,
		     void                       *user_data)
{
  xGetPropertyReq *req;
  GetPropertyState *state;

  state = g_new (GetPropertyState, 1);

  state->user_data = user_data;
  state->callback = callback;
  state->error_callback = error_callback;

  state->async.next = dpy->async_handlers;
  state->async.handler = get_property_handler;
  state->async.data = (XPointer) state;
  dpy->async_handlers = &state->async;
  
  LockDisplay (dpy);

  GetReq (GetProperty, req);
  req->window = window;
  req->property = property;
  req->type = AnyPropertyType;
  req->delete = False;
  req->longOffset = 0;
  req->longLength = G_MAXLONG;

  state->get_property_req = dpy->request;
    
  UnlockDisplay (dpy);

  SyncHandle ();
}

/************************************************************************/

struct _LmcAsyncErrorTrap
{
  Display *dpy;
  _XAsyncHandler async;

  unsigned long first_request;
  unsigned long last_request;
};
	
static Bool
error_trap_handler (Display *dpy,
		    xReply  *rep,
		    char    *buf,
		    int      len,
		    XPointer data)
{
  LmcAsyncErrorTrap *trap = (LmcAsyncErrorTrap *)data;

  if (dpy->last_request_read == trap->last_request)
    {
      xGetInputFocusReply replbuf;
      xGetInputFocusReply *repl;
      
      if (rep->generic.type != X_Error)
	{
	  /* Actually does nothing, since there are no additional bytes
	   * to read, but maintain good form.
	   */
	  repl = (xGetInputFocusReply *)
	    _XGetAsyncReply(dpy, (char *)&replbuf, rep, buf, len,
			    (sizeof(xGetInputFocusReply) - sizeof(xReply)) >> 2,
			    True);
	}

      DeqAsyncHandler (dpy, &trap->async);
      g_free (trap);
      
      return (rep->generic.type != X_Error);
    }
  else if (dpy->last_request_read >= trap->first_request)
    {
      if (rep->generic.type == X_Error)
	return True;
    }
  

  return False;
}

/**
 * lmc_async_error_trap_set:
 * @dpy: a X Display
 *
 * Ignores errors for requests on this display made between
 * this call and a matching call lmc_async_error_trap_set.
 *
 * Note that this may interfere with intermediate calls that
 * have a return value or check for errors.
 * 
 * Note that this doesn't work properly if the display is being used
 * from multiple threads.
 * 
 * Return value: the trap structure to pass to
 *  lc_async_error_trap_unset().
 **/
LmcAsyncErrorTrap *
lmc_async_error_trap_set (Display *dpy)
{
  LmcAsyncErrorTrap *trap = g_new (LmcAsyncErrorTrap, 1);
  
  LockDisplay(dpy);

  trap->dpy = dpy;
  trap->async.next = dpy->async_handlers;
  trap->async.handler = error_trap_handler;
  trap->async.data = (XPointer) trap;
  dpy->async_handlers = &trap->async;

  trap->first_request = dpy->request + 1;
  
  UnlockDisplay(dpy);

  return trap;
}
     
void
lmc_async_error_trap_unset (LmcAsyncErrorTrap *trap)
{
  Display *dpy = trap->dpy;
  xReq *req;
  
  LockDisplay(dpy);
  
  GetEmptyReq(GetInputFocus, req);
  trap->last_request = dpy->request;
  
  UnlockDisplay(dpy);
  SyncHandle();
}
