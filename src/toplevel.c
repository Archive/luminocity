/* Image setup code:
 * 
 * Copyright © 2004 Keith Packard
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Keith Packard not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Keith Packard makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * KEITH PACKARD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL KEITH PACKARD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/shm.h>

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/XShm.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/sync.h>

#include "async.h"
#include "toplevel.h"
#include "decoration.h"

#include "lmc-marshal.h"

typedef struct _LmcToplevelClass  LmcToplevelClass;

struct _LmcToplevel
{
  LmcWindow parent_instance;

  Visual *visual;
  int depth;
  LmcBits *bits;

  int x, y, width, height;
  int mouse_x, mouse_y;

  enum {
    /* Withdrawn. Unmapped and we don't care about it */
    WITHDRAWN,
    /* Got a MapRequest, still waiting for properties */
    MAP_PENDING,
    /* Got a MapRequest and all the necessary properties */
    MAP_REQUEST,
    /* We've mapped the window */
    MAPPED
  } state;
  
  guint mapped : 1;
  guint override_redirect : 1;
  guint fullscreen : 1;
  guint pad_alpha : 1;
  Damage damage;

  unsigned long damage_serial;
  XserverRegion parts_region;

  XImage *image;

  XShmSegmentInfo shm_info;
  Pixmap shm_pixmap;
  GC shm_gc;

  XWMHints *wm_hints;
  XSizeHints *wm_normal_hints;
  gchar *wm_name;

  XSyncCounter sync_counter;
  XSyncValue sync_value;
  XSyncAlarm sync_alarm;
  gulong sync_alarm_connection;

  gboolean have_sync_wait_position_changed;

  gboolean have_sync_wait_reconfigure;
  int sync_wait_x;
  int sync_wait_y;
  int sync_wait_width;
  int sync_wait_height;

  gboolean have_sync_wait_damage;
  gpointer user_data;

  LmcBorderInfo border_info;

  enum {
    UNMAPPED,
    DRAW_PENDING,
    WAIT_FOR_SYNC,
    READY
  } draw_state;
};

struct _LmcToplevelClass
{
  LmcWindowClass parent_class;
};

static gboolean toplevel_initialize_sync_counter (LmcToplevel *toplevel);

/**********************************************************************/

guint modified_signal;
guint map_request_signal;
guint size_changed_signal;
guint begin_move_signal;
guint update_move_signal;
guint end_move_signal;
guint position_changed_signal;
guint wm_name_changed_signal;
guint bits_available;

G_DEFINE_TYPE (LmcToplevel, lmc_toplevel, LMC_TYPE_WINDOW)

static void
destroy_bits (gpointer  data)
{
  XShmSegmentInfo *shm_info = data;

  shmdt (shm_info->shmaddr);

  g_free (shm_info);
}

static gboolean
create_window_image (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);;
  int image_size;
  XGCValues gcv;
  LmcBitsFormat format;
  int width, height;

  g_return_val_if_fail (toplevel->image == NULL, False);

  if (toplevel->depth == 16 &&
      toplevel->visual->red_mask == 0xf800 &&
      toplevel->visual->green_mask == 0x7e0 &&
      toplevel->visual->blue_mask == 0x1f)
    {
      format = LMC_BITS_RGB_16;
      toplevel->pad_alpha = FALSE;
    }
  else if (toplevel->depth == 24 &&
	   toplevel->visual->red_mask == 0xff0000 &&
	   toplevel->visual->green_mask == 0xff00 &&
	   toplevel->visual->blue_mask == 0xff)
    {
      format = LMC_BITS_ARGB_32;
      toplevel->pad_alpha = TRUE;
    }
  else if (toplevel->depth == 32 &&
	   toplevel->visual->red_mask == 0xff0000 &&
	   toplevel->visual->green_mask == 0xff00 &&
	   toplevel->visual->blue_mask == 0xff)
    {
      format = LMC_BITS_ARGB_32;
      toplevel->pad_alpha = FALSE;
    }
  else
    {
      g_warning ("Unknown visual format depth=%d, r=%#lx/g=%#lx/b=%#lx",
		 toplevel->depth, toplevel->visual->red_mask, toplevel->visual->green_mask, toplevel->visual->blue_mask);
      
      return FALSE;
    }

  width = toplevel->border_info.left
    + toplevel->width
    + toplevel->border_info.right;
  height = toplevel->border_info.top
    + toplevel->height
    + toplevel->border_info.bottom;

  toplevel->image = XCreateImage (xdisplay,
				  toplevel->visual,
				  toplevel->depth,
				  ZPixmap,
				  0,
				  0,
				  width,
				  height,
				  32,
				  0);
  if (!toplevel->image)
    return FALSE;
  
  image_size = (toplevel->image->bytes_per_line * height);

  toplevel->shm_info.shmid = shmget (IPC_PRIVATE, image_size, IPC_CREAT|0600);
  if (toplevel->shm_info.shmid < 0)
    {
      XDestroyImage (toplevel->image);
      toplevel->image = NULL;
      return FALSE;
    }
  
  toplevel->shm_info.shmaddr = (char *) shmat (toplevel->shm_info.shmid, 0, 0);
  if (toplevel->shm_info.shmaddr == ((char *) -1))
    {
      XDestroyImage (toplevel->image);
      toplevel->image = NULL;
      shmctl (toplevel->shm_info.shmid, IPC_RMID, 0);
      return FALSE;
    }
    
  lmc_display_error_trap_push (lwindow->display);
  
  toplevel->shm_info.readOnly = False;
  XShmAttach (xdisplay, &toplevel->shm_info);
  XSync (xdisplay, False);

  if (lmc_display_error_trap_pop (lwindow->display))
    {
      XDestroyImage (toplevel->image);
      toplevel->image = NULL;
      shmdt (&toplevel->shm_info.shmaddr);
      shmctl (toplevel->shm_info.shmid, IPC_RMID, 0);
      return FALSE;
    }

  /* Detach now so we clean up on abnormal exit */
  shmctl (toplevel->shm_info.shmid, IPC_RMID, 0);
  
  toplevel->image->data = toplevel->shm_info.shmaddr;
  toplevel->image->obdata = (char *) &toplevel->shm_info;
  toplevel->shm_pixmap = XShmCreatePixmap (xdisplay,
					   lwindow->xwindow,
					   toplevel->image->data,
					   &toplevel->shm_info,
					   width,
					   height,
					   toplevel->depth);
  
  gcv.graphics_exposures = False;
  gcv.subwindow_mode = IncludeInferiors;
  toplevel->shm_gc = XCreateGC (xdisplay,
				lwindow->xwindow,
				GCGraphicsExposures|GCSubwindowMode,
				&gcv);


  toplevel->bits = lmc_bits_new (format,
				 width, height,
				 toplevel->image->data,
				 toplevel->image->bytes_per_line,
				 destroy_bits,
				 g_memdup (&toplevel->shm_info, sizeof (XShmSegmentInfo)));

  return TRUE;
}

static void
destroy_window_image (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);;
  
  if (toplevel->image)
    {
      XShmDetach (xdisplay, &toplevel->shm_info);
      XSync (xdisplay, False);
      
      toplevel->image->data = NULL;
      toplevel->image->obdata = NULL;
      XDestroyImage (toplevel->image);
      toplevel->image = NULL;
      
      XFreePixmap (xdisplay, toplevel->shm_pixmap);
      toplevel->shm_pixmap = None;
      
      XFreeGC (xdisplay, toplevel->shm_gc);
      toplevel->shm_gc = None;

      lmc_bits_unref (toplevel->bits);
      toplevel->bits = NULL;
    }
}

typedef struct
{
  guint idle_id;
  GSList *windows;
} DamageInfo;

static void
destroy_damage_info (DamageInfo *info)
{
  if (info->idle_id)
    g_source_remove (info->idle_id);
  
  g_slist_free (info->windows);
  g_free (info);
}

static void
toplevel_pad_alpha (LmcToplevel *toplevel,
		    int          x,
		    int          y,
		    int          width,
		    int          height)
{
  guchar *row;
  int i, j;
  int x1 = x;
  int x2 = x + width;
  int y1 = y;
  int y2 = y + height;

  if (x1 < toplevel->border_info.left)
    x1 = toplevel->border_info.left;
  if (x2 > toplevel->border_info.left + toplevel->width)
    x2 = toplevel->border_info.left + toplevel->width;

  if (y1 < toplevel->border_info.top)
    y1 = toplevel->border_info.top;
  if (y2 > toplevel->border_info.top + toplevel->height)
    y2 = toplevel->border_info.top + toplevel->height;

  if (x1 >= x2 || y1 >= y2)
    return;
  row = toplevel->image->data + toplevel->image->bytes_per_line * y1 + 4 * x1;
  for (i = y2 - y1; i; i--)
    {
      char *p = row;

      for (j = x2 - x1; j; j--)
	{
#if G_BYTE_ORDER == G_LITTLE_ENDIAN	  
	  p[3] = 0xff;
#else	  
	  p[0] = 0xff;
#endif	  
	  p += 4;
	}

      row += toplevel->image->bytes_per_line;
    }
}

static void
undamage_window (LmcToplevel *toplevel)
{
  LmcDisplay *display = LMC_WINDOW (toplevel)->display;
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);
  Window xwindow = LMC_WINDOW_XWINDOW (toplevel);
  int n_rects;
  int i;
  int x1, y1, x2, y2;
  gboolean get_all;

  if (!toplevel->image)
    return;

  /* If we've already undamaged at least once at this size, just get the
   * part that changed. Otherwise, we get everything.
   */
  get_all = toplevel->damage_serial == 0;
  toplevel->damage_serial = NextRequest (xdisplay);

  /* Window could go away at any point */
  lmc_display_error_trap_push (display);

  if (toplevel->damage)
    XDamageSubtract (xdisplay,
		     toplevel->damage,
		     None,
		     toplevel->parts_region);

  lmc_bits_lock (toplevel->bits, TRUE);
      
  if (!get_all)
    XFixesSetGCClipRegion (xdisplay, toplevel->shm_gc,
			   toplevel->border_info.left,
			   toplevel->border_info.top,
			   toplevel->parts_region);

  XCopyArea (xdisplay,
	     xwindow,
	     toplevel->shm_pixmap,
	     toplevel->shm_gc,
	     0, 0,
	     toplevel->width, toplevel->height,
	     toplevel->border_info.left, toplevel->border_info.top);
  
  if (get_all)
    {
      XSync (xdisplay, False);
      lmc_display_error_trap_pop (display);
      lmc_bits_unlock (toplevel->bits);
      
      if (toplevel->pad_alpha)
	toplevel_pad_alpha (toplevel,
			    toplevel->border_info.left, toplevel->border_info.top,
			    toplevel->width, toplevel->height);

      g_signal_emit (toplevel, modified_signal, 0,
		     0, 0,
		     toplevel->border_info.left + toplevel->width + toplevel->border_info.right,
		     toplevel->border_info.top + toplevel->height + toplevel->border_info.bottom);
    }
  else
    {
      XRectangle *rects = XFixesFetchRegion (xdisplay, toplevel->parts_region, &n_rects);

      lmc_display_error_trap_pop (display);
      lmc_bits_unlock (toplevel->bits);

      if (rects)	/* XFixesFetchRegion returns NULL on error and
			 * leaves n_rects unchanged */
	{
	  if (n_rects)
	    {
	      if (toplevel->pad_alpha)
		{
		  for (i = 0; i < n_rects; i++)
		    toplevel_pad_alpha (toplevel,
					toplevel->border_info.left + rects[i].x, toplevel->border_info.top + rects[i].y,
					rects[i].width, rects[i].height);
		}

	      x1 = rects[0].x;
	      y1 = rects[0].y;
	      x2 = rects[0].x + rects[0].width;
	      y2 = rects[0].y + rects[0].height;
	      
	      for (i = 1; i < n_rects; i++)
		{
		  x1 = MIN (x1, rects[i].x);
		  y1 = MIN (y1, rects[i].y);
		  x2 = MAX (x2, rects[i].x + rects[i].width);
		  y2 = MAX (y2, rects[i].y + rects[i].height);
		}
	      
	      g_signal_emit (toplevel, modified_signal, 0,
			     toplevel->border_info.left + x1,
			     toplevel->border_info.top + y1,
			     x2 - x1,
			     y2 - y1);
	      
	    }
	  
	  XFree (rects);
	}
    }
}

static gboolean
undamage_windows (DamageInfo *info)
{
  GSList *windows = info->windows;
  GSList *l;
  info->windows = NULL;

  for (l = windows; l; l = l->next)
    undamage_window (l->data);

  g_slist_free (windows);

  info->idle_id = 0;

  return FALSE;
}

static DamageInfo *
get_damage_info (LmcDisplay *display,
		 gboolean    create)
{
  DamageInfo *info = g_object_get_data (G_OBJECT (display), "damage-info");
  if (!info && create)
    {
      info = g_new0 (DamageInfo, 1);
      g_object_set_data_full (G_OBJECT (display), "damage-info",
			      info, (GDestroyNotify)destroy_damage_info);
    }

  return info;
}

static void
add_damaged_window (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  DamageInfo *info = get_damage_info (lwindow->display, TRUE);

  if (toplevel->sync_alarm_connection)
    toplevel->have_sync_wait_damage = TRUE;
  else
    {
      if (!info->windows)
	info->idle_id = g_idle_add ((GSourceFunc)undamage_windows, info);
      
      if (!g_slist_find (info->windows, toplevel))
	info->windows = g_slist_prepend (info->windows, toplevel);
    }
}

static void
remove_damaged_window (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  DamageInfo *info = get_damage_info (lwindow->display, TRUE);
      
  info->windows = g_slist_remove (info->windows, toplevel);
  
  if (!info->windows && info->idle_id)
    {
      g_source_remove (info->idle_id);
      info->idle_id = 0;
    }
}

static void
initialize_damage (LmcToplevel *toplevel)
{
  create_window_image (toplevel);

  toplevel->damage_serial = 0;

  add_damaged_window (toplevel);
}

static void
lmc_toplevel_finalize (GObject *object)
{
  G_OBJECT_CLASS (lmc_toplevel_parent_class)->finalize (object);
}

static void
lmc_toplevel_class_init (LmcToplevelClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_toplevel_finalize;

  modified_signal =  g_signal_new ("modified",
				   G_OBJECT_CLASS_TYPE (object_class),
				   G_SIGNAL_RUN_LAST,
				   0,
				   NULL, NULL,
				   lmc_marshal_NONE__INT_INT_INT_INT,
				   G_TYPE_NONE, 4,
				   G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT);
  map_request_signal =  g_signal_new ("map-request",
				      G_OBJECT_CLASS_TYPE (object_class),
				      G_SIGNAL_RUN_LAST,
				      0,
				      NULL, NULL,
				      lmc_marshal_NONE__NONE,
				      G_TYPE_NONE, 0);
  size_changed_signal =  g_signal_new ("size-changed",
				       G_OBJECT_CLASS_TYPE (object_class),
				       G_SIGNAL_RUN_LAST,
				       0,
				       NULL, NULL,
				       lmc_marshal_NONE__NONE,
				       G_TYPE_NONE, 0);
  begin_move_signal =  g_signal_new ("begin-move",
				     G_OBJECT_CLASS_TYPE (object_class),
				     G_SIGNAL_RUN_LAST,
				     0,
				     NULL, NULL,
				     lmc_marshal_NONE__NONE,
				     G_TYPE_NONE, 0);
  update_move_signal =  g_signal_new ("update-move",
				      G_OBJECT_CLASS_TYPE (object_class),
				      G_SIGNAL_RUN_LAST,
				      0,
				      NULL, NULL,
				      lmc_marshal_NONE__NONE,
				      G_TYPE_NONE, 0);
  end_move_signal =  g_signal_new ("end-move",
				   G_OBJECT_CLASS_TYPE (object_class),
				   G_SIGNAL_RUN_LAST,
				   0,
				   NULL, NULL,
				   lmc_marshal_NONE__NONE,
				   G_TYPE_NONE, 0);
  position_changed_signal =  g_signal_new ("position-changed",
					   G_OBJECT_CLASS_TYPE (object_class),
					   G_SIGNAL_RUN_LAST,
					   0,
					   NULL, NULL,
					   lmc_marshal_NONE__NONE,
					   G_TYPE_NONE, 0);
  wm_name_changed_signal = g_signal_new ("wm-name-changed",
					 G_OBJECT_CLASS_TYPE (object_class),
					 G_SIGNAL_RUN_LAST,
					 0,
					 NULL, NULL,
					 lmc_marshal_NONE__NONE,
					 G_TYPE_NONE, 0);
  bits_available = g_signal_new ("bits-available",
				 G_OBJECT_CLASS_TYPE (object_class),
				 G_SIGNAL_RUN_LAST,
				 0,
				 NULL, NULL,
				 lmc_marshal_NONE__NONE,
				 G_TYPE_NONE, 0);
}

static void
lmc_toplevel_init (LmcToplevel *ltoplevel)
{
}

static void
toplevel_map_request_begin (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);
  
  toplevel->state = MAP_PENDING;

  /* We'll wait for these properties before acting on the MapRequest.
   * Note that if the last one isn't _NET_WM_NAME, on_property_changed()
   * needs to be modified correspondingly.
   */
  lmc_window_monitor_property (LMC_WINDOW (toplevel), XA_WM_HINTS);
  lmc_window_monitor_property (LMC_WINDOW (toplevel), XA_WM_NORMAL_HINTS);
  lmc_window_monitor_property (LMC_WINDOW (toplevel), XA_WM_NAME);
  lmc_window_monitor_property (LMC_WINDOW (toplevel),
			       XInternAtom (xdisplay, "WM_PROTOCOLS", False));
  lmc_window_monitor_property (LMC_WINDOW (toplevel),
			       XInternAtom (xdisplay, "_NET_WM_NAME", False));

  /* We start getting these properties, but don't wait for the response
   */
  lmc_window_monitor_property (LMC_WINDOW (toplevel),
			       XInternAtom (xdisplay, "_NET_WM_SYNC_REQUEST_COUNTER", False));
}

static gboolean
on_map_request (LmcToplevel      *toplevel,
		XMapRequestEvent *xev)
{
  toplevel_map_request_begin (toplevel);

  return TRUE;
}

static gboolean
on_map_notify (LmcToplevel  *toplevel,
	       XMapEvent    *xev)
{
  if (!toplevel->mapped)
    {
      LmcWindow *lwindow = LMC_WINDOW (toplevel);
      Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);
      
      toplevel->mapped = TRUE;

      initialize_damage (toplevel);

      toplevel->draw_state = DRAW_PENDING;
      
      lmc_window_monitor_property (LMC_WINDOW (toplevel),
				   XInternAtom (xdisplay, "_NET_WM_SYNC_REQUEST_COUNTER", False));
      lmc_window_monitor_property (LMC_WINDOW (toplevel),
				   XInternAtom (xdisplay, "WM_PROTOCOLS", False));
    }
  
  return FALSE;
}

static gboolean
on_unmap_notify (LmcToplevel  *toplevel,
		 XUnmapEvent  *xev)
{
  if (toplevel->mapped)
    {
      toplevel->mapped = FALSE;

      remove_damaged_window (toplevel);
      destroy_window_image (toplevel);
    }
    
  return FALSE;
}

static void
do_size_change (LmcToplevel *toplevel)
{
  if (toplevel->mapped)
    {
      destroy_window_image (toplevel);
      initialize_damage (toplevel);
      g_signal_emit (toplevel, size_changed_signal, 0);
    }
}

static void
do_begin_move (LmcToplevel *toplevel)
{
    g_signal_emit (toplevel, begin_move_signal, 0);
}

static void
do_update_move (LmcToplevel *toplevel)
{
    g_signal_emit (toplevel, update_move_signal, 0);
}

static void
do_end_move (LmcToplevel *toplevel)
{
    g_signal_emit (toplevel, end_move_signal, 0);
}

static void
do_position_change (LmcToplevel *toplevel)
{
  if (toplevel->sync_alarm_connection)
    toplevel->have_sync_wait_position_changed = TRUE;
  else
    g_signal_emit (toplevel, position_changed_signal, 0);
}

static gboolean
on_configure_notify (LmcToplevel     *toplevel,
		     XConfigureEvent *xev)
{
  /* We only have to worry about ConfigureNotify for windows that we don't control
   */
  if (toplevel->override_redirect)
    {
      gboolean size_changed = toplevel->width != xev->width || toplevel->height != xev->height;
      gboolean position_changed = toplevel->x != xev->x || toplevel->y != xev->y;

      toplevel->x = xev->x;
      toplevel->y = xev->y;
      toplevel->width = xev->width;
      toplevel->height = xev->height;

      if (size_changed)
	do_size_change (toplevel);
      if (position_changed)
	do_position_change (toplevel);
    }
  
  return FALSE;
}

static gboolean
on_damage_notify (LmcToplevel        *toplevel,
		  XDamageNotifyEvent *xev)
{
  if (xev->serial >= toplevel->damage_serial && toplevel->mapped)
    add_damaged_window (toplevel);
  
  return FALSE;
}

static void
sync_value_increment (XSyncValue *value)
{
  XSyncValue one;
  int overflow;
  
  XSyncIntToValue (&one, 1);
  XSyncValueAdd (value, *value, one, &overflow);
}

static gboolean
on_sync_alarm_notify (LmcDisplay            *display,
		      XSyncAlarmNotifyEvent *xev,
		      LmcToplevel           *toplevel)
{
  if (xev->alarm != toplevel->sync_alarm)
    return FALSE;

  g_signal_handler_disconnect (display, toplevel->sync_alarm_connection);
  toplevel->sync_alarm_connection = 0;

  if (toplevel->have_sync_wait_damage)
    {
      toplevel->have_sync_wait_damage = FALSE;
      undamage_window (toplevel);
    }
  
  if (toplevel->have_sync_wait_position_changed)
    {
      toplevel->have_sync_wait_position_changed = FALSE;
      do_position_change (toplevel);
    }

  if (toplevel->have_sync_wait_reconfigure)
    {
      toplevel->have_sync_wait_reconfigure = FALSE;
      lmc_toplevel_move_resize (toplevel,
				toplevel->sync_wait_x,
				toplevel->sync_wait_y,
				toplevel->sync_wait_width,
				toplevel->sync_wait_height);
    }

  if (toplevel->draw_state == WAIT_FOR_SYNC)
    {
      toplevel->draw_state = READY;
      
      g_signal_emit (toplevel, modified_signal, 0,
		     0, 0, toplevel->width, toplevel->height);
      g_signal_emit (toplevel, bits_available, 0);
    }
  
  return TRUE;
}

static void
send_sync_request (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);
  XClientMessageEvent xev;

  if (toplevel->sync_alarm_connection)
    return;

  xev.type = ClientMessage;
  xev.window = lwindow->xwindow;
  xev.message_type = XInternAtom (xdisplay, "WM_PROTOCOLS", False);
  xev.format = 32;
  xev.data.l[0] = XInternAtom (xdisplay, "_NET_WM_SYNC_REQUEST", False);
  xev.data.l[1] = CurrentTime; /* Harmless at least for GTK+ */
  xev.data.l[2] = XSyncValueLow32 (toplevel->sync_value);
  xev.data.l[3] = XSyncValueHigh32 (toplevel->sync_value);
  xev.data.l[4] = 0;

  sync_value_increment (&toplevel->sync_value);
  XSendEvent (xdisplay, lwindow->xwindow, False, 0, (XEvent *)&xev);

  toplevel->sync_alarm_connection = g_signal_connect (lwindow->display, "event::SyncAlarmNotify",
                                                     G_CALLBACK (on_sync_alarm_notify), toplevel);

  toplevel->have_sync_wait_reconfigure = FALSE;
  toplevel->have_sync_wait_position_changed = FALSE;
  toplevel->have_sync_wait_damage = FALSE;
}

static void
on_property_changed (LmcToplevel *toplevel,
		     Atom         property)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);

  if (property == XA_WM_HINTS)
    {
      if (toplevel->wm_hints)
	{
	  g_free (toplevel->wm_hints);
	  toplevel->wm_hints = NULL;
	}
    }
  else if (property == XA_WM_NORMAL_HINTS)
    {
      if (toplevel->wm_normal_hints)
	{
	  g_free (toplevel->wm_normal_hints);
	  toplevel->wm_normal_hints = NULL;
	}
    }
  else if (property == XA_WM_NAME ||
	   property == XInternAtom (xdisplay, "_NET_WM_NAME", False))
    {
      if (toplevel->wm_name)
	{
	  g_free (toplevel->wm_name);
	  toplevel->wm_name = NULL;
	}
      g_signal_emit (toplevel, wm_name_changed_signal, 0);
    }

  /* _NWT_WM_NAME is the last property we require when mapping a window
   */
  if (toplevel->state == MAP_PENDING &&
      property == XInternAtom (xdisplay, "_NET_WM_NAME", False))
    {
      toplevel->state = MAP_REQUEST;
      g_signal_emit (toplevel, map_request_signal, 0);
    }

  if (toplevel->draw_state == DRAW_PENDING &&
      property == XInternAtom (xdisplay, "WM_PROTOCOLS", False))
    {
      if (toplevel_initialize_sync_counter (toplevel))
	{
	  send_sync_request (toplevel);
	  lmc_toplevel_send_configure_notify (toplevel);
	  toplevel->draw_state = WAIT_FOR_SYNC;
	}
      else
	{
	  toplevel->draw_state = READY;
	  g_signal_emit (toplevel, modified_signal, 0,
			 0, 0, toplevel->width, toplevel->height);
	  g_signal_emit (toplevel, bits_available, 0);
	}
    }
}

LmcToplevel *
lmc_toplevel_new (LmcDisplay *ldisplay,
		  Window      xwindow)
{
  Display *xdisplay = ldisplay->xdisplay;
  XWindowAttributes attr;

  LmcToplevel *toplevel;

  lmc_display_error_trap_push (ldisplay);

  /* We've already selected for SubstructureNotify on the parent, so we'll
   * have an up-to-date view of the windows geometry from here on.
   */
  if (!XGetWindowAttributes (xdisplay, xwindow, &attr) ||
      attr.class == InputOnly)
    {
      lmc_display_error_trap_pop (ldisplay);
      return NULL;
    }
  
  toplevel = g_object_new (LMC_TYPE_TOPLEVEL,
			   "display", ldisplay,
			   "xid",     xwindow,
			   NULL);

  toplevel->x = attr.x;
  toplevel->y = attr.y;
  toplevel->width = attr.width;
  toplevel->height = attr.height;
  toplevel->depth = attr.depth;
  toplevel->visual = attr.visual;
  toplevel->override_redirect = attr.override_redirect;

  /* The easiest way to handle already mapped windows is to unmap them and
   * pretend that the application just showed them.
   */
  toplevel->mapped = FALSE;
  if (attr.map_state != IsUnmapped)
    {
      LmcAsyncErrorTrap *trap = lmc_async_error_trap_set (xdisplay);

      XUnmapWindow (xdisplay, xwindow);
      lmc_async_error_trap_unset (trap);
      
      toplevel_map_request_begin (toplevel);
    }
  else
    toplevel->state = WITHDRAWN;
    
  toplevel->damage = XDamageCreate (xdisplay, xwindow, XDamageReportNonEmpty);

  /* We need to sync here or we won't know whether toplevel->damage has been created
   * sucessfully
   */
  XSync (xdisplay, False);
  if (lmc_display_error_trap_pop (ldisplay) != Success)
    toplevel->damage = None;
  
  toplevel->parts_region = XFixesCreateRegion (xdisplay, 0, 0);
  
  g_signal_connect (toplevel, "event::MapRequest",      G_CALLBACK (on_map_request), NULL);
  g_signal_connect_after (toplevel, "event::MapNotify",       G_CALLBACK (on_map_notify), NULL);
  g_signal_connect (toplevel, "event::UnmapNotify",     G_CALLBACK (on_unmap_notify), NULL);
  g_signal_connect (toplevel, "event::ConfigureNotify", G_CALLBACK (on_configure_notify), NULL);
  g_signal_connect (toplevel, "event::DamageNotify",    G_CALLBACK (on_damage_notify), NULL);
  g_signal_connect (toplevel, "property-changed",    G_CALLBACK (on_property_changed), NULL);

  if (toplevel->mapped)
    initialize_damage (toplevel);

  toplevel->draw_state = UNMAPPED;

  return toplevel;
}

LmcBits *
lmc_toplevel_get_bits (LmcToplevel *toplevel)
{
  if (toplevel->draw_state != READY)
      return NULL;
  
  return toplevel->bits;
}

gboolean
lmc_toplevel_is_mapped (LmcToplevel *toplevel)
{
  return toplevel->mapped;
}

gboolean
lmc_toplevel_is_override_redirect (LmcToplevel *toplevel)
{
  return toplevel->override_redirect;
}

gboolean
lmc_toplevel_is_resizable (LmcToplevel *toplevel)
{
  const XSizeHints *hints = lmc_toplevel_get_wm_normal_hints (toplevel);
  int min_width = 0, min_height = 0;

  if (!hints)
    return TRUE;
  
  if (hints->flags & PMinSize)
    {
      min_width = hints->min_width;
      min_height = hints->min_height;
    }
  else if (hints->flags & PBaseSize)
    {
      min_width = hints->base_width;
      min_height = hints->base_height;
    }

  if (hints->flags & PMaxSize)
    {
      if (hints->max_width == min_width &&
	  hints->max_height == min_height)
	return FALSE;
    }

  return TRUE;
}

void
lmc_toplevel_get_mouse_position (LmcToplevel *toplevel,
				 int         *mouse_x,
				 int         *mouse_y)
{
  if (mouse_x)
    *mouse_x = toplevel->mouse_x;
  if (mouse_y)
    *mouse_y = toplevel->mouse_y;
}

void
lmc_toplevel_get_position (LmcToplevel *toplevel,
			   int         *x,
			   int         *y)
{
  if (x)
    *x = toplevel->x;
  if (y)
    *y = toplevel->y;
}

void
lmc_toplevel_get_set_position (LmcToplevel *toplevel,
			       int         *x,
			       int         *y)
{
  if (x)
    {
      if (toplevel->have_sync_wait_reconfigure)
	*x = toplevel->sync_wait_x;
      else
	*x = toplevel->x;
    }
  if (y)
    {
      if (toplevel->have_sync_wait_reconfigure)
	*y = toplevel->sync_wait_y;
      else
	*y = toplevel->y;
    }
}

void
lmc_toplevel_set_position (LmcToplevel *toplevel,
			   int          x,
			   int          y)
{
  lmc_toplevel_move_resize (toplevel,
			    x + toplevel->border_info.left,
			    y + toplevel->border_info.top,
			    toplevel->width, toplevel->height);
}

void
lmc_toplevel_constrain_size (LmcToplevel *toplevel,
			     int          width,
			     int          height,
			     int         *new_width,
			     int         *new_height)
{
  const XSizeHints *hints;

  /* Before the toplevel is mapped, we don't have authoritative size hints
   */
  if (toplevel->state != MAP_REQUEST && toplevel->state != MAPPED)
    return;
  
  hints = lmc_toplevel_get_wm_normal_hints (toplevel);

  /* Ater gdk_window_constrain_size(), which is partially borrowed from fvwm.
   *
   * Copyright 1993, Robert Nation
   *     You may use this code for any purpose, as long as the original
   *     copyright remains in the source code and all documentation
   *
   * which in turn borrows parts of the algorithm from uwm
   */
  int min_width = 0;
  int min_height = 0;
  int base_width = 0;
  int base_height = 0;
  int xinc = 1;
  int yinc = 1;
  int max_width = G_MAXSHORT;
  int max_height = G_MAXSHORT;
  
  if (!hints)
    {
      *new_width = width;
      *new_height = height;

      return;
    }
  
#define FLOOR(value, base)	( ((gint) ((value) / (base))) * (base) )
#define FLOOR64(value, base)	( ((gint64) ((value) / (base))) * (base) )

  if ((hints->flags & PBaseSize) && (hints->flags & PMinSize))
    {
      base_width = hints->base_width;
      base_height = hints->base_height;
      min_width = hints->min_width;
      min_height = hints->min_height;
    }
  else if (hints->flags & PBaseSize)
    {
      base_width = hints->base_width;
      base_height = hints->base_height;
      min_width = hints->base_width;
      min_height = hints->base_height;
    }
  else if (hints->flags & PMinSize)
    {
      base_width = hints->min_width;
      base_height = hints->min_height;
      min_width = hints->min_width;
      min_height = hints->min_height;
    }

  if (hints->flags & PMaxSize)
    {
      max_width = hints->max_width ;
      max_height = hints->max_height;
    }

  if (hints->flags & PResizeInc)
    {
      xinc = MAX (xinc, hints->width_inc);
      yinc = MAX (yinc, hints->height_inc);
    }
  
  /* clamp width and height to min and max values
   */
  width = CLAMP (width, min_width, max_width);
  height = CLAMP (height, min_height, max_height);
  
  /* shrink to base + N * inc
   */
  width = base_width + FLOOR (width - base_width, xinc);
  height = base_height + FLOOR (height - base_height, yinc);

  /* constrain aspect ratio, according to:
   *
   * min_aspect.x       width      max_aspect.x
   * ------------  <= -------- <=  -----------
   * min_aspect.y       height     max_aspect.y
   */
  
  if (hints->flags & PAspect &&
      hints->min_aspect.y > 0 && hints->max_aspect.x > 0)
    {
      /* Use 64 bit arithmetic to prevent overflow */
      
      gint64 min_aspect_x = hints->min_aspect.x;
      gint64 min_aspect_y = hints->min_aspect.y;
      gint64 max_aspect_x = hints->max_aspect.x;
      gint64 max_aspect_y = hints->max_aspect.y;
      
      gint64 delta;

      if (min_aspect_x * height > width * min_aspect_y)
	{
	  delta = FLOOR64 (height - width * min_aspect_y / min_aspect_x, yinc);
	  if (height - delta >= min_height)
	    height -= delta;
	  else
	    { 
	      delta = FLOOR64 (height * min_aspect_x / min_aspect_y - width, xinc);
	      if (width + delta <= max_width) 
		width += delta;
	    }
	}
      
      if (width * max_aspect_y > max_aspect_x * height)
	{
	  delta = FLOOR64 (width - height * max_aspect_x / max_aspect_y, xinc);
	  if (width - delta >= min_width) 
	    width -= delta;
	  else
	    {
	      delta = FLOOR64 (width * min_aspect_y / max_aspect_y - height, yinc);
	      if (height + delta <= max_height)
		height += delta;
	    }
	}
    }

#undef FLOOR
#undef FLOOR64
  
  *new_width = width;
  *new_height = height;
}

void
lmc_toplevel_set_border_info (LmcToplevel         *toplevel,
			      const LmcBorderInfo *border_info)
{
  if (border_info != NULL)
    {
      toplevel->border_info = *border_info;
    }
  else
    {
      toplevel->border_info.left = 0;
      toplevel->border_info.right = 0;
      toplevel->border_info.top = 0;
      toplevel->border_info.bottom = 0;
    }
}

void lmc_toplevel_get_border_info (LmcToplevel         *toplevel,
				   LmcBorderInfo       *border_info)
{
  *border_info = toplevel->border_info;
}

void
lmc_toplevel_begin_move (LmcToplevel *toplevel, int source_x, int source_y)
{
  toplevel->mouse_x = source_x;
  toplevel->mouse_y = source_y;

  do_begin_move (toplevel);
}

void
lmc_toplevel_update_move (LmcToplevel *toplevel, int source_x, int source_y)
{
  toplevel->mouse_x = source_x;
  toplevel->mouse_y = source_y;

  do_update_move (toplevel);
}

void
lmc_toplevel_end_move (LmcToplevel *toplevel)
{
  do_end_move (toplevel);    
}

void
lmc_toplevel_move_resize (LmcToplevel *toplevel,
			  int          x,
			  int          y,
			  int          width,
			  int          height)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);
  LmcAsyncErrorTrap *trap;
  gboolean size_changed;
  gboolean position_changed;

  lmc_toplevel_constrain_size (toplevel, width, height, &width, &height);

  if (toplevel->sync_alarm_connection)
    {
      toplevel->have_sync_wait_reconfigure = TRUE;
      toplevel->sync_wait_x = x;
      toplevel->sync_wait_y = y;
      toplevel->sync_wait_width = width;
      toplevel->sync_wait_height = height;
      return;
    }
  
  size_changed = width != toplevel->width || height != toplevel->height;
  position_changed = x != toplevel->x || y != toplevel->y;

  if (!size_changed && !position_changed)
    return;

  trap = lmc_async_error_trap_set (xdisplay);

  if (size_changed && toplevel_initialize_sync_counter (toplevel))
    {
      send_sync_request (toplevel);
      
      toplevel->have_sync_wait_reconfigure = FALSE;
      toplevel->sync_wait_width = width;
      toplevel->sync_wait_height = height;
      toplevel->sync_wait_x = toplevel->x;
      toplevel->sync_wait_y = toplevel->y;
    }

  if (size_changed && position_changed)
    XMoveResizeWindow (xdisplay, lwindow->xwindow, x, y, width, height);
  else if (size_changed)
    XResizeWindow (xdisplay, lwindow->xwindow, width, height);
  else
    XMoveWindow (xdisplay, lwindow->xwindow, x, y);
  
  lmc_async_error_trap_unset (trap);
  
  if (!toplevel->override_redirect)
    {
      toplevel->x = x;
      toplevel->y = y;
      toplevel->width = width;
      toplevel->height = height;

      if (size_changed)
	do_size_change (toplevel);
      if (position_changed)
	do_position_change (toplevel);
    }
}

void
lmc_toplevel_set_size (LmcToplevel *toplevel,
		       int          width,
		       int          height)
{
  lmc_toplevel_move_resize (toplevel,
			    toplevel->x, toplevel->y, toplevel->width, toplevel->height);
}

void
lmc_toplevel_map (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);

  if (toplevel->state == MAP_REQUEST)
    {
      /* Constrain the size of the window if necessary before actually
       * mapping the window
       */

      lmc_toplevel_set_size (toplevel, toplevel->width, toplevel->height);
    }

  XMapWindow (xdisplay, lwindow->xwindow);
  toplevel->state = MAPPED;
}

void
lmc_toplevel_get_size (LmcToplevel *toplevel,
		       int         *width,
		       int         *height)
{
  if (width)
    *width = toplevel->width;
  if (height)
    *height = toplevel->height;
}

void
lmc_toplevel_get_set_size (LmcToplevel *toplevel,
			   int         *width,
			   int         *height)
{
  if (width)
    {
      if (toplevel->have_sync_wait_reconfigure)
	*width = toplevel->sync_wait_width;
      else
	*width = toplevel->width;
    }
  if (height)
    {
      if (toplevel->have_sync_wait_reconfigure)
	*height = toplevel->sync_wait_height;
      else
	*height = toplevel->height;
    }
}

void
lmc_toplevel_raise (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);;
  
  XRaiseWindow (xdisplay, lwindow->xwindow);
}

void
lmc_toplevel_lower (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);;

  XLowerWindow (xdisplay, lwindow->xwindow);
}

gboolean
lmc_toplevel_grab_focus (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);;
  LmcAsyncErrorTrap *trap;
  const XWMHints *hints;
  gboolean has_wm_take_focus;
  gboolean has_input;

  hints = lmc_toplevel_get_wm_hints (toplevel);
  if (hints && hints->flags & InputHint)
    has_input = hints->input;
  else
    has_input = TRUE;

  has_wm_take_focus = lmc_toplevel_has_protocol (toplevel,
						 XInternAtom (xdisplay, "WM_TAKE_FOCUS", False));

  if (!has_wm_take_focus && !has_input)
    return FALSE;

  trap = lmc_async_error_trap_set (xdisplay);

  if (has_input)
    XSetInputFocus (xdisplay, lwindow->xwindow, RevertToPointerRoot, CurrentTime);

  if (has_wm_take_focus)
    {
      XClientMessageEvent xev;

      xev.type = ClientMessage;
      xev.window = lwindow->xwindow;
      xev.message_type = XInternAtom (xdisplay, "WM_PROTOCOLS", False);
      xev.format = 32;
      xev.data.l[0] = XInternAtom (xdisplay, "WM_TAKE_FOCUS", False);
      xev.data.l[1] = CurrentTime; /* Harmless at least for GTK+ */
      xev.data.l[2] = 0;
      xev.data.l[3] = 0;
      xev.data.l[4] = 0;

      XSendEvent (xdisplay, lwindow->xwindow, False, 0, (XEvent *)&xev);
    }
  
  lmc_async_error_trap_unset (trap);

  return TRUE;
}

void
lmc_toplevel_delete (LmcToplevel *toplevel)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);;
  LmcAsyncErrorTrap *trap;
  gboolean has_wm_delete_window;
    
  has_wm_delete_window = lmc_toplevel_has_protocol (toplevel,
						    XInternAtom (xdisplay, "WM_DELETE_WINDOW", False));

  trap = lmc_async_error_trap_set (xdisplay);

  if (has_wm_delete_window)
    {
      XClientMessageEvent xev;

      xev.type = ClientMessage;
      xev.window = lwindow->xwindow;
      xev.message_type = XInternAtom (xdisplay, "WM_PROTOCOLS", False);
      xev.format = 32;
      xev.data.l[0] = XInternAtom (xdisplay, "WM_DELETE_WINDOW", False);
      xev.data.l[1] = CurrentTime; /* Blech */
      xev.data.l[2] = 0;
      xev.data.l[3] = 0;
      xev.data.l[4] = 0;

      XSendEvent (xdisplay, lwindow->xwindow, False, 0, (XEvent *)&xev);
    }
  else
    XKillClient (xdisplay, lwindow->xwindow);

  lmc_async_error_trap_unset (trap);
}

void
lmc_toplevel_send_configure_notify (LmcToplevel *toplevel)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);
  Window xwindow = LMC_WINDOW_XWINDOW (toplevel);
  LmcAsyncErrorTrap *trap = lmc_async_error_trap_set (xdisplay);
  XConfigureEvent xev;

  xev.type = ConfigureNotify;
  xev.event = xwindow;
  xev.window = xwindow;
  xev.x = toplevel->x;
  xev.y = toplevel->y;
  xev.width = toplevel->width;
  xev.height = toplevel->height;
  xev.border_width = 0;

  /* The ICCCM specifies that clients should ignore this field, because
   * of reparenting window managers. Even though we aren't one, we
   * take advantage of that and don't go to the trouble of computing the
   * value.
   */
  xev.above = None;
  xev.override_redirect = toplevel->override_redirect;

  XSendEvent (xdisplay, xwindow, False, StructureNotifyMask, (XEvent *)&xev);
  
  lmc_async_error_trap_unset (trap);
}

typedef struct
{
  int idx;
  long flag;
  enum { FIELD_LONG, FIELD_INT, FIELD_XID } type;
  off_t offset;
} FieldInfo;

static gboolean
parse_format32_property (const FieldInfo  *fields,
			 int               n_fields,
			 long              flags,
			 LmcPropertyValue *value,
			 guchar           *result)
{
  int i;
  
  for (i = 0; i < n_fields; i++)
    {
      if (!fields[i].flag || (flags & fields[i].flag) != 0)
	{
	  if (fields[i].idx >= value->n_items)
	    return FALSE;
	  
	  switch (fields[i].type)
	    {
	    case FIELD_LONG:
	      {
		long *location = (long *)(result + fields[i].offset);
		*location = value->data.l[fields[i].idx];
		break;
	      }
	    case FIELD_INT:
	      {
		int *location = (int *)(result + fields[i].offset);
		*location = value->data.l[fields[i].idx];
		break;
	      }
	    case FIELD_XID:
	      {
		XID *location = (XID *)(result + fields[i].offset);
		*location = value->data.l[fields[i].idx];
		break;
	      }
	    }
	}
    }

  return TRUE;
}

const XWMHints *
lmc_toplevel_get_wm_hints (LmcToplevel *toplevel)
{
  static const FieldInfo fields[] = {
    {  0, 0,                FIELD_LONG, G_STRUCT_OFFSET (XWMHints, flags) },
    {  1, InputHint,        FIELD_INT,  G_STRUCT_OFFSET (XWMHints, input) },
    {  2, StateHint,        FIELD_INT,  G_STRUCT_OFFSET (XWMHints, initial_state) },
    {  3, IconPixmapHint,   FIELD_XID,  G_STRUCT_OFFSET (XWMHints, icon_pixmap) },
    {  4, IconWindowHint,   FIELD_XID,  G_STRUCT_OFFSET (XWMHints, icon_window) },
    {  5, IconPositionHint, FIELD_INT,  G_STRUCT_OFFSET (XWMHints, icon_x) },
    {  6, IconPositionHint, FIELD_INT,  G_STRUCT_OFFSET (XWMHints, icon_y) },
    {  7, IconMaskHint,     FIELD_XID,  G_STRUCT_OFFSET (XWMHints, icon_mask) },
    {  8, WindowGroupHint,  FIELD_XID,  G_STRUCT_OFFSET (XWMHints, window_group) },
  };

  if (!toplevel->wm_hints)
    {
      LmcPropertyValue *value = lmc_window_get_property_value (LMC_WINDOW (toplevel), XA_WM_HINTS);
      if (value && value->type == XA_WM_SIZE_HINTS && value->format == 32 && value->n_items >= 1)
	{
	  toplevel->wm_hints = g_new0 (XWMHints, 1);
	  
	  if (!parse_format32_property (fields,
					G_N_ELEMENTS (fields),
					value->data.l[0], value,
					(guchar *)toplevel->wm_hints))
	    {
	      g_free (toplevel->wm_hints);
	      toplevel->wm_hints = NULL;
	    }
	}
    }

  return toplevel->wm_hints;
}

const XSizeHints *
lmc_toplevel_get_wm_normal_hints (LmcToplevel *toplevel)
{
  static const FieldInfo fields[] = {
    {  0, 0,           FIELD_LONG, G_STRUCT_OFFSET (XSizeHints, flags) },
    {  5, PMinSize,    FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, min_width) },
    {  6, PMinSize,    FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, min_height) },
    {  7, PMaxSize,    FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, max_width) },
    {  8, PMaxSize,    FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, max_height) },
    {  9, PResizeInc,  FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, width_inc) },
    { 10, PResizeInc,  FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, height_inc) },
    { 11, PAspect,     FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, min_aspect.x) },
    { 12, PAspect,     FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, min_aspect.y) },
    { 13, PAspect,     FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, max_aspect.x) },
    { 14, PAspect,     FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, max_aspect.y) },
    { 15, PBaseSize,   FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, base_width) },
    { 16, PBaseSize,   FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, base_height) },
    { 15, PWinGravity, FIELD_INT,  G_STRUCT_OFFSET (XSizeHints, base_width) },
  };

  if (!toplevel->wm_normal_hints)
    {
      LmcPropertyValue *value = lmc_window_get_property_value (LMC_WINDOW (toplevel), XA_WM_NORMAL_HINTS);
      if (value && value->type == XA_WM_SIZE_HINTS && value->format == 32 && value->n_items >= 1)
	{
	  toplevel->wm_normal_hints = g_new0 (XSizeHints, 1);
	  
	  if (!parse_format32_property (fields,
					G_N_ELEMENTS (fields),
					value->data.l[0], value,
					(guchar *)toplevel->wm_normal_hints))
	    {
	      g_free (toplevel->wm_normal_hints);
	      toplevel->wm_normal_hints = NULL;
	    }
	}
    }

  return toplevel->wm_normal_hints;
}

const gchar *
lmc_toplevel_get_wm_name (LmcToplevel *toplevel)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);
  
  if (!toplevel->wm_name)
    {
      LmcPropertyValue *value;

      value = lmc_window_get_property_value (LMC_WINDOW (toplevel), XInternAtom (xdisplay, "_NET_WM_NAME", False));
      if (value && value->type == XInternAtom (xdisplay, "UTF8_STRING", False) && value->format == 8 &&
	  g_utf8_validate ((char *)value->data.b, -1, NULL))
	{
	  toplevel->wm_name = g_strdup ((char *)value->data.b);
	  return toplevel->wm_name;
	}
      
      value = lmc_window_get_property_value (LMC_WINDOW (toplevel), XA_WM_NAME);
      if (value && value->type == XA_STRING && value->format == 8)
	{
	  toplevel->wm_name = g_convert ((char *)value->data.b, -1,
					 "UTF-8", "ISO-8859-1",
					 NULL, NULL, NULL);
	}
    }
  
  return toplevel->wm_name;
}

gboolean
lmc_toplevel_has_protocol (LmcToplevel *toplevel,
			   Atom         protocol)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);
  LmcPropertyValue *value;
  
  value = lmc_window_get_property_value (LMC_WINDOW (toplevel), XInternAtom (xdisplay, "WM_PROTOCOLS", False));
  if (value && value->type == XA_ATOM && value->format == 32)
    {
      gulong i;
      
      for (i = 0; i < value->n_items; i++)
	{
	  if (value->data.l[i] == protocol)
	    return TRUE;
	}
    }

  return FALSE;
}

static gboolean
toplevel_initialize_sync_counter (LmcToplevel *toplevel)
{
  XSyncAlarmAttributes values;
  LmcDisplay *ldisplay = LMC_WINDOW (toplevel)->display;
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);
  LmcPropertyValue *value;
  
  if (toplevel->sync_counter)
    return toplevel->sync_alarm != None;
  
  if (!lmc_toplevel_has_protocol (toplevel,
				  XInternAtom (xdisplay, "_NET_WM_SYNC_REQUEST", False)))
    return FALSE;
  
  value = lmc_window_get_property_value (LMC_WINDOW (toplevel),
					 XInternAtom (xdisplay, "_NET_WM_SYNC_REQUEST_COUNTER", False));
  if (!value || value->type != XA_CARDINAL || value->format != 32 ||
      value->data.l[0] == None)
    return FALSE;

  toplevel->sync_counter = value->data.l[0];

  lmc_display_error_trap_push (ldisplay);

  XSyncIntsToValue (&toplevel->sync_value,
		    (guint) g_random_int_range (G_MININT, G_MAXINT),
		    0);
  XSyncSetCounter (xdisplay, toplevel->sync_counter, toplevel->sync_value);

  sync_value_increment (&toplevel->sync_value);

  values.events = True;
  values.trigger.counter = toplevel->sync_counter;
  values.trigger.wait_value = toplevel->sync_value;
  
  values.trigger.value_type = XSyncAbsolute;
  values.trigger.test_type = XSyncPositiveComparison;
  XSyncIntToValue (&values.delta, 1);
  values.events = True;

  /* Note that by default, the alarm increments the trigger value
   * when it fires until the condition (counter.value < trigger.value)
   * is FALSE again.
   */
  toplevel->sync_alarm = XSyncCreateAlarm (xdisplay,
					   XSyncCACounter | XSyncCAValue | XSyncCAValueType | XSyncCATestType | XSyncCADelta | XSyncCAEvents,
					   &values);
  XSync (xdisplay, False);

  if (lmc_display_error_trap_pop (ldisplay))
    {
      LmcAsyncErrorTrap *trap = lmc_async_error_trap_set (xdisplay);
      XSyncDestroyAlarm (xdisplay, toplevel->sync_alarm);
      lmc_async_error_trap_unset (trap);

      toplevel->sync_alarm = None;
    }

  return TRUE;
}

static void
update_state (LmcToplevel *toplevel)
{
  LmcWindow *window = LMC_WINDOW (toplevel);
  Display *xdisplay = LMC_WINDOW_XDISPLAY (toplevel);

  Atom state = XInternAtom (xdisplay, "_NET_WM_STATE", False);
  Atom fullscreen = XInternAtom (xdisplay, "_NET_WM_STATE_FULLSCREEN", False);

  if (toplevel->fullscreen)
    XChangeProperty (xdisplay, window->xwindow, state, XA_ATOM, 32,
		     PropModeReplace, (gchar *)&fullscreen, 1);
  else
    XChangeProperty (xdisplay, window->xwindow, state, XA_ATOM, 32,
		     PropModeReplace, NULL, 0);
}

gboolean
lmc_toplevel_get_fullscreen (LmcToplevel *toplevel)
{
  return toplevel->fullscreen;
}

void
lmc_toplevel_set_fullscreen (LmcToplevel *toplevel,
			     gboolean fullscreen)
{
  fullscreen = !!fullscreen;
  
  if (fullscreen == toplevel->fullscreen)
    return;

  toplevel->fullscreen = fullscreen;

  update_state (toplevel);
}
