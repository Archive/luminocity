#ifndef __LMC_DISPLAY_H__
#define __LMC_DISPLAY_H__

#include <glib-object.h>
#include <X11/Xlib.h>

G_BEGIN_DECLS

#define LMC_TYPE_DISPLAY            (lmc_display_get_type ())
#define LMC_DISPLAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_DISPLAY, LmcDisplay))
#define LMC_DISPLAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_DISPLAY, LmcDisplayClass))
#define LMC_IS_DISPLAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_DISPLAY))
#define LMC_IS_DISPLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_DISPLAY))
#define LMC_DISPLAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_DISPLAY, LmcDisplayClass))

typedef struct _LmcWindow        LmcWindow;
typedef struct _LmcDisplay       LmcDisplay;
typedef struct _LmcDisplayClass  LmcDisplayClass;

typedef enum
{
  LMC_EXTENSION_FIXES     = 1 << 0,
  LMC_EXTENSION_DAMAGE    = 1 << 1,
  LMC_EXTENSION_COMPOSITE = 1 << 2,
  LMC_EXTENSION_TEST      = 1 << 3,
  LMC_EXTENSION_SYNC      = 1 << 3
} LmcExtension;

struct _LmcDisplay
{
  GObject parent_instance;

  Display *xdisplay;
  
  GHashTable *windows;
  GMainContext *main_context;
  
  LmcExtension extensions;
  guint super_modifier;
  
  int xfixes_event_base;
  int xfixes_error_base;
  int damage_event_base;
  int damage_error_base;
  int composite_event_base;
  int composite_error_base;
  int test_event_base;
  int test_error_base;
  int sync_event_base;
  int sync_error_base;
};

struct _LmcDisplayClass
{
  GObjectClass parent_class;
};

GType	    lmc_display_get_type  (void) G_GNUC_CONST;

LmcDisplay *lmc_display_new (Display *xdisplay);

void       lmc_display_attach        (LmcDisplay   *ldisplay,
				      GMainContext *context);
LmcWindow *lmc_display_lookup        (LmcDisplay   *ldisplay,
				      Window        xwindow);
void       lmc_display_add_window    (LmcDisplay   *ldisplay,
				      LmcWindow    *lwindow);
void       lmc_display_remove_window (LmcDisplay   *ldisplay,
				      LmcWindow    *lwindow);

LmcExtension lmc_display_get_extensions     (LmcDisplay *ldisplay);
guint        lmc_display_get_super_modifier (LmcDisplay *ldisplay);

void lmc_display_error_trap_push (LmcDisplay *display);
int  lmc_display_error_trap_pop  (LmcDisplay *display);

void lmc_display_do_event (LmcDisplay  *ldisplay,
			   XEvent      *xev,
			   const char  *detail);

G_END_DECLS

#endif /* __LMC_DISPLAY_H__ */
