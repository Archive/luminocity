#include "async.h"
#include "window.h"
#include "lmc-marshal.h"

enum {
  PROP_0,
  PROP_DISPLAY,
  PROP_XID,
};

static void     window_free_properties (LmcWindow      *lwindow);
static gboolean on_property_notify     (LmcWindow      *lwindow,
					XPropertyEvent *xev);

static guint event_signal;
static guint property_changed_signal;
     
G_DEFINE_TYPE (LmcWindow, lmc_window, G_TYPE_OBJECT)

static void
lmc_window_finalize (GObject *object)
{
  LmcWindow *lwindow = LMC_WINDOW (object);

  window_free_properties (lwindow);

  if (lwindow->xwindow)
    lmc_display_remove_window (lwindow->display, lwindow);

  G_OBJECT_CLASS (lmc_window_parent_class)->finalize (object);
}

static void
lmc_window_set_property (GObject         *object,
			 guint            prop_id,
			 const GValue    *value,
			 GParamSpec      *pspec)
{
  LmcWindow *window = LMC_WINDOW (object);

  switch (prop_id)
    {
    case PROP_DISPLAY:
      window->display = g_value_get_object (value);
      break;
    case PROP_XID:
      window->xwindow = g_value_get_ulong (value);
      break;
    default:
      break;
    }
}

static void
lmc_window_get_property (GObject         *object,
			 guint            prop_id,
			 GValue          *value,
			 GParamSpec      *pspec)
{
  LmcWindow *window = LMC_WINDOW (object);
  
  switch (prop_id)
    {
    case PROP_DISPLAY:
      g_value_set_object (value, window->display);
      break;
    case PROP_XID:
      g_value_set_ulong (value, window->xwindow);
      break;
    default:
      break;
    }
}

static gboolean
on_destroy_notify (LmcWindow           *lwindow,
		   XDestroyWindowEvent *xev)
{
  if (xev->window == lwindow->xwindow)
    {
      lmc_display_remove_window (lwindow->display, lwindow);
      lwindow->xwindow = None;
    }

  return FALSE;
}

static GObject*
lmc_window_constructor (GType                  type,
			guint                  n_construct_properties,
			GObjectConstructParam *construct_params)
{
  GObject *object;
  LmcWindow *lwindow;

  object = (* G_OBJECT_CLASS (lmc_window_parent_class)->constructor) (type,
								      n_construct_properties,
								      construct_params);
  
  lwindow = LMC_WINDOW (object);

  if (lwindow->xwindow == None || lwindow->display == NULL)
    g_error ("Display and XID must be specified when creating a LmcWindow");

  lmc_display_add_window (lwindow->display, lwindow);
  lmc_window_select_input (lwindow, StructureNotifyMask);

  g_signal_connect (lwindow, "event::DestroyNotify",
		    G_CALLBACK (on_destroy_notify), NULL);
  g_signal_connect (lwindow, "event::PropertyNotify",
		    G_CALLBACK (on_property_notify), NULL);
  
  return object;
}

static void
lmc_window_class_init (LmcWindowClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_window_finalize;
  object_class->set_property = lmc_window_set_property;
  object_class->get_property = lmc_window_get_property;
  object_class->constructor = lmc_window_constructor;

  event_signal =  g_signal_new ("event",
				G_OBJECT_CLASS_TYPE (object_class),
				G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
				0,
				g_signal_accumulator_true_handled, NULL,
				lmc_marshal_BOOLEAN__POINTER,
				G_TYPE_BOOLEAN, 1, G_TYPE_POINTER);
  property_changed_signal =  g_signal_new ("property-changed",
					   G_OBJECT_CLASS_TYPE (object_class),
					   G_SIGNAL_RUN_LAST,
					   0,
					   NULL, NULL,
					   lmc_marshal_NONE__ULONG,
					   G_TYPE_NONE, 1, G_TYPE_ULONG);

  g_object_class_install_property (object_class,
				   PROP_DISPLAY,
				   g_param_spec_object ("display",
							"Display",
							"Display of the window",
							LMC_TYPE_DISPLAY,
							G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class,
				   PROP_XID,
				   g_param_spec_ulong ("xid",
						       "XID",
						       "XID of window",
						       0, G_MAXLONG, None,
						       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
lmc_window_init (LmcWindow *lwindow)
{
  lwindow->event_masks = NULL;
  lwindow->event_mask = 0;
}

void
lmc_window_do_event (LmcWindow   *lwindow,
		     XEvent      *xev,
		     const char  *detail)
{
  gboolean result;
  
  g_signal_emit (lwindow, event_signal, g_quark_from_string (detail), xev, &result);
}

static void
recompute_event_mask (LmcWindow *lwindow)
{
  GList *l;
  long event_mask = 0;
  
  for (l = lwindow->event_masks; l; l = l->next)
    event_mask |= (long)l->data;

  if (event_mask != lwindow->event_mask)
    {
      XSelectInput (lwindow->display->xdisplay, lwindow->xwindow, event_mask);
      lwindow->event_mask = event_mask;
    }
}

void
lmc_window_select_input (LmcWindow *lwindow,
			 long       event_mask)
{
  GList *l;

  for (l = lwindow->event_masks; l && event_mask; l = l->next)
    {
      long old_event_mask = (long)l->data;
      long uncommon_bits = old_event_mask ^ event_mask;

      l->data = (gpointer)(old_event_mask | uncommon_bits);

      event_mask = event_mask & ~uncommon_bits;
    }

  if (event_mask != 0)
    lwindow->event_masks = g_list_prepend (lwindow->event_masks, (gpointer)event_mask);

  recompute_event_mask (lwindow);
}

void
lmc_window_unselect_input (LmcWindow *lwindow,
			   long       event_mask)
{
  GList *l;

  for (l = lwindow->event_masks; l && event_mask;)
    {
      GList *next = l->next;
      
      long old_event_mask = (long)l->data;
      long common_bits = old_event_mask & event_mask;

      if (common_bits == old_event_mask)
	lwindow->event_masks = g_list_remove (lwindow->event_masks, l);
      else
	l->data = (gpointer)(old_event_mask & ~common_bits);

      event_mask = event_mask & ~common_bits;
	
      l = next;
    }

  if (event_mask != 0)
    g_warning ("lmc_window_unselect_input() called for unselected bits");
  
  recompute_event_mask (lwindow);
}

/************************************************************************
 *                     Asynchronous property handling                   *
 ************************************************************************/
 
typedef struct _PropertyInfo      PropertyInfo;
typedef struct _PropertyRetrieval PropertyRetrieval;

struct _PropertyInfo
{
  Atom property;
  gboolean monitored;
  gboolean have_value;
  GList *retrievals;
  LmcPropertyValue value;
};

struct _PropertyRetrieval
{
  GMainContext *main_context;
  LmcWindow *lwindow;
  PropertyInfo *info;
  gulong serial;
  LmcPropertyValue value;
};

PropertyInfo *
window_get_property_info (LmcWindow *lwindow,
			  Atom       property,
			  gboolean   create)
{
  PropertyInfo *info;
  GList *l;

  for (l = lwindow->properties; l; l = l->next)
    {
      info = l->data;
      
      if (info->property == property)
	return info;
    }

 if (create)
    {
      info = g_new (PropertyInfo, 1);
      info->property = property;
      info->monitored = FALSE;
      info->have_value = FALSE;
      info->retrievals = NULL;

      lwindow->properties = g_list_prepend (lwindow->properties, info);

      return info;
    }
  else
    return NULL;
}

static void
window_free_properties (LmcWindow *lwindow)
{
  GList *l, *ll;

  for (l = lwindow->properties; l; l = l->next)
    {
      PropertyInfo *info = l->data;
      for (ll = info->retrievals; ll; ll = ll->next)
	{
	  PropertyRetrieval *retrieval = ll->data;
	  retrieval->info = NULL;
	}

      if (info->value.type != None)
	g_free (info->value.data.b);

      g_free (info);
    }

  g_list_free (lwindow->properties);
  lwindow->properties = NULL;
}

static gboolean
on_property_notify (LmcWindow      *lwindow,
		    XPropertyEvent *xev)
{
  PropertyInfo *info = window_get_property_info (lwindow, xev->atom, FALSE);

  if (!info)
    return FALSE;

  if (xev->state == PropertyNewValue)
    {
      if (info->monitored)
	lmc_window_update_property (lwindow, xev->atom);
    }
  else 				/* PropertyDeleted */
    {
      gboolean found_newer = FALSE;
      GList *l;

      /* This is tricky, because the retrieval list might contain retrievals
       * that predate or postdate the PropertyNotify
       */
      for (l = info->retrievals; l;)
	{
	  PropertyRetrieval *retrieval = l->data;
	  GList *next = l->next;

	  if (retrieval->serial > xev->serial)
	    found_newer = TRUE;
	  else
	    {
	      retrieval->info = NULL;
	      info->retrievals = g_list_remove_link (info->retrievals, l);
	    }

	  l = next;
	}

      if (!found_newer)
	{
	  if (info->value.type != None)
	    {
	      g_free (info->value.data.b);
	      info->value.data.b = NULL;
	      info->value.type = None;
	    }
	  
	  info->have_value = TRUE;
      
	  if (info->monitored)
	    g_signal_emit (lwindow, property_changed_signal, 0, info->property, 0);
	}
    }
    
  return FALSE;
}

/**
 * lmc_window_monitor_property:
 * @lwindow: an X Window
 * @property: a property name
 * 
 * Expresses interest in a particular property on the window. Immediately
 * calls lmc_window_update_property() on the property and also, in
 * the future when a PropertyNotify event is received for that property,
 * automatically calls lmc_window_update_property().
 **/
void
lmc_window_monitor_property (LmcWindow  *lwindow,
			     Atom        property)
{
  PropertyInfo *info = window_get_property_info (lwindow, property, TRUE);

  if (!info->monitored)
    info->monitored = TRUE;

  lmc_window_select_input (lwindow, PropertyChangeMask);
  lmc_window_update_property (lwindow, property);
}

/* We use idles because the async callbacks can't reenter Xlib
 * and might be fired in a different thread.
 */
static void
retrieval_add_idle (PropertyRetrieval *retrieval,
		    GSourceFunc        callback)
{
  GSource *source = g_idle_source_new ();

  g_source_set_priority (source, G_PRIORITY_DEFAULT);
  g_source_set_callback (source, callback, retrieval, NULL);
  g_source_attach (source, retrieval->main_context);
  g_source_unref (source);
}

static gboolean
get_property_error_idle (gpointer data)
{
  PropertyRetrieval *retrieval = data;
  PropertyInfo *info = retrieval->info;

  if (info)
    info->retrievals = g_list_remove (info->retrievals, retrieval);

  g_main_context_unref (retrieval->main_context);
  g_free (retrieval);

  return FALSE;			/* one-shot */
}

static Bool
get_property_error_callback (int   type,
			     void *user_data)
{
  PropertyRetrieval *retrieval = user_data;
  
  retrieval_add_idle (retrieval, get_property_error_idle);
  
  /* Ignore BadWindow, generate XError on anything else */
  return type == BadWindow;
}

static gboolean
get_property_idle (gpointer data)
{
  PropertyRetrieval *retrieval = data;
  PropertyInfo *info = retrieval->info;
  LmcWindow *lwindow;

  if (info)
    {
      info->retrievals = g_list_remove (info->retrievals, retrieval);

      if (!info->retrievals)	/* Nothing outstanding */
	{
	  info->have_value = TRUE;
	  info->value = retrieval->value;
	}
    }

  lwindow = retrieval->lwindow;
  
  g_main_context_unref (retrieval->main_context);
  g_free (retrieval);

  g_signal_emit (lwindow, property_changed_signal, 0, info->property, 0);

  return FALSE;			/* one-shot */
}

static void
get_property_callback (Atom           type,
		       int            format,
		       unsigned long  n_items,
		       unsigned char *data,
		       void          *user_data)
{
  PropertyRetrieval *retrieval = user_data;

  retrieval->value.type = type;
  retrieval->value.format = format;
  retrieval->value.n_items = n_items;
  retrieval->value.data.b = data;

  retrieval_add_idle (retrieval, get_property_idle);
}

/**
 * lmc_window_update_property:
 * @lwindow: an X window
 * @property: a property name
 * 
 * Asynchronously fetches the value of a property from the window.
 * When the property value is retrieved back from the server,
 * a ::property-changed event will be emitted. ::property-changed
 * is not emitted if a BadWindow error occurs; such errors will
 * be silently ignored.
 **/
void
lmc_window_update_property  (LmcWindow  *lwindow,
			     Atom        property)
{
  PropertyInfo *info = window_get_property_info (lwindow, property, TRUE);
  PropertyRetrieval *retrieval;

  g_return_if_fail (lwindow->display->main_context != NULL);

  if (info->have_value)
    {
      if (info->value.type != None)
	{
	  g_free (info->value.data.b);
	  info->value.data.b = NULL;
	  info->value.type = None;
	}
      
      info->have_value = FALSE;
    }

  retrieval = g_new (PropertyRetrieval, 1);
  retrieval->main_context = lwindow->display->main_context;
  g_main_context_ref (retrieval->main_context);
  retrieval->lwindow = lwindow;
  retrieval->info = info;
  retrieval->serial = NextRequest (LMC_WINDOW_XDISPLAY (lwindow));

  LmcAsyncGetProperty (LMC_WINDOW_XDISPLAY (lwindow), LMC_WINDOW_XWINDOW (lwindow),
		       property,
		       get_property_callback, get_property_error_callback,
		       retrieval);
}

/**
 * lmc_window_get_property_value:
 * @lwindow: 
 * @property: 
 * @type: location to store the type of the property. A type of None,
 *        means the property does not exist on the window.
 * @format: location to store the format (8,16,32) of the property
 * @data: location to store a pointer to the data of the property.
 *        the value stored there is owned by the window, and must
 *        not be modified or freed.
 * @n_items: location to store the number of items in the property.
 * 
 * Gets the value of a property retrieved via lmc_window_monitor_property()
 * or lmc_window_update_property().
 * 
 * Return value: Information about the property, if the property is up-to-date.
 * NULL if a call to lmc_window_update_property() is still pending.
 **/
LmcPropertyValue *
lmc_window_get_property_value (LmcWindow  *lwindow,
			       Atom        property)
{
  PropertyInfo *info = window_get_property_info (lwindow, property, FALSE);

  if (!info)
    {
      g_warning ("lmc_window_get_property() called without a preceding call to\n"
		 "lmc_window_monitor_property() or lmc_window_update_property()");
      return NULL;
    }

  if (info->have_value)
    return &info->value;
  else
    return NULL;
}
