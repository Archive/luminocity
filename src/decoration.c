#include <gdk-pixbuf/gdk-pixbuf.h>
#include <pango/pangoft2.h>

#include "decoration.h"
#include "lmc-marshal.h"
#include "utils.h"

typedef struct _LmcDecorationFactoryClass  LmcDecorationFactoryClass;

struct _LmcDecorationFactory
{
  GObject parent_instance;
  
  GdkPixbuf *pixbuf;
  PangoFontMap *font_map;
  PangoContext *context;
};

struct _LmcDecorationFactoryClass
{
  GObjectClass parent_class;
};

typedef struct _LmcDecorationClass  LmcDecorationClass;

struct _LmcDecoration
{
  GObject parent_instance;

  LmcDecorationFactory *factory;

  int window_width;
  int window_height;
  
  gboolean need_repaint;

  PangoLayout *layout;
  int title_width;

  LmcBorderInfo border;
};

struct _LmcDecorationClass
{
  GObjectClass parent_class;
};

/**********************************************************************/

typedef struct LmcActionArea LmcActionArea;
struct LmcActionArea {
  int x0, x1, y0, y1;
  LmcAction action;
};

typedef struct _LmcTheme  LmcTheme;
struct _LmcTheme {
  const char *name;
  const char *filename;
  LmcBorderInfo border_info;
  const LmcActionArea *action_areas;
  int num_action_areas;
  const LmcPixel title_color;
};

/* Matches decoration.png */
static const LmcActionArea baby_blue_action_areas[] = {
  {   6,  21,   5,   20, LMC_ACTION_MENU },
  {  22, -58,   0,   24, LMC_ACTION_MOVE },
  { -57, -42,   5,   20, LMC_ACTION_MINIMIZE },
  { -39, -24,   5,   20, LMC_ACTION_MAXIMIZE },
  { -21,  -6,   5,   20, LMC_ACTION_CLOSE },
  {  -7,   0,  22,  -17, LMC_ACTION_RESIZE_RIGHT },
  {  -7,   0, -17,    0, LMC_ACTION_RESIZE_BOTTOM_RIGHT },
  { -17,   0,  -7,    0, LMC_ACTION_RESIZE_BOTTOM_RIGHT },
  {  17, -17,  -7,    0, LMC_ACTION_RESIZE_BOTTOM },
  {   0,  17,  -7,    0, LMC_ACTION_RESIZE_BOTTOM_LEFT },
  {   0,   7,  22,  -17, LMC_ACTION_RESIZE_LEFT },
};

/* Account for the black line around the decorations in the calculation
 * of the vertical placement of titlebar text */
#define TITLE_TOP_PAD 1

/* Leave a gap to prevent title text bleeding into the blank area to
 * the right of the title
 */
#define TITLE_RIGHT_PAD 2

/* Horizontal and vertical offset for title text shadow */
#define TITLE_SHADOW_OFFSET 2

static const LmcTheme baby_blue_theme = {
  "Baby Blue",
  "decoration.png",
  {
    /*  left right top bottom */
    7,    7,  24,     7,	/* border */
    24,   60,  24,     19,	/* unscaled size */
  },
  baby_blue_action_areas,
  G_N_ELEMENTS (baby_blue_action_areas),
  {
    0xff, 0xff, 0xff, 0xff
  }
};

static const LmcTheme swoosh_theme = {
  "Swoosh",
  "swoosh.png",
  {
    /*  left right top bottom */
    7,    7,  32,     7,	/* border */
    24,   60,  32,     19,	/* unscaled size */
  },
  baby_blue_action_areas,
  G_N_ELEMENTS (baby_blue_action_areas),
  {
    0xff, 0xff, 0xff, 0xff
  }
};

static const LmcTheme clearlooks_theme = {
  "ClearLooks",
  "clearlooks.png",
  {
    /*  left right top bottom */
    4,    4,   25,     4,	/* border */
    24,   68,  26,     19,	/* unscaled size */
  },
  baby_blue_action_areas,
  G_N_ELEMENTS (baby_blue_action_areas),
  {
    0xff, 0xff, 0xff, 0xff
  }
};

static const LmcTheme *lmc_theme = &clearlooks_theme;

/**********************************************************************/

G_DEFINE_TYPE (LmcDecorationFactory, lmc_decoration_factory, G_TYPE_OBJECT)

static void
lmc_decoration_factory_finalize (GObject *object)
{
  LmcDecorationFactory *factory = LMC_DECORATION_FACTORY (object);

  g_object_unref (factory->context);
  g_object_unref (factory->font_map);
  g_object_unref (factory->pixbuf);
  
  G_OBJECT_CLASS (lmc_decoration_factory_parent_class)->finalize (object);
}

static void
lmc_decoration_factory_class_init (LmcDecorationFactoryClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_decoration_factory_finalize;
}

static void
lmc_decoration_factory_init (LmcDecorationFactory *decoration_factory)
{
}

LmcDecorationFactory *
lmc_decoration_factory_new (void)
{
  LmcDecorationFactory *factory;
  GdkPixbuf *pixbuf;
  PangoFontDescription *desc;

  pixbuf = lmc_load_datadir_pixbuf (lmc_theme->filename);
  if (!pixbuf)
    return NULL;

  factory = g_object_new (LMC_TYPE_DECORATION_FACTORY, NULL);

  factory->pixbuf = pixbuf;

  factory->font_map = pango_ft2_font_map_new ();
  pango_ft2_font_map_set_resolution (PANGO_FT2_FONT_MAP (factory->font_map),
				     96., 96.);
  
  desc = pango_font_description_from_string ("Sans Bold 11");
  factory->context = pango_ft2_font_map_create_context (PANGO_FT2_FONT_MAP (factory->font_map));
  pango_context_set_font_description (factory->context, desc);
  pango_font_description_free (desc);

  return factory;
}

LmcDecoration *
lmc_decoration_factory_create_decoration (LmcDecorationFactory *factory)
{
  LmcDecoration *decoration;

  decoration = g_object_new (LMC_TYPE_DECORATION, NULL);
  decoration->factory = g_object_ref (factory);

  return decoration;
}


/**********************************************************************/

G_DEFINE_TYPE (LmcDecoration, lmc_decoration, G_TYPE_OBJECT)

static int
decoration_get_title_width (LmcDecoration *decoration)
{
  int title_space;
  int width;
  
  title_space = (decoration->window_width
		 + lmc_theme->border_info.left
		 + lmc_theme->border_info.right
		 - lmc_theme->border_info.left_unscaled
		 - lmc_theme->border_info.right_unscaled);
  title_space = MAX (title_space, 0);

  pango_layout_get_pixel_size (decoration->layout, &width, NULL);

  return MIN (width + TITLE_RIGHT_PAD, title_space);
}

static void
get_layout_bitmap (PangoLayout    *layout,
		   FT_Bitmap      *bitmap,
		   PangoRectangle *ink_pixels)
{
  PangoRectangle ink_rect;
  
  pango_layout_get_extents (layout, &ink_rect, NULL);
  
  ink_pixels->x = ink_rect.x >> 10;
  ink_pixels->width = ((ink_rect.x + ink_rect.width + 1023) >> 10) - ink_pixels->x;
  
  ink_pixels->y = ink_rect.y >> 10;
  ink_pixels->height = ((ink_rect.y + ink_rect.height + 1023) >> 10) - ink_pixels->y;

  if (ink_pixels->width == 0)
    ink_pixels->width = 1;
  if (ink_pixels->height == 0)
    ink_pixels->height = 1;

  bitmap->width = ink_pixels->width;
  bitmap->pitch = (bitmap->width + 3) & ~3;
  bitmap->rows = ink_pixels->height;
  bitmap->buffer = g_malloc (bitmap->pitch * bitmap->rows);
  bitmap->num_grays = 256;
  bitmap->pixel_mode = FT_PIXEL_MODE_GRAY;
  memset (bitmap->buffer, 0x00, bitmap->pitch * bitmap->rows);

  pango_ft2_render_layout (bitmap, layout, - ink_pixels->x, - ink_pixels->y);
}

static void
draw_layout_on_bits (PangoLayout    *layout,
		     LmcBits        *bits,
		     const LmcPixel *color,
		     int             x,
		     int             y,
		     int             clip_x,
		     int             clip_y,
		     int             clip_width,
		     int             clip_height)
{
  PangoRectangle ink_pixels;
  FT_Bitmap bitmap;
  int i, j;
  guchar *layout_bits;

  get_layout_bitmap (layout, &bitmap, &ink_pixels);

  layout_bits = bitmap.buffer;
  
  for (j = y + ink_pixels.y; j < y + ink_pixels.y + ink_pixels.height; j++)
    {
      if (j >= clip_y && j < clip_y + clip_height)
	{
	  int start_x, end_x;

	  start_x = MAX (x + ink_pixels.x, clip_x);
	  end_x = MIN (x + ink_pixels.x + ink_pixels.width, clip_x + clip_width);

	  if (start_x < end_x)
	    {
	      guchar *b = layout_bits + (start_x - (x + ink_pixels.x));

	      for (i = start_x ; i < end_x; i++)
		{
		  LmcPixel pixel;
		  guint32 a = (*b * color->alpha + 0x80) >> 8;
		  guint32 tr1, tg1, tb1;
		  guint32 tr2, tg2, tb2;

		  lmc_bits_get_pixel (bits, i, j, &pixel);

		  tr1 = (255 - a) * pixel.red + 0x80;
		  tr2 = a * color->red + 0x80;
		  pixel.red = ((tr1 + (tr1 >> 8)) >> 8) + ((tr2 + (tr2 >> 8)) >> 8);
		  
		  tg1 = (255 - a) * pixel.green + 0x80;
		  tg2 = a * color->green + 0x80;
		  pixel.green = ((tg1 + (tg1 >> 8)) >> 8) + ((tg2 + (tg2 >> 8)) >> 8);
		  
		  tb1 = (255 - a) * pixel.blue + 0x80;
		  tb2 = a * color->blue + 0x80;
		  pixel.blue = ((tb1 + (tb1 >> 8)) >> 8) + ((tb2 + (tb2 >> 8)) >> 8);

		  lmc_bits_set_pixel (bits, i, j, &pixel);
		  b++;
		}
	    }
	}
	  	      
      layout_bits += bitmap.pitch;
    }

  g_free (bitmap.buffer);
}

void
lmc_decoration_set_title (LmcDecoration *decoration, const char *title)
{
  if (decoration->layout != NULL)
    {
      g_object_unref (G_OBJECT (decoration->layout));
    }		    

  decoration->layout = NULL;

  if (title != NULL)
    {
      decoration->layout = pango_layout_new (decoration->factory->context);
      pango_layout_set_text (decoration->layout, title, -1);
    }
}

void
lmc_decoration_set_window_size (LmcDecoration *decoration,
				int            width,
				int            height)
{
  decoration->window_width = width;
  decoration->window_height = height;
}

static void
scale_pixbuf_to_bits (GdkPixbuf *pixbuf,
		      LmcBits   *bits,
		      int        x,
		      int        y,
		      int        width,
		      int        height,
		      int        offset_x,
		      int        offset_y,
		      double     scale_x,
		      double     scale_y)
{
  int i, j;
  LmcPixel pixel;
  guchar *pixbuf_pixels, *p;
  int rowstride;
  int src_i, src_j;
  int x0, y0, x1, y1;

  x1 = x + width;
  y1 = y + height;

  x0 = CLAMP (x, 0, bits->width);
  y0 = CLAMP (y, 0, bits->height);
  x1 = CLAMP (x1, 0, bits->width);
  y1 = CLAMP (y1, 0, bits->height);

  pixbuf_pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  for (j = y0; j < y1; j++)
    {
      src_j = (j - offset_y) / scale_y;
      p = pixbuf_pixels + src_j * rowstride;
      for (i = x0; i < x1; i++)
	{
	  src_i = (i - offset_x) / scale_x;
	  pixel.red = p[src_i * 4 + 0];
	  pixel.green = p[src_i * 4 + 1];
	  pixel.blue = p[src_i * 4 + 2];
	  pixel.alpha = p[src_i * 4 + 3];
	  lmc_bits_set_pixel (bits, i, j, &pixel);
	}
    }
}

void
lmc_decoration_draw (LmcDecoration *decoration,
		     LmcBits *bits)
{
  GdkPixbuf *master_pixbuf = decoration->factory->pixbuf;
  
  int master_width = gdk_pixbuf_get_width (master_pixbuf);
  int master_height = gdk_pixbuf_get_height (master_pixbuf);
  int master_x[6];
  int master_y[6];
  
  int repaint_width = bits->width;
  int repaint_height = bits->height;
  int repaint_x[6];
  int repaint_y[6];
  
  int layout_width, layout_height;
  
  int title_width;
  
  int i, j;

  LmcPixel transparent_black = { 0, 0, 0, 0x80 };

  /* See the image in output.c:texture_draw_border() */

  master_x[0] = 0;
  master_x[1] = lmc_theme->border_info.left;
  master_x[2] = lmc_theme->border_info.left_unscaled;
  master_x[3] = master_width - lmc_theme->border_info.right_unscaled;
  master_x[4] = master_width - lmc_theme->border_info.right;
  master_x[5] = master_width;

  master_y[0] = 0;
  master_y[1] = lmc_theme->border_info.top;
  master_y[2] = lmc_theme->border_info.top_unscaled;
  master_y[3] = master_height - lmc_theme->border_info.bottom_unscaled;
  master_y[4] = master_height - lmc_theme->border_info.bottom;
  master_y[5] = master_height;

  repaint_x[0] = 0;
  repaint_x[1] = lmc_theme->border_info.left;
  repaint_x[2] = lmc_theme->border_info.left_unscaled;
  repaint_x[3] = repaint_width - lmc_theme->border_info.right_unscaled;
  repaint_x[4] = repaint_width - lmc_theme->border_info.right;
  repaint_x[5] = repaint_width;

  repaint_y[0] = 0;
  repaint_y[1] = lmc_theme->border_info.top;
  repaint_y[2] = lmc_theme->border_info.top_unscaled;
  repaint_y[3] = repaint_height - lmc_theme->border_info.bottom_unscaled;
  repaint_y[4] = repaint_height - lmc_theme->border_info.bottom;
  repaint_y[5] = repaint_height;

  /* Now copy the master pixbuf onto the pixbuf where we are
   * going to draw the title
   */
  for (i = 0; i < 5; i++)
    for (j = 0; j < 5; j++)
      {
	/* Skip decoration interior. */
	if ((i > 0 && i < 4) && (j > 0 && j < 4))
	  continue;

	int master_w = master_x[j + 1] - master_x[j];
	int master_h = master_y[i + 1] - master_y[i];
	int repaint_w = repaint_x[j + 1] - repaint_x[j];
	int repaint_h = repaint_y[i + 1] - repaint_y[i];
	double scale_x, scale_y;
	double offset_x, offset_y;
	
	if (master_w == 0 || master_h == 0)
	    continue;
	
	scale_x =  (double)repaint_w / master_w;
	scale_y = (double)repaint_h / master_h;
	offset_x = repaint_x[j] - master_x[j] * scale_x;
	offset_y = repaint_y[i] - master_y[i] * scale_y;
	
	scale_pixbuf_to_bits (master_pixbuf,
			      bits,
			      repaint_x[j], repaint_y[i],
			      repaint_w, repaint_h,
			      offset_x, offset_y,
			      scale_x, scale_y);
      }

  /* Now draw the title */
  if (decoration->layout != NULL)
    {
      title_width = decoration_get_title_width (decoration);
      pango_layout_get_pixel_size (decoration->layout,
				   &layout_width, &layout_height);
      draw_layout_on_bits (decoration->layout, bits, &transparent_black,
			   lmc_theme->border_info.left_unscaled + TITLE_SHADOW_OFFSET,
			   TITLE_TOP_PAD + ((lmc_theme->border_info.top - TITLE_TOP_PAD) - layout_height) / 2 + TITLE_SHADOW_OFFSET,
			   lmc_theme->border_info.left_unscaled, 0,
			   title_width - TITLE_RIGHT_PAD, lmc_theme->border_info.top);

      draw_layout_on_bits (decoration->layout, bits, &lmc_theme->title_color,
			   lmc_theme->border_info.left_unscaled,
			   TITLE_TOP_PAD + ((lmc_theme->border_info.top - TITLE_TOP_PAD) - layout_height) / 2,
			   lmc_theme->border_info.left_unscaled, 0,
			   title_width - TITLE_RIGHT_PAD, lmc_theme->border_info.top);
    }
}

static void
lmc_decoration_finalize (GObject *object)
{
  LmcDecoration *decoration = LMC_DECORATION (object);

  lmc_decoration_set_title (decoration, NULL);

  g_object_unref (decoration->factory);
  
  G_OBJECT_CLASS (lmc_decoration_parent_class)->finalize (object);
}

static void
lmc_decoration_class_init (LmcDecorationClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_decoration_finalize;
}

static void
lmc_decoration_init (LmcDecoration *decoration)
{
  decoration->border = lmc_theme->border_info;
}

const LmcBorderInfo *
lmc_decoration_get_border (LmcDecoration *decoration)
{
  return &decoration->border;
}

LmcAction
lmc_decoration_get_action (LmcDecoration *decoration,
			   int            x,
			   int            y)
{
  int i;
  int width, height;
  
  x += decoration->border.left;
  y += decoration->border.top;
  width = decoration->window_width + decoration->border.left + decoration->border.right;
  height = decoration->window_height + decoration->border.top + decoration->border.bottom;

  for (i = 0; i < lmc_theme->num_action_areas; i++)
    {
      int deco_x0 = lmc_theme->action_areas[i].x0;
      int deco_x1 = lmc_theme->action_areas[i].x1;
      int deco_y0 = lmc_theme->action_areas[i].y0;
      int deco_y1 = lmc_theme->action_areas[i].y1;

      if (deco_x0 < 0)
	deco_x0 = width + deco_x0;
      if (deco_x1 <= 0)
	deco_x1 = width + deco_x1;
      if (deco_y0 < 0)
	deco_y0 = height + deco_y0;
      if (deco_y1 <= 0)
	deco_y1 = height + deco_y1;

      if (x >= deco_x0 && x < deco_x1 &&
	  y >= deco_y0 && y < deco_y1)
	return lmc_theme->action_areas[i].action;
    }

  return LMC_ACTION_NOOP;
}

gboolean
lmc_decoration_contains (LmcDecoration *decoration,
			 int            x,
			 int            y,
			 int            width,
			 int            height)
{
  return
    x < decoration->border.left || 
    y < decoration->border.top ||
    x + width > decoration->window_width + decoration->border.left ||
    y + height > decoration->window_height + decoration->border.top;
}
