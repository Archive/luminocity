#ifndef __LMC_WINDOW_H__
#define __LMC_WINDOW_H__

#include "display.h"
#include "types.h"

G_BEGIN_DECLS

#define LMC_TYPE_WINDOW            (lmc_window_get_type ())
#define LMC_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_WINDOW, LmcWindow))
#define LMC_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_WINDOW, LmcWindowClass))
#define LMC_IS_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_WINDOW))
#define LMC_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_WINDOW))
#define LMC_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_WINDOW, LmcWindowClass))

#define LMC_WINDOW_XWINDOW(window)  (LMC_WINDOW (window)->xwindow)
#define LMC_WINDOW_XDISPLAY(window) (LMC_WINDOW (window)->display->xdisplay)

typedef struct _LmcWindowClass  LmcWindowClass;

struct _LmcWindow
{
  GObject parent_instance;

  LmcDisplay *display;
  Window xwindow;

  GList *event_masks;
  long event_mask;

  GList *properties;
};

struct _LmcWindowClass
{
  GObjectClass parent_class;
};

GType	    lmc_window_get_type  (void) G_GNUC_CONST;

void lmc_window_do_event (LmcWindow   *lwindow,
			  XEvent      *xev,
			  const char  *detail);

void lmc_window_select_input   (LmcWindow *lwindow,
				long       event_mask);
void lmc_window_unselect_input (LmcWindow *lwindow,
				long       event_mask);

void              lmc_window_monitor_property   (LmcWindow *lwindow,
						 Atom       property);
void              lmc_window_update_property    (LmcWindow *lwindow,
						 Atom       property);
LmcPropertyValue *lmc_window_get_property_value (LmcWindow *lwindow,
						 Atom       property);

G_END_DECLS

#endif /* __LMC_WINDOW_H__ */
