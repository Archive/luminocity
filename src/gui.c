#include <X11/extensions/XTest.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>
#include <X11/Xcursor/Xcursor.h>

#include <math.h>

#include "decoration.h"
#include "gui.h"
#include "toplevel.h"
#include "utils.h"
#include "spring-model.h"

typedef struct _LmcGuiClass  LmcGuiClass;
typedef struct Animation Animation;

double pager_colors[GUI_MAX_DESKTOPS][3] = {
  { 0.1, 0.1, 0.6 },
  { 0.1, 0.2, 0.5 },
  { 0.1, 0.4, 0.4 },
  { 0.1, 0.6, 0.2 },
};

struct _LmcGui
{
  GObject parent_instance;

  LmcRoot *root;
  LmcOutput *output;

  Animation *animation;
  int workspace;
  int n_desktops;
  LmcOutputViewport *pagers[GUI_MAX_DESKTOPS];

  LmcAction action;
  int action_button;
  LmcToplevel *action_toplevel;
  int action_x_offset;
  int action_y_offset;

  LmcToplevel *focus_toplevel;

  LmcBits *action_cursor_bits;
  LmcAction action_cursor_action;
  int action_cursor_hot_x;
  int action_cursor_hot_y;
  
  gboolean have_pointer;
  double pointer_x;	/* in source coordinates */
  double pointer_y;	/* in source coordinates */

  LmcDecorationFactory *decoration_factory;

  XRectangle visible_source;
  
  KeyCode *keycode_map;
  GSList *down_keys;

  int saved_x;
  int saved_y;
  int saved_width;
  int saved_height;
};

struct _LmcGuiClass
{
  GObjectClass parent_class;
};

/**********************************************************************/

G_DEFINE_TYPE (LmcGui, lmc_gui, G_TYPE_OBJECT)

static void handle_motion (LmcGui *gui,
			   int output_x,
			   int output_y);

static void
lmc_gui_finalize (GObject *object)
{
  LmcGui *gui = LMC_GUI (object);

  g_slist_free (gui->down_keys);
  g_free (gui->keycode_map);

  G_OBJECT_CLASS (lmc_gui_parent_class)->finalize (object);
}

static void
lmc_gui_class_init (LmcGuiClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_gui_finalize;
}

static void
lmc_gui_init (LmcGui *gui)
{
  gui->keycode_map = g_new (KeyCode, 256);
  gui->have_pointer = FALSE;
  gui->workspace = 0;
}

static LmcOutputWindow *
gui_get_output_window (LmcToplevel *toplevel)
{
  return g_object_get_data (G_OBJECT (toplevel), "gui-output-window");
}

static LmcDecoration *
gui_get_decoration (LmcToplevel *toplevel)
{
  return g_object_get_data (G_OBJECT (toplevel), "gui-decoration");
}

static void
gui_update_bits (LmcGui      *gui,
		 LmcToplevel *toplevel)
{
  LmcOutputWindow *owindow = gui_get_output_window (toplevel);

  if (owindow)
    {
      LmcBits *bits = lmc_toplevel_get_bits (toplevel);
      if (bits)
	lmc_output_window_set_bits (owindow, bits);
    }
}

static void
gui_update_stacking (LmcGui      *gui,
		     LmcToplevel *toplevel)
{
  LmcOutputWindow *owindow = gui_get_output_window (toplevel);
  if (owindow)
    {
      GList *toplevels = lmc_root_get_toplevels (gui->root);
      GList *l;

      for (l = g_list_find (toplevels, toplevel)->next; l; l = l->next)
	{
	  LmcOutputWindow *owindow_above = gui_get_output_window (l->data);
	  if (owindow_above)
	    {
	      lmc_output_window_restack (owindow, owindow_above);
	      return;
	    }
	}
      
      lmc_output_window_restack (owindow, NULL);
    }
}
  
static LmcOutputWindow *
gui_create_output_window (LmcGui      *gui,
			  LmcToplevel *toplevel)
{
  LmcOutputWindow *owindow;
  LmcBorderInfo border_info;
  int x, y;
  int width, height;

  owindow = lmc_output_create_window (gui->output);
  g_object_set_data (G_OBJECT (toplevel), "gui-output-window", owindow);

  lmc_toplevel_get_position (toplevel, &x, &y);
  lmc_toplevel_get_size (toplevel, &width, &height);
  
  lmc_toplevel_get_border_info (toplevel, &border_info);
      
  lmc_output_window_move (owindow, x, y);
  lmc_output_window_set_alpha (owindow,
			       toplevel == gui->focus_toplevel ? 1.00 : 0.75);
  gui_update_stacking (gui, toplevel);
  gui_update_bits (gui, toplevel);

  return owindow;
}

static void
lmc_animation_callback (gpointer user_data);

LmcAnimation *
lmc_animation_new (LmcGui *gui, LmcToplevel *toplevel,
		   LmcAnimationFunc animation_func, gpointer user_data)
{
  LmcAnimation *animation;

  animation = g_new (LmcAnimation, 1);
  animation->toplevel = toplevel;
  animation->owindow = gui_get_output_window (toplevel);
  animation->output = gui->output;
  animation->gui = gui;
  animation->timer = g_timer_new ();
  animation->ref_count = 1;
  animation->animation_func = animation_func;
  animation->user_data = user_data;
    
  lmc_output_queue_bread_crumb (gui->output,
				lmc_animation_callback, animation);

  return animation;
}

void
lmc_animation_ref (LmcAnimation *animation)
{
  animation->ref_count++;
}

void
lmc_animation_unref (LmcAnimation *animation)
{
  animation->ref_count--;

  if (animation->ref_count == 0)
    {
      g_timer_destroy (animation->timer);
      g_free (animation);
    }
}     

static void
lmc_animation_callback (gpointer user_data)
{
  LmcAnimation *animation = user_data;
  gboolean again;

  again = animation->animation_func (animation, animation->user_data);

  if (again)
    lmc_output_queue_bread_crumb (animation->gui->output,
				  lmc_animation_callback, animation);
  else
    lmc_animation_unref (animation);
}

void
lmc_animation_reset (LmcAnimation *animation)
{
  g_timer_reset (animation->timer);
}

#define INIT_ALPHA 0.3
#define FADE_TIME  0.15

static gboolean
fade_in_callback (LmcAnimation *animation, gpointer user_data)
{
  LmcOutputWindow *owindow;
  double t, t_hat, alpha, target_alpha;

  owindow = gui_get_output_window (animation->toplevel);
  if (owindow == NULL)
    return FALSE;

  t = g_timer_elapsed (animation->timer, NULL) / FADE_TIME;

  if (t >= 1.0)
    t = 1.0;
    
  if (animation->gui->focus_toplevel == animation->toplevel)
    target_alpha = 1.0;
  else
    target_alpha = 0.75;
	    
  t_hat = t * t;
  alpha = t_hat * (target_alpha - INIT_ALPHA) + INIT_ALPHA;

  lmc_output_window_set_alpha (owindow, alpha);

  if (t >= 1.0)
    return FALSE;

  return TRUE;
}

static void
gui_destroy_output_window (LmcGui      *gui,
			   LmcToplevel *toplevel)
{
  LmcOutputWindow *owindow = gui_get_output_window (toplevel);

  if (owindow)
    {
      lmc_output_window_destroy (owindow);
      g_object_set_data (G_OBJECT (toplevel), "gui-output-window", NULL);
    }
}

static void
on_bits_available (LmcToplevel *toplevel,
		   LmcGui      *gui)
{
  LmcOutputWindow *owindow;

  lmc_output_freeze (gui->output);
    
  owindow = gui_create_output_window (gui, toplevel);
  lmc_output_window_set_alpha (owindow, INIT_ALPHA);

  lmc_output_thaw (gui->output);

  lmc_wobble_setup (toplevel, gui);

  lmc_animation_new (gui, toplevel, fade_in_callback, NULL);

}

static void
gui_end_action (LmcGui      *gui,
		LmcToplevel *toplevel)
		
{
  if (gui->action_toplevel == toplevel)
    {
      gui->action = LMC_ACTION_NONE;
      gui->action_toplevel = NULL;
      gui->action_button = 0;
    }
}

static gboolean
on_map_notify (LmcToplevel *toplevel,
	       XMapEvent   *xev,
	       LmcGui      *gui)
{
  if (!lmc_toplevel_is_override_redirect (toplevel))
    {
      LmcDecoration *decoration;
      const LmcBorderInfo *border_info;
      const char *title;

      decoration = lmc_decoration_factory_create_decoration (gui->decoration_factory);
      g_object_set_data (G_OBJECT (toplevel), "gui-decoration", decoration);
      g_object_set_data (G_OBJECT (decoration), "gui-toplevel", toplevel);

      border_info = lmc_decoration_get_border (decoration);
      lmc_toplevel_set_border_info (toplevel, border_info);
      title = lmc_toplevel_get_wm_name (toplevel);
      lmc_decoration_set_title (decoration, title);
    }

  return FALSE;
}

static gboolean
on_unmap_notify (LmcToplevel *toplevel,
		 XUnmapEvent *xev,
		 LmcGui      *gui)
{
  gui_destroy_output_window (gui, toplevel);
  gui_end_action (gui, toplevel);

  return FALSE;
}

static gboolean
on_destroy_notify (LmcToplevel         *toplevel,
		   XDestroyWindowEvent *xev,
		   LmcGui              *gui)
{
  if (gui->focus_toplevel == toplevel)
    gui->focus_toplevel = NULL;
  
  gui_destroy_output_window (gui, toplevel);
  gui_end_action (gui, toplevel);

  return FALSE;
}

static void
on_position_changed (LmcToplevel     *toplevel,
		     LmcGui          *gui)
{
  LmcOutputWindow *owindow = gui_get_output_window (toplevel);
  LmcBorderInfo border_info;
  
  lmc_toplevel_get_border_info (toplevel, &border_info);

  if (owindow)
    {
      int x, y;
  
      lmc_toplevel_get_position (toplevel, &x, &y);
      lmc_output_window_move (owindow,
			      x - border_info.left,
			      y - border_info.top);
    }
}

static void
on_modified (LmcToplevel *toplevel,
	     int          x,
	     int          y,
	     int          width,
	     int          height,
	     LmcGui      *gui)
{
  LmcOutputWindow *owindow = gui_get_output_window (toplevel);
  LmcDecoration *decoration = gui_get_decoration (toplevel);
  LmcBits *bits;
  int toplevel_width, toplevel_height;

  if (decoration && lmc_decoration_contains (decoration, x, y, width, height))
    {
      lmc_toplevel_get_size (toplevel, &toplevel_width, &toplevel_height);
      lmc_decoration_set_window_size (decoration,
				      toplevel_width, toplevel_height);

      bits = lmc_toplevel_get_bits (toplevel);
      if (bits)
	lmc_decoration_draw (decoration, bits);
    }

  if (owindow)
    {
      gui_update_bits (gui, toplevel);
      lmc_output_window_modify (owindow, x, y, width, height);
    }
}

static void
on_wm_name_changed (LmcToplevel *toplevel,
		    LmcGui      *gui)
{
  LmcOutputWindow *owindow = gui_get_output_window (toplevel);
  LmcDecoration *decoration = gui_get_decoration (toplevel);
  LmcBits *bits;
  const char *title;

  if (decoration)
    {
      title = lmc_toplevel_get_wm_name (toplevel);
      lmc_decoration_set_title (decoration, title);
      bits = lmc_toplevel_get_bits (toplevel);
      lmc_decoration_draw (decoration, bits);
    }

  if (owindow)
    {
      gui_update_bits (gui, toplevel);
      lmc_output_window_modify (owindow, -10, -30, 300, 100);
    }
}

static void
gui_show_border (LmcGui *gui,
		 LmcToplevel *toplevel)
{
}

static void
gui_hide_border (LmcGui *gui,
		 LmcToplevel *toplevel)
{
}

static gboolean
on_client_message (LmcToplevel         *toplevel,
		   XClientMessageEvent *xev,
		   LmcGui              *gui)
{
    LmcDisplay *ldisplay = LMC_WINDOW (toplevel)->display;
    Display *dpy = ldisplay->xdisplay;
    
    if (xev->message_type == XInternAtom (dpy, "_NET_WM_STATE", False))
    {
#define REMOVE 0
#define ADD 1
#define TOGGLE 2
	
	Atom full_screen = XInternAtom (dpy, "_NET_WM_STATE_FULLSCREEN", False);

	if (xev->data.l[1] == full_screen || xev->data.l[2] == full_screen)
	  {
	    guint width, height;

	    lmc_output_get_size (gui->output, &width, &height);
	    
	    if (xev->data.l[0] == REMOVE)
	      {
		if (lmc_toplevel_get_fullscreen (toplevel))
		  {
		    lmc_output_freeze (gui->output);
		    lmc_toplevel_set_fullscreen (toplevel, FALSE);
		    lmc_toplevel_move_resize (toplevel, gui->saved_x, gui->saved_y,
					      gui->saved_width, gui->saved_height);
		    gui_show_border (gui, toplevel);

		    lmc_output_set_show_pager (gui->output, gui->n_desktops > 1);
		    
		    lmc_output_thaw (gui->output);
		  }
	      }
	    else if (xev->data.l[0] == ADD)
	      {
		if (!lmc_toplevel_get_fullscreen (toplevel))
		  {
		    lmc_output_freeze (gui->output);
		    lmc_toplevel_get_position (toplevel, &gui->saved_x, &gui->saved_y);
		    lmc_toplevel_get_size (toplevel, &gui->saved_width, &gui->saved_height);
		    
		    lmc_toplevel_move_resize (toplevel, 0, 0, width, height);
		    
		    lmc_toplevel_set_fullscreen (toplevel, TRUE);

		    gui_hide_border (gui, toplevel);

		    lmc_output_set_show_pager (gui->output, FALSE);

		    lmc_output_thaw (gui->output);
		  }
	      }
	    else if (xev->data.l[1] == TOGGLE)
	      {
		/* FIXME, but 'toggle' is crack anyway .. */
		g_warning ("toggle not implemented\n");
	      }
	  }
    }

    return TRUE;
}

static void
gui_add_toplevel (LmcGui      *gui,
		  LmcToplevel *toplevel)
{
  g_signal_connect (toplevel, "bits-available", G_CALLBACK (on_bits_available), gui);
  g_signal_connect (toplevel, "event::MapNotify", G_CALLBACK (on_map_notify), gui);
  g_signal_connect (toplevel, "event::UnmapNotify", G_CALLBACK (on_unmap_notify), gui);
  g_signal_connect (toplevel, "event::DestroyNotify", G_CALLBACK (on_destroy_notify), gui);
  g_signal_connect (toplevel, "event::ClientMessage", G_CALLBACK (on_client_message), gui);
  g_signal_connect (toplevel, "position-changed", G_CALLBACK (on_position_changed), gui);
  g_signal_connect (toplevel, "modified", G_CALLBACK (on_modified), gui);
  g_signal_connect (toplevel, "wm-name-changed", G_CALLBACK (on_wm_name_changed), gui);
  
  if (lmc_toplevel_is_mapped (toplevel))
    gui_create_output_window (gui, toplevel);
}

static void
on_toplevel_added (LmcRoot     *root,
		   LmcToplevel *toplevel,
		   LmcGui      *gui)
{
  gui_add_toplevel (gui, toplevel);
}

static void
on_toplevel_restacked (LmcRoot     *root,
		       LmcToplevel *toplevel,
		       LmcToplevel *above,
		       LmcGui      *gui)
{
  gui_update_stacking (gui, toplevel);
}


static LmcToplevel *
gui_toplevel_at_position (LmcGui    *gui,
			  int        pos_x,
			  int        pos_y,
			  LmcAction *action)
{
  GList *toplevels = lmc_root_get_toplevels (gui->root);
  GList *l;

  if (action)
    *action = LMC_ACTION_NONE;

  for (l = toplevels; l; l = l->next)
    {
      LmcToplevel *toplevel = l->data;
      int x, y, width, height;

      if (lmc_toplevel_is_mapped (toplevel))
	{
	  LmcDecoration *decoration;
	  const LmcBorderInfo *border;
	  
	  lmc_toplevel_get_position (toplevel, &x, &y);
	  lmc_toplevel_get_size (toplevel, &width, &height);

	  if (pos_x >= x && pos_x < x + width &&
	      pos_y >= y && pos_y < y + height)
	    return toplevel;

	  decoration = gui_get_decoration (toplevel);
	  if (decoration)
	    {
	      border = lmc_decoration_get_border (decoration);
	      
	      if (pos_x >= x - border->left &&
		  pos_x < x + width + border->right &&
		  pos_y >= y - border->top &&
		  pos_y < y + height + border->bottom)
		{
		  LmcAction tmp_action;
		  
		  tmp_action = lmc_decoration_get_action (decoration, pos_x - x, pos_y - y);

		  switch (tmp_action)
		    {
		    case LMC_ACTION_RESIZE_LEFT:
		    case LMC_ACTION_RESIZE_RIGHT:
		    case LMC_ACTION_RESIZE_TOP_LEFT:
		    case LMC_ACTION_RESIZE_TOP:
		    case LMC_ACTION_RESIZE_TOP_RIGHT:
		    case LMC_ACTION_RESIZE_BOTTOM:
		    case LMC_ACTION_RESIZE_BOTTOM_LEFT:
		    case LMC_ACTION_RESIZE_BOTTOM_RIGHT:
		      if (!lmc_toplevel_is_resizable (toplevel))
			tmp_action = LMC_ACTION_NOOP;
		      break;
		    default:
		      break;
		    }
		  
		  if (action)
		    *action = tmp_action;
		  
		  return toplevel;
		}
	    }
	}
    }

  return NULL;
}

static void
gui_set_cursor_action (LmcGui    *gui,
		       LmcAction  action)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  int shape = - 1;		/* Quiet gcc */

  if (action == gui->action_cursor_action)
    return;

  if (gui->action_cursor_bits)
    {
      lmc_bits_unref (gui->action_cursor_bits);
      gui->action_cursor_bits = NULL;
    }
  
  switch (action)
    {
    case LMC_ACTION_NONE:
      shape = -1;
      break;
    case LMC_ACTION_NOOP:
    case LMC_ACTION_MOVE:
    case LMC_ACTION_MENU:
    case LMC_ACTION_MINIMIZE:
    case LMC_ACTION_MAXIMIZE:
    case LMC_ACTION_CLOSE:
      /* We need to override the cursor when over the title bar
       * since the target X server will change the cursor based
       * on the window that the pointer is over. Really, this
       * is a hack, and we need to represent the title bar to
       * the target X server.
       */
      shape = XC_left_ptr;
      break;
    case LMC_ACTION_RESIZE_LEFT:
      shape = XC_left_side;
      break;
    case LMC_ACTION_RESIZE_RIGHT:
      shape = XC_right_side;
      break;
    case LMC_ACTION_RESIZE_TOP_LEFT:
      shape = XC_top_left_corner;
      break;
    case LMC_ACTION_RESIZE_TOP:
      shape = XC_top_side;
      break;
    case LMC_ACTION_RESIZE_TOP_RIGHT:
      shape = XC_top_right_corner;
      break;
    case LMC_ACTION_RESIZE_BOTTOM:
      shape = XC_bottom_side;
      break;
    case LMC_ACTION_RESIZE_BOTTOM_LEFT:
      shape = XC_bottom_left_corner;
      break;
    case LMC_ACTION_RESIZE_BOTTOM_RIGHT:
      shape = XC_bottom_right_corner;
      break;
    }

  if (shape != -1)
    {
      XcursorImage *image;
      
      image = XcursorShapeLoadImage (shape,
				     XcursorGetTheme (xdisplay),
				     XcursorGetDefaultSize (xdisplay));
      if (image)
	{
	  gui->action_cursor_bits = lmc_bits_new (LMC_BITS_ARGB_32,
						  image->width, image->height,
						  (guchar *)image->pixels,
						  image->width * 4,
						  (GDestroyNotify)XcursorImageDestroy,
						  image);
	  gui->action_cursor_hot_x = image->xhot;
	  gui->action_cursor_hot_y = image->yhot;
	}
    }
  
  gui->action_cursor_action = action;
}

static void
gui_translate_cursor_s2o (LmcGui *gui,
			  double	 *x,
			  double	 *y);

static void
gui_update_pointer (LmcGui *gui)
{
  LmcBits *bits;
  double pointer_x, pointer_y;
  
  if (gui->have_pointer)
    {
      int hot_x, hot_y;
      LmcAction cursor_action = LMC_ACTION_NONE;

      if (gui->action != LMC_ACTION_NONE)
	cursor_action = gui->action;
      else
	{
	  LmcAction tmp_action;
	  
	  if (gui_toplevel_at_position (gui, gui->pointer_x, gui->pointer_y, &tmp_action))
	    cursor_action = tmp_action;
	}

      gui_set_cursor_action (gui, cursor_action);

      if (gui->action_cursor_bits)
	{
	  bits = gui->action_cursor_bits;
	  hot_x = gui->action_cursor_hot_x;
	  hot_y = gui->action_cursor_hot_y;
	}
      else
	lmc_root_get_cursor (gui->root, &bits, &hot_x, &hot_y);

      pointer_x = gui->pointer_x - hot_x;
      pointer_y = gui->pointer_y - hot_y;
    }
  else
    {
      pointer_x = 0;
      pointer_y = 0;
      bits = NULL;
    }

  gui_translate_cursor_s2o (gui, &pointer_x, &pointer_y);

  lmc_output_set_cursor (gui->output, bits, pointer_x, pointer_y);
}

static void
on_cursor_changed (LmcRoot     *root,
		   LmcGui      *gui)
{
  gui_update_pointer (gui);
}

static void
gui_set_focus_toplevel (LmcGui      *gui,
			LmcToplevel *toplevel)
{
  if (gui->focus_toplevel != toplevel)
    {
      LmcOutputWindow *owindow;
      
      if (gui->focus_toplevel)
	{
	  owindow = gui_get_output_window (gui->focus_toplevel);
	  if (owindow)
	    lmc_output_window_set_alpha (owindow, 0.75);
	}

      gui->focus_toplevel = toplevel;

      if (gui->focus_toplevel)
	{
	  owindow = gui_get_output_window (gui->focus_toplevel);
	  if (owindow)
	    lmc_output_window_set_alpha (owindow, 1.0);
	}
    }
}

static void
on_focus_changed (LmcRoot     *root,
		  LmcGui      *gui)
{
  gui_set_focus_toplevel (gui, lmc_root_get_focus_toplevel (root));
}

static void
gui_begin_move (LmcGui       *gui,
		LmcToplevel  *toplevel,
		int	      button,
		int	      source_x,
		int	      source_y)
{
  gui->action = LMC_ACTION_MOVE;
  gui->action_button = button;
  gui->action_toplevel = toplevel;

  lmc_toplevel_begin_move (toplevel, source_x, source_y);
  
  lmc_toplevel_grab_focus (toplevel);
}

static void
gui_update_move (LmcGui       *gui,
		 int	       source_x,
		 int	       source_y)
{
  lmc_toplevel_update_move (gui->action_toplevel, source_x, source_y);
}

static void
gui_end_move (LmcGui		*gui,
	      LmcToplevel	*toplevel,
	      int		 button)
{
  lmc_toplevel_end_move (toplevel);
}

static void
categorize_resize_action (LmcAction  action,
			  gboolean  *from_left,
			  gboolean  *from_right,
			  gboolean  *from_top,
			  gboolean  *from_bottom)
{
  *from_left = *from_right = *from_top = *from_bottom = FALSE;
  
  switch (action) {
  case LMC_ACTION_RESIZE_LEFT:
  case LMC_ACTION_RESIZE_TOP_LEFT:
  case LMC_ACTION_RESIZE_BOTTOM_LEFT:
    *from_left = TRUE;
    break;
  case LMC_ACTION_RESIZE_BOTTOM:
  case LMC_ACTION_RESIZE_TOP:
    break;
  case LMC_ACTION_RESIZE_RIGHT:
  case LMC_ACTION_RESIZE_TOP_RIGHT:
  case LMC_ACTION_RESIZE_BOTTOM_RIGHT:
    *from_right = TRUE;
    break;
  default:
    g_assert_not_reached ();
  }

  switch (action) {
  case LMC_ACTION_RESIZE_TOP:
  case LMC_ACTION_RESIZE_TOP_LEFT:
  case LMC_ACTION_RESIZE_TOP_RIGHT:
    *from_top = TRUE;
    break;
  case LMC_ACTION_RESIZE_LEFT:
  case LMC_ACTION_RESIZE_RIGHT:
    break;
  case LMC_ACTION_RESIZE_BOTTOM:
  case LMC_ACTION_RESIZE_BOTTOM_LEFT:
  case LMC_ACTION_RESIZE_BOTTOM_RIGHT:
    *from_bottom = TRUE;
    break;
  default:
    g_assert_not_reached ();
  }
}


static void
gui_begin_resize (LmcGui       *gui,
		  LmcToplevel  *toplevel,
		  LmcAction     action,
		  int		button,
		  int		source_x,
		  int		source_y)
{
  int x, y, width, height;
  gboolean from_left, from_right, from_top, from_bottom;

  lmc_toplevel_get_set_position (toplevel, &x, &y);
  lmc_toplevel_get_set_size (toplevel, &width, &height);

  categorize_resize_action (action,
			    &from_left, &from_right, &from_top, &from_bottom);
	    
  gui->action = action;
  gui->action_button = button;
  gui->action_toplevel = toplevel;

  if (from_left)
    gui->action_x_offset = source_x - x;
  else if (from_right)
    gui->action_x_offset = source_x - width;

  if (from_top)
    gui->action_y_offset = source_y - y;
  else if (from_bottom)
    gui->action_y_offset = source_y - height;
  
  lmc_toplevel_grab_focus (toplevel);
}

static void
gui_update_resize (LmcGui       *gui,
		   int		 source_x,
		   int		 source_y)
{
  int x, y, width, height;
  int new_x, new_y, new_width, new_height;
  int constrained_width, constrained_height;
  gboolean from_left, from_right, from_top, from_bottom;
  
  lmc_toplevel_get_set_position (gui->action_toplevel, &x, &y);
  lmc_toplevel_get_set_size (gui->action_toplevel, &width, &height);

  categorize_resize_action (gui->action,
			    &from_left, &from_right, &from_top, &from_bottom);

  new_x = x;
  new_y = y;
  new_width = width;
  new_height = height;

  if (from_left)
    {
      new_x = source_x - gui->action_x_offset;
      new_width = width + x - new_x;
    }
  else if (from_right)
    new_width = source_x - gui->action_x_offset;

  if (from_top)
    {
      new_y = source_y - gui->action_y_offset;
      new_height = height + y - new_y;
    }
  else if (from_bottom)
    new_height = source_y - gui->action_y_offset;

  /* We can't just let set_size() constrain since that
   * will give the wrong behavior for resizing from top/left:
   * we'll push the window along
   */
  lmc_toplevel_constrain_size (gui->action_toplevel,
			       new_width, new_height,
			       &constrained_width, &constrained_height);

  if (from_left)
    new_x -= constrained_width - new_width;
  if (from_top)
    new_y -= constrained_height - new_height;

  lmc_toplevel_move_resize (gui->action_toplevel,
			    new_x, new_y,
			    constrained_width, constrained_height);
}

static void
gui_raise_lower (LmcGui       *gui,
		 LmcToplevel  *toplevel,
		 int	       button,
		 int	       source_x,
		 int	       source_y)
{
  GList *toplevels = lmc_root_get_toplevels (gui->root);
  GList *l;
  
  for (l = g_list_find (toplevels, toplevel)->prev; l; l = l->prev)
    {
      if (gui_get_output_window (l->data)) /* Not already at the top */
	{
	  lmc_toplevel_raise (toplevel);
	  return;
	}
    }
  
  lmc_toplevel_lower (toplevel);
}

static void
gui_move_pointer (LmcGui  *gui,
		  double   x,
		  double   y);

#define SWITCH_TIME 0.4

struct Animation
{
  LmcGui *gui;
  XRectangle start_rect;
  XRectangle end_rect;
  int target_workspace;
  GTimer *timer;
};

static void
animation_destroy (Animation *animation)
{
  g_timer_destroy (animation->timer);
  g_free (animation);
}

static void
lmc_gui_set_visible_source (LmcGui *gui,
			    XRectangle *visible)
{
  double cursor_x, cursor_y;

  cursor_x = gui->pointer_x;
  cursor_y = gui->pointer_y;
  
  /* Get the cursor position in output coordinates */
  gui_translate_cursor_s2o (gui, &cursor_x, &cursor_y);

  lmc_output_set_visible_rect (gui->output, visible);
  gui->visible_source = *visible;

  /* "Move" the cursor to the same position, updating the
   * source position
   */
  handle_motion (gui, cursor_x, cursor_y);
}

static gint
position (gint from, gint to, gdouble elapsed)
{
  int n_pixels = (to - from) * elapsed;

  if (n_pixels < 0)
    n_pixels *= -1;
  
#if 0
  gint n_pixels;

  if (elapsed <= ACCEL_THRESHOLD)
    {
      n_pixels = SLIDE_SPEED * elapsed;
    }
  else
    {
      /* The formula is a second degree polynomial in
       * @elapsed that has the line SLIDE_SPEED * @elapsed
       * as tangent for @elapsed == ACCEL_THRESHOLD.
       * This makes @n_pixels a smooth function of elapsed time.
       */
      n_pixels = (SLIDE_SPEED / ACCEL_THRESHOLD) * elapsed * elapsed -
	SLIDE_SPEED * elapsed + SLIDE_SPEED * ACCEL_THRESHOLD;
    }
#endif
  
  if (to > from)
    return MIN (from + n_pixels, to);
  else
    return MAX (from - n_pixels, to);
}

static void
compute_intermediate (double elapsed,
		      const XRectangle *start,
		      const XRectangle *goal,
		      XRectangle       *intermediate)
{
  intermediate->x = position (start->x, goal->x, elapsed);
  intermediate->y = position (start->y, goal->y, elapsed);
  intermediate->width =
    position (start->x + start->width, goal->x + goal->width, elapsed) - intermediate->x;
  intermediate->height =
    position (start->y + start->height, goal->y + goal->height, elapsed) - intermediate->y;
}

static gboolean
animation_do_frame (Animation *animation, gdouble t)
{
  LmcGui *gui = animation->gui;
  int new_y;
  int width, height;
  XRectangle current;
  double t_zoom;	/* runs from 0.0 to 1.0 to 1.0 */
  XRectangle *start, *end;

  lmc_output_get_size (gui->output, &width, &height);
  
  new_y = animation->start_rect.y + t * (animation->end_rect.y - animation->start_rect.y);

  lmc_output_get_visible_rect (gui->output, &current);

#define ZOOM_SIZE 200

  t_zoom = 1 - pow ((1 - 2 * t), 2); //  * (1 - 2 * t);

  
#if 0
#define OUT_TIME 0.9
  if (t <= OUT_TIME)
    {
      double t_hat = t / OUT_TIME;
  
      t_zoom = 1 - (1 - 2 * t_hat) * (1 - 2 * t_hat);
    }
  else
    {
      double t_hat = (t - OUT_TIME) / (1 - OUT_TIME);
      
      t_zoom = - OUT_TIME * (1 - (1 - 2 * t_hat) * (1 - 2 * t_hat));
    }
#endif

#if 0
  double t_hat = 3 * G_PI * t;

  t_zoom = 3 * sin (t_hat * pow (t, 1.5)) * (1 - t); // 2 * M_PI - t_hat)) / 2 * M_PI;
#endif
  
  start = &animation->start_rect;
  end = &animation->end_rect;

  compute_intermediate (t, start, end, &current);

  current.x -= t_zoom * ZOOM_SIZE;
  current.y -= t_zoom * ZOOM_SIZE;
  current.width += t_zoom * 2 * ZOOM_SIZE;
  current.height += t_zoom * 2 * ZOOM_SIZE;
#if 0
  current.x = start_x + (end->x - start->x)  - animation->start_rect.x - 
  current.y = new_y;
  current.width = animation->start_rect.width + t_zoom * 2 * ZOOM_SIZE;
  current.height = animation->start_rect.height + t_zoom * 2 * ZOOM_SIZE;
#endif

  if (current.width <= 0)
    current.width = 1;
  if (current.height <= 0)
    current.height = 1;
  
  lmc_gui_set_visible_source (gui, &current);

  if (t >= 1.0)
    return TRUE;
  else
    return FALSE;
}

static void
animation_next_frame (gpointer data)
{
  Animation *animation = data;
  gdouble elapsed;
  double t;
  LmcGui *gui = animation->gui;
  gboolean done;
  
  elapsed = g_timer_elapsed (animation->timer, NULL);

  if (elapsed >= SWITCH_TIME)
      elapsed = SWITCH_TIME;

  t = elapsed / SWITCH_TIME;

  if (t >= 1.0)
    t = 1.0;
  
  lmc_output_freeze (gui->output);

  done = animation_do_frame (animation, t);
  
  lmc_output_thaw (gui->output);
  
  if (done)
    {
      gui->workspace = animation->target_workspace;
      
      animation_destroy (animation);
      gui->animation = NULL;
      g_assert (gui->animation == NULL);
    }
  else
    {
      lmc_output_queue_bread_crumb (gui->output, animation_next_frame, animation);
    }
}

void
animation_update_target (Animation *animation, int target_workspace)
{
  int width, height;
  
  animation->target_workspace = target_workspace;
 
  lmc_output_get_visible_rect (animation->gui->output, &animation->start_rect);

  lmc_output_get_size (animation->gui->output, &width, &height);
  
  animation->end_rect.y = animation->target_workspace * height;
  
  g_timer_reset (animation->timer);
}

static Animation *
animation_new (LmcGui *gui, int target_workspace)
{
  int width, height;
  
  Animation *animation = g_new0 (Animation, 1);

  animation->gui = gui;
  animation->timer = g_timer_new ();

  animation_update_target (animation, target_workspace);
  animation->end_rect = animation->start_rect;

  lmc_output_get_size (animation->gui->output, &width, &height);
  
  animation->end_rect.y = animation->target_workspace * height;
  
  
    
  return animation;
}

static void
gui_switch_to_workspace (LmcGui *gui,
			 int     workspace)
{
  if (gui->animation)
    {
      animation_update_target (gui->animation, workspace);
    }
  else
    {
      gui->animation = animation_new (gui, workspace);
      animation_next_frame (gui->animation);
    }
}

/* o2s: output to source
 * s2o: source to output
 */

static void
gui_translate_cursor_o2s (LmcGui *gui,
			  double *x,
			  double *y)
{
  int output_width, output_height;
  double xratio, yratio;

  lmc_output_get_size (gui->output, &output_width, &output_height);

  xratio = *x / (double)output_width;

  *x = gui->visible_source.x + xratio * gui->visible_source.width;

  yratio = *y / (double)output_height;

  *y = gui->visible_source.y + yratio * gui->visible_source.height;
}

static void
gui_translate_cursor_s2o (LmcGui *gui,
			  double *x,
			  double *y)
{
  int output_width, output_height;
  double xratio, yratio;

  lmc_output_get_size (gui->output, &output_width, &output_height);

  xratio = (*x - gui->visible_source.x) / (double)gui->visible_source.width;

  *x = xratio * output_width;

  yratio = (*y - gui->visible_source.y) / (double)gui->visible_source.height;

  *y = yratio * output_height;
}

static void
gui_handle_button_press (LmcGui *gui,
			 int button, int state,
			 int source_x, int source_y,
			 int output_x, int output_y)
{
  LmcToplevel *toplevel;
  LmcAction action;
  int width, height;
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  LmcDisplay *ldisplay = LMC_WINDOW (gui->output)->display;
  guint super_mod = lmc_display_get_super_modifier (ldisplay);
  
  lmc_output_get_size (gui->output, &width, &height);
  
  if (output_x > width - width / 8 && output_y > height - gui->n_desktops * (height / 8))
    {
      int y_rel = output_y - (height - gui->n_desktops * (height / 8));
      
      gui_switch_to_workspace (gui, y_rel / (height / 8));

      return;
    }

  toplevel = gui_toplevel_at_position (gui, source_x, source_y, &action);

  if (toplevel &&
      (button == 1 || button == 2 || button == 3) &&
      (action != LMC_ACTION_NONE || (state & super_mod) != 0))
    {
      switch (action) {
      case LMC_ACTION_NONE:
      case LMC_ACTION_NOOP:
	if (button == 1)
	  gui_begin_move (gui, toplevel, button, source_x, source_y);
	else if (button == 2)
	  {
	    int x, y, width, height;
	    
	    lmc_toplevel_get_position (toplevel, &x, &y);
	    lmc_toplevel_get_size (toplevel, &width, &height);
	    
	    if (source_x < x + width / 2)
	      action = ((source_y < y + height / 2) ?
			LMC_ACTION_RESIZE_TOP_LEFT :
			LMC_ACTION_RESIZE_BOTTOM_LEFT);
	    else
	      action = ((source_y < y + height / 2) ?
			LMC_ACTION_RESIZE_TOP_RIGHT :
			LMC_ACTION_RESIZE_BOTTOM_RIGHT);
	    
	    gui_begin_resize (gui, toplevel, action, button, source_x, source_y);
	  }
	else if (button == 3)
	  gui_raise_lower (gui, toplevel, button, source_x, source_y);
	break;
      case LMC_ACTION_MOVE:
	if (button == 1)
	  gui_begin_move (gui, toplevel, button, source_x, source_y);
	else if (button == 3)
	  gui_raise_lower (gui, toplevel, button, source_x, source_y);
	break;
      case LMC_ACTION_RESIZE_LEFT:
      case LMC_ACTION_RESIZE_TOP_LEFT:
      case LMC_ACTION_RESIZE_BOTTOM_LEFT:
      case LMC_ACTION_RESIZE_BOTTOM:
      case LMC_ACTION_RESIZE_TOP:
      case LMC_ACTION_RESIZE_RIGHT:
      case LMC_ACTION_RESIZE_TOP_RIGHT:
      case LMC_ACTION_RESIZE_BOTTOM_RIGHT:
	if (button == 1)
	  gui_begin_resize (gui, toplevel, action, button, source_x, source_y);
	break;
      case LMC_ACTION_CLOSE:
	lmc_toplevel_delete (toplevel);
	break;
      case LMC_ACTION_MENU:
      case LMC_ACTION_MINIMIZE:
      case LMC_ACTION_MAXIMIZE:
	break;
      }
      return;
    }
  
  XTestFakeButtonEvent (source_xdisplay,
			button, True, 0);
}

static gboolean
on_button_press (LmcOutput    *output,
		 XButtonEvent *xev,
		 LmcGui       *gui)
{
  int width, height;
  double output_x, output_y;
  double source_x, source_y;

  lmc_output_get_size (gui->output, &width, &height);

  source_x = xev->x;
  source_y = xev->y;
  gui_translate_cursor_o2s (gui, &source_x, &source_y);

  output_x = xev->x;
  output_y = xev->y;

  gui_handle_button_press (gui, xev->button, xev->state, source_x, source_y, output_x, output_y);
  
  return TRUE;
}

static gboolean
gui_handle_button_release (LmcGui *gui,
			   int button)
{
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);

  if (button == gui->action_button)
  {
      if (gui->action == LMC_ACTION_MOVE)
	  gui_end_move (gui, gui->action_toplevel, button);

      gui->action = LMC_ACTION_NONE;
      gui->action_button = 0;
      gui->action_toplevel = NULL;

      return FALSE;
  }

  XTestFakeButtonEvent (source_xdisplay,
			button,
			False, 0);

  return TRUE;
}

static gboolean
on_button_release (LmcOutput    *output,
		   XButtonEvent *xev,
		   LmcGui       *gui)
{
  return gui_handle_button_release (gui, xev->button);
}

static void
gui_move_pointer (LmcGui  *gui,
		  double   x,
		  double   y)
{
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  int screen = lmc_root_get_screen (gui->root);
  int width, height;

  lmc_output_get_size (gui->output, &width, &height);

  gui_translate_cursor_o2s (gui, &x, &y);
  
  gui->pointer_x = x;
  gui->pointer_y = y;

  XTestFakeMotionEvent (source_xdisplay, screen, (int)(x), (int)(y), 0);

  XSync (source_xdisplay, False);
  
  gui_update_pointer (gui);
}

static void
gui_toggle_fullscreen (LmcGui *gui)
{
  Display *target_xdisplay = LMC_WINDOW_XDISPLAY (gui->output);
  Screen *target_screen = ScreenOfDisplay (target_xdisplay, lmc_root_get_screen (gui->root));
  XClientMessageEvent xev;
  Window target_xwindow = LMC_WINDOW_XWINDOW (gui->output);
  int width, height;

  lmc_output_get_size (gui->output, &width, &height);

  /* Fullscreen only make sense if the output and the target display have
   * the same dimensions
   */
  if (WidthOfScreen (target_screen) != width ||
      HeightOfScreen (target_screen) != height)
    return;
  
  xev.type = ClientMessage;
  xev.window = target_xwindow;
  xev.message_type = XInternAtom (target_xdisplay, "_NET_WM_STATE", False);
  xev.format = 32;
  xev.data.l[0] = 2; /* NET_WM_STATE_TOGGLE */
  xev.data.l[1] = XInternAtom (target_xdisplay, "_NET_WM_STATE_FULLSCREEN", False);
  xev.data.l[2] = 0; /* Only one property to change */
  xev.data.l[3] = 0;
  xev.data.l[4] = 0;
  xev.data.l[5] = 0;
      
  XSendEvent (target_xdisplay, RootWindowOfScreen (target_screen),
	      False, SubstructureNotifyMask | SubstructureRedirectMask,
	      (XEvent *)&xev);
}

static gboolean
on_key_press (LmcOutput *output,
	      XKeyEvent *xev,
	      LmcGui    *gui)
{
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  guint super_mod = lmc_display_get_super_modifier (LMC_WINDOW (gui->output)->display);
  KeyCode source_keycode;

  if ((xev->state & super_mod) != 0)
    {
      KeySym keysym;
      char buffer[32];

      XLookupString (xev, buffer, sizeof(buffer), &keysym, NULL);

      switch (keysym)
	{
	case XK_F11:
	  gui_toggle_fullscreen (gui);
	  break;
	case XK_q:
	  g_signal_emit_by_name (gui->output, "delete-window");
	  break;
	case XK_F1:
	  gui_switch_to_workspace (gui, 0);
	  break;
	case XK_F2:
	  gui_switch_to_workspace (gui, 1);
	  break;
	case XK_F3:
	  gui_switch_to_workspace (gui, 2);
	  break;
	case XK_F4:
	  gui_switch_to_workspace (gui, 3);
	  break;
	}
      
      return TRUE;
    }

  source_keycode = gui->keycode_map[xev->keycode];
    
  XTestFakeKeyEvent (source_xdisplay,
		     source_keycode, True,
		     0);

  if (!g_slist_find (gui->down_keys, GUINT_TO_POINTER ((guint)source_keycode)))
    gui->down_keys = g_slist_prepend (gui->down_keys,
				      GUINT_TO_POINTER ((guint)source_keycode));

  return TRUE;
}

static gboolean
on_key_release (LmcOutput *output,
		XKeyEvent *xev,
		LmcGui    *gui)
{
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  KeyCode source_keycode;
  
  source_keycode = gui->keycode_map[xev->keycode];
    
  XTestFakeKeyEvent (source_xdisplay,
		     source_keycode, False,
		     0);
  
  gui->down_keys = g_slist_remove (gui->down_keys,
				   GUINT_TO_POINTER ((guint)source_keycode));

  return FALSE;
}

static void
handle_motion (LmcGui *gui,
	       int output_x,
	       int output_y)
{
  double source_x, source_y;

  source_x = output_x;
  source_y = output_y;

  gui_translate_cursor_o2s (gui, &source_x, &source_y);
  
  if (gui->action_button)
    {
      switch (gui->action)
	{
	case LMC_ACTION_MOVE:
	  gui_update_move (gui, source_x, source_y);
	  break;
	case LMC_ACTION_RESIZE_LEFT:
	case LMC_ACTION_RESIZE_TOP_LEFT:
	case LMC_ACTION_RESIZE_BOTTOM_LEFT:
	case LMC_ACTION_RESIZE_BOTTOM:
	case LMC_ACTION_RESIZE_TOP:
	case LMC_ACTION_RESIZE_RIGHT:
	case LMC_ACTION_RESIZE_TOP_RIGHT:
	case LMC_ACTION_RESIZE_BOTTOM_RIGHT:
	  gui_update_resize (gui, source_x, source_y);
	  break;
	default:
	  g_assert_not_reached ();
	}
    }

  gui_move_pointer (gui, output_x, output_y);
}

static gboolean
on_motion_notify (LmcOutput    *output,
		  XMotionEvent *xev,
		  LmcGui       *gui)
{
  handle_motion (gui, (xev->x + 0.5), (xev->y + 0.5));
  
  return FALSE;
}

static gboolean
on_enter_notify (LmcOutput      *output,
		 XCrossingEvent *xev,
		 LmcGui         *gui)
{
  gui->have_pointer = TRUE;

  gui_move_pointer (gui, xev->x, xev->y);
  
  return FALSE;
}

static gboolean
on_leave_notify (LmcOutput      *output,
		 XCrossingEvent *xev,
		 LmcGui         *gui)
{
  gui->have_pointer = FALSE;

  gui_update_pointer (gui);

  return FALSE;
}

static gboolean
on_focus_out (LmcOutput         *output,
	      XFocusChangeEvent *xev,
	      LmcGui            *gui)
{
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  GSList *l;

  for (l = gui->down_keys; l; l = l->next)
    {
      XTestFakeKeyEvent (source_xdisplay,
			 GPOINTER_TO_UINT (l->data), False,
			 0);
    }
  
  g_slist_free (gui->down_keys);
  gui->down_keys = NULL;

  return FALSE;
}

static void
gui_update_maps (LmcGui   *gui,
		 gboolean  update_modmap,
		 gboolean  update_keymap)
{
  Display *source_xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  Display *target_xdisplay = LMC_WINDOW_XDISPLAY (gui->output);

  if (update_keymap)
    {
      int target_min_keycode, target_max_keycode;
      int source_min_keycode, source_max_keycode;
      int keysyms_per_keycode;
      KeySym *target_keymap;
      KeySym *source_keymap;
      KeyCode source_next_keycode;
      int i;
      
      XDisplayKeycodes (target_xdisplay, &target_min_keycode, &target_max_keycode);
      XDisplayKeycodes (source_xdisplay, &source_min_keycode, &source_max_keycode);

      target_keymap = XGetKeyboardMapping (target_xdisplay,
					   target_min_keycode,
					   target_max_keycode - target_min_keycode + 1,
					   &keysyms_per_keycode);
      
      source_keymap = g_new0 (KeySym,
			      (source_max_keycode - source_min_keycode + 1) * keysyms_per_keycode);
      source_next_keycode = source_min_keycode;
			     
      for (i = target_min_keycode;
	   i <= target_max_keycode && source_next_keycode <= source_max_keycode;
	   i++)
	{
	  KeySym *target_keysyms = target_keymap + (i - target_min_keycode) * keysyms_per_keycode;
	  int j;
	  gboolean found_key = FALSE;

	  for (j = 0; j < keysyms_per_keycode; j++)
	    if (target_keysyms[j] != 0)
	      found_key = TRUE;

	  if (found_key)
	    {
	      KeySym *source_keysyms = source_keymap + (source_next_keycode - source_min_keycode) * keysyms_per_keycode;
	      
	      for (j = 0; j < keysyms_per_keycode; j++)
		source_keysyms[j] = target_keysyms[j];

	      gui->keycode_map[i] = source_next_keycode;
	      source_next_keycode++;
	    }
	}

      XChangeKeyboardMapping (source_xdisplay,
			      source_min_keycode, keysyms_per_keycode,
			      source_keymap, source_max_keycode - source_min_keycode + 1);

      XFree (target_keymap);
      g_free (source_keymap);
    }

  if (update_modmap || update_keymap) /* Changing the keymap implies a change in the modmap */
    {
      XModifierKeymap *target_modmap;
      XModifierKeymap *source_modmap;
      int i;

      target_modmap = XGetModifierMapping (target_xdisplay);
      source_modmap = XNewModifiermap (target_modmap->max_keypermod);

      for (i = 0; i < 8 * target_modmap->max_keypermod; i++)
	{
	  if (target_modmap->modifiermap[i])
	    source_modmap->modifiermap[i] = gui->keycode_map[target_modmap->modifiermap[i]];
	  else
	    source_modmap->modifiermap[i] = 0;
	}

      XSetModifierMapping (source_xdisplay, source_modmap);

      XFreeModifiermap (source_modmap);
      XFreeModifiermap (target_modmap);
    }
}

static gboolean
on_mapping_notify (LmcDisplay     *display,
		   XMappingEvent  *xev,
		   LmcGui         *gui)
{
  if (xev->request == MappingModifier)
    gui_update_maps (gui, TRUE, FALSE);
  else if (xev->request == MappingKeyboard)
    gui_update_maps (gui, FALSE, TRUE);
  
  return FALSE;
}

/* Sets default pointing-arrow cursor on @indow */
static void
set_root_cursor (LmcWindow *window)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (window);
  Window xwindow = LMC_WINDOW_XWINDOW (window);
  Cursor cursor;

  cursor = XCreateFontCursor (xdisplay, XC_left_ptr);
  XDefineCursor (xdisplay, xwindow, cursor);
  XFreeCursor (xdisplay, cursor);
}

/* Sets an empty 1x1 cursor on @window */
static void
set_empty_cursor (LmcWindow *window)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (window);
  Window xwindow = LMC_WINDOW_XWINDOW (window);
  Pixmap pixmap;
  XGCValues gcv;
  GC gc;
  Cursor cursor;
  XColor dummy_color = { 0 };

  gcv.foreground = 0;
  
  pixmap = XCreatePixmap (xdisplay, xwindow, 1, 1, 1);
  gc = XCreateGC (xdisplay, pixmap, GCForeground, &gcv);

  XDrawPoint (xdisplay, pixmap, gc, 0, 0);

  cursor = XCreatePixmapCursor (xdisplay, pixmap, pixmap, &dummy_color, &dummy_color, 0, 0);
  XDefineCursor (xdisplay, xwindow, cursor);

  XFreeGC (xdisplay, gc);
  XFreePixmap (xdisplay, pixmap);
  XFreeCursor (xdisplay, cursor);
}

static void
gui_setup_pagers (LmcGui *gui)
{
  int width, height;
  int pager_x, pager_y,pager_width, pager_height;
  int i;

  lmc_output_get_size (gui->output, &width, &height);

  pager_width = width / 8;
  pager_height = height / 8;
  pager_x = width - pager_width;
  pager_y = height - gui->n_desktops * pager_height;

  if (gui->n_desktops == 1)
    lmc_output_set_show_pager (gui->output, FALSE);
  else
    {
      GdkPixbuf *pixbuf;
      
      pixbuf = lmc_load_datadir_pixbuf ("pager-decoration.png");
      if (pixbuf)
	{
	  /* Matches pager-decoration.png */
	  static const LmcBorderInfo pager_border = {
	    /*  left right top bottom */
	    7,    7,  7,     7,	/* border */
	    7,    7,  7,     7,	/* unscaled size */
	  };
	  
	  LmcBits *bits = lmc_bits_new_from_pixbuf (pixbuf);
	  
	  lmc_output_set_pager (gui->output,
				pager_x, pager_y, pager_width, pager_height * gui->n_desktops,
				bits, &pager_border);
	  
	  g_object_unref (pixbuf);
	  lmc_bits_unref (bits);
	}
    }

  for (i = 0; i < gui->n_desktops; i++)
    {
      gui->pagers[i] = lmc_output_add_viewport (gui->output,
						0, height * i, width, height,
						pager_x, pager_y + i * (height / 8), width / 8, height / 8);
      lmc_output_viewport_set_bgcolor (gui->pagers[i],
				       pager_colors[i][0],
				       pager_colors[i][1],
				       pager_colors[i][2]);
    }
}

LmcGui *
lmc_gui_new (LmcRoot   *root,
	     LmcOutput *output,
	     int        n_desktops)
{
  LmcDisplay *source_display;
  GList *toplevels;
  GList *l;
  LmcGui *gui;
  LmcDecorationFactory *decoration_factory;
  int width, height;

  source_display = LMC_WINDOW (root)->display;

  if ((lmc_display_get_extensions (source_display) & LMC_EXTENSION_TEST) != LMC_EXTENSION_TEST)
    {
      g_printerr ("Test extension is required\n");
      return NULL;
    }

  decoration_factory = lmc_decoration_factory_new ();
  if (!decoration_factory)
    return NULL;

  gui = g_object_new (LMC_TYPE_GUI, NULL);

  gui->root = root;
  gui->output = output;
  gui->decoration_factory = decoration_factory;
  gui->n_desktops = n_desktops;

  g_signal_connect (root, "toplevel-added", G_CALLBACK (on_toplevel_added), gui);
  g_signal_connect (root, "toplevel-restacked", G_CALLBACK (on_toplevel_restacked), gui);
  g_signal_connect (root, "cursor-changed", G_CALLBACK (on_cursor_changed), gui);
  g_signal_connect (root, "focus-changed", G_CALLBACK (on_focus_changed), gui);

  g_signal_connect (root, "event", G_CALLBACK (on_client_message), gui);
  
  lmc_window_select_input (LMC_WINDOW (output),
			   ButtonPressMask |
			   ButtonReleaseMask |
			   PointerMotionMask |
			   EnterWindowMask |
			   LeaveWindowMask |
			   KeyPressMask |
			   KeyReleaseMask |
			   FocusChangeMask);
  g_signal_connect (output, "event::ButtonPress", G_CALLBACK (on_button_press), gui);
  g_signal_connect (output, "event::ButtonRelease", G_CALLBACK (on_button_release), gui);
  g_signal_connect (output, "event::MotionNotify", G_CALLBACK (on_motion_notify), gui);
  g_signal_connect (output, "event::KeyPress", G_CALLBACK (on_key_press), gui);
  g_signal_connect (output, "event::KeyRelease", G_CALLBACK (on_key_release), gui);
  g_signal_connect (output, "event::EnterNotify", G_CALLBACK (on_enter_notify), gui);
  g_signal_connect (output, "event::LeaveNotify", G_CALLBACK (on_leave_notify), gui);
  g_signal_connect (output, "event::FocusOut", G_CALLBACK (on_focus_out), gui);

  g_signal_connect (LMC_WINDOW (output)->display, "event::MappingNotify",
		    G_CALLBACK (on_mapping_notify), gui);

  gui_setup_pagers (gui);

  lmc_output_viewport_set_bgcolor (lmc_output_get_main_viewport (gui->output),
				   pager_colors[0][0],
				   pager_colors[0][1],
				   pager_colors[0][2]);

  set_root_cursor (LMC_WINDOW (root));
  set_empty_cursor (LMC_WINDOW (output));

  gui_update_maps (gui, TRUE, TRUE);

  toplevels = lmc_root_get_toplevels (root);
  for (l = g_list_last (toplevels); l; l = l->prev)
    gui_add_toplevel (gui, l->data);

  gui_set_focus_toplevel (gui, lmc_root_get_focus_toplevel (root));

  gui->visible_source.x = 0;
  gui->visible_source.y = 0;

  lmc_output_get_size (gui->output, &width, &height);

  gui->visible_source.width = width;
  gui->visible_source.height = height;
  
  return gui;
}

void
lmc_gui_toggle_fullscreen (LmcGui *gui)
{
  gui_toggle_fullscreen (gui);
}

void
lmc_gui_set_background (LmcGui     *gui,
			const char *filename)
{
  GError *error = NULL;
  GdkPixbuf *pixbuf;

  pixbuf = gdk_pixbuf_new_from_file (filename, &error);
  if (!pixbuf)
    {
      g_printerr ("Cannot open background image: %s\n",
		  error->message);
      g_error_free (error);

      return;
    }
  
  lmc_output_set_background (gui->output,
			     lmc_bits_new_from_pixbuf (pixbuf));
  g_object_unref (pixbuf);
}

void
lmc_gui_cleanup (LmcGui *gui)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (gui->root);
  
  gui_switch_to_workspace (gui, 0);

  XFlush (xdisplay);
}

