#include <stdlib.h>
#include <glib.h>
#include <math.h>

#include "gui.h"
#include "output.h"

typedef struct _xy_pair Point;
typedef struct _xy_pair Vector;
struct _xy_pair {
  double x, y;
};

typedef struct _Model Model;
typedef struct _Object Object;
typedef struct _Spring Spring;

#define GRID_WIDTH  4
#define GRID_HEIGHT 4

#define MODEL_MAX_SPRINGS (GRID_WIDTH * GRID_HEIGHT * 2)

#define DEFAULT_SPRING_K  5.0
#define DEFAULT_FRICTION  1.4

struct _Spring {
  Object *a;
  Object *b;
  /* Spring position at rest, from a to b:
	offset = b.position - a.position
  */
  Vector offset;
};

struct _Object {
  Vector force;

  Point position;
  Vector velocity;

  double mass;
  double theta;

  int immobile;
};

struct _Model {
  int num_objects;
  Object *objects;

  int num_springs;
  Spring springs[MODEL_MAX_SPRINGS];

  Object *anchor_object;
  Vector anchor_offset;

  double friction;	/* Friction constant */
  double k;		/* Spring constant */

  double last_time;
  double steps;
};

static void
object_init (Object *object,
	     double position_x, double position_y,
	     double velocity_x, double velocity_y, double mass)
{
  object->force.x = 0;
  object->force.y = 0;

  object->position.x = position_x;
  object->position.y = position_y;

  object->velocity.x = velocity_x;
  object->velocity.y = velocity_y;

  object->mass = mass;

  object->immobile = 0;
}

static void
spring_init (Spring *spring,
	     Object *object_a, Object *object_b,
	     double offset_x, double offset_y)
{
  spring->a = object_a;
  spring->b = object_b;
  spring->offset.x = offset_x;
  spring->offset.y = offset_y;
}
  
static void
model_add_spring (Model *model,
		  Object *object_a, Object *object_b,
 		  double offset_x, double offset_y)
{
  Spring *spring;
  
  g_assert (model->num_springs < MODEL_MAX_SPRINGS);

  spring = &model->springs[model->num_springs];
  model->num_springs++;

  spring_init (spring, object_a, object_b, offset_x, offset_y);
}

static void
model_init_objects (Model *model, int x, int y, int width, int height)
{
  int grid_x, grid_y, i;
  int dest_x, dest_y;
  int v_x, v_y;
  double scale;

  i = 0;
  for (grid_y = 0; grid_y < GRID_HEIGHT; grid_y++)
    for (grid_x = 0; grid_x < GRID_WIDTH; grid_x++) {
      dest_x = x + grid_x * width / 3;
      dest_y = y + grid_y * width / 3;
      scale = (double) rand() / 2 / RAND_MAX;
      v_x = (dest_x - (x + width / 2)) * scale;
      v_y = (dest_y - (y + width / 2)) * scale;
      object_init (&model->objects[i],
		   x + width / 2,
		   y + height / 2,
		   v_x, v_y, 20);
      i++;
    }
}

static void
model_init_springs (Model *model, int x, int y, int width, int height)
{
  int grid_x, grid_y, i;
  const double hpad = (double) width / (GRID_WIDTH - 1);
  const double vpad = (double) height / (GRID_HEIGHT - 1);

  model->num_springs = 0;

  i = 0;
  for (grid_y = 0; grid_y < GRID_HEIGHT; grid_y++)
    for (grid_x = 0; grid_x < GRID_WIDTH; grid_x++) {
      if (grid_x > 0)
	model_add_spring (model,
			  &model->objects[i - 1],
			  &model->objects[i],
			  hpad, 0);

      if (grid_y > 0)
	model_add_spring (model,
			  &model->objects[i - GRID_WIDTH],
			  &model->objects[i],
			  0, vpad);

      i++;
    }
}

static Model *
model_new (int x, int y, int width, int height)
{
  Model *model;

  model = g_new (Model, 1);

  model->num_objects = GRID_WIDTH * GRID_HEIGHT;
  model->objects = g_new (Object, model->num_objects);

  model->num_springs = 0;

  model->anchor_object = NULL;

  model->friction = DEFAULT_FRICTION;
  model->k	  = DEFAULT_SPRING_K;
 
  model->last_time = 0;
  model->steps = 0;

  model_init_objects (model, x, y, width, height);
  model_init_springs (model, x, y, width, height);

  return model;
}

static void
object_apply_force (Object *object, double fx, double fy)
{
  object->force.x += fx;
  object->force.y += fy;
}

/* The model here can be understood as a rigid body of the spring's
 * rest shape, centered on the vector between the two object
 * positions. This rigid body is then connected by linear-force
 * springs to each object. This model does degnerate into a simple
 * spring for linear displacements, and does something reasonable for
 * rotation.
 *
 * There are other possibilities for handling the rotation of the
 * spring, and it might be interesting to explore something which has
 * better length-preserving properties. For example, with the current
 * model, an initial 180 degree rotation of the spring results in the
 * spring collapsing down to 0 size before expanding back to it's
 * natural size again.
 */
static void
spring_exert_forces (Spring *spring, double k)
{
  Vector da, db;
  Vector a, b;

  a = spring->a->position;
  b = spring->b->position;

  /* A nice vector diagram would likely help here, but my ASCII-art
   * skills aren't up to the task. Here's how to make your own
   * diagram:
   *
   * Draw a and b, and the vector AB from a to b
   * Find the center of AB
   * Draw spring->offset so that its center point is on the center of AB
   * Draw da from a to the initial point of spring->offset
   * Draw db from b to the final point of spring->offset
   *
   * The math below should be easy to verify from the diagram.
   */

  da.x = 0.5 * (b.x - a.x - spring->offset.x);
  da.y = 0.5 * (b.y - a.y - spring->offset.y);

  db.x = 0.5 * (a.x - b.x + spring->offset.x);
  db.y = 0.5 * (a.y - b.y + spring->offset.y);
  
  object_apply_force (spring->a, k * da.x, k * da.y);

  object_apply_force (spring->b, k * db.x, k * db.y);
}

static void
model_step_object (Model *model, Object *object)
{
  Vector acceleration;
  
  object->theta += 0.05;

  /* Slow down due to friction. */
  object->force.x -= model->friction * object->velocity.x;
  object->force.y -= model->friction * object->velocity.y;

  acceleration.x = object->force.x / object->mass;
  acceleration.y = object->force.y / object->mass;

  if (object->immobile) {
    object->velocity.x = 0;
    object->velocity.y = 0;
  } else {
    object->velocity.x += acceleration.x;
    object->velocity.y += acceleration.y;

    object->position.x += object->velocity.x;
    object->position.y += object->velocity.y;
  }

  object->force.x = 0.0;
  object->force.y = 0.0;
}

static int
model_step (Model *model, double t)
{
  int i, j, steps;

  model->steps += (t - model->last_time) / 0.03;
  model->last_time = t;
  steps = floor(model->steps);
  model->steps -= steps;

  for (j = 0; j < steps; j++)
    {
      for (i = 0; i < model->num_springs; i++)
	spring_exert_forces (&model->springs[i], model->k);

      for (i = 0; i < model->num_objects; i++)
	model_step_object (model, &model->objects[i]);
    }

  return j;
}

static void
bezier_patch_evaluate (Model *model, double u, double v,
		       double *patch_x, double *patch_y)
{
  double coeffs_u[4], coeffs_v[4];
  double x, y;
  int i, j;
  
  coeffs_u[0] = (1 - u) * (1 - u) * (1 - u);
  coeffs_u[1] = 3 * u * (1 - u) * (1 - u);
  coeffs_u[2] = 3 * u * u * (1 - u);
  coeffs_u[3] = u * u * u;

  coeffs_v[0] = (1 - v) * (1 - v) * (1 - v);
  coeffs_v[1] = 3 * v * (1 - v) * (1 - v);
  coeffs_v[2] = 3 * v * v * (1 - v);
  coeffs_v[3] = v * v * v;

  x = 0;
  y = 0;
  for (i = 0; i < 4; i++)
    for (j = 0; j < 4; j++)
      {
	x += coeffs_u[i] * coeffs_v[j] * model->objects[j * GRID_WIDTH + i].position.x;
	y += coeffs_u[i] * coeffs_v[j] * model->objects[j * GRID_WIDTH + i].position.y;
      }

  *patch_x = x;
  *patch_y = y;
}

static void
wobble (int u, int v,
	int x, int y, int width, int height,
	int *deformed_x, int *deformed_y,
	void *p)
{
  Model *model = p;
  double patch_x, patch_y;

  bezier_patch_evaluate (model,
			 (double) u / width, (double) v / height,
			 &patch_x, &patch_y);

  *deformed_x = patch_x;
  *deformed_y = patch_y;

}

static gboolean
wobbly_callback (LmcAnimation *animation, gpointer user_data)
{
  Model *model = user_data;
  LmcOutputWindow *owindow;
  double t;

  owindow = g_object_get_data (G_OBJECT (animation->toplevel),
			       "gui-output-window");
  if (owindow == NULL)
    return FALSE;

  t = g_timer_elapsed (animation->timer, NULL);
  if (model_step (model, t) == 0)
    return TRUE;

  lmc_toplevel_set_position (animation->toplevel,
			     model->objects[0].position.x,
			     model->objects[0].position.y);

  lmc_output_freeze (animation->output);
  
  lmc_output_window_set_deformation (owindow, wobble, model);

  lmc_output_thaw (animation->output);

  return TRUE;
}

void
destroy_animation (void *data)
{
  LmcAnimation *animation = data;

  lmc_animation_unref (animation);
}

static double
object_distance (Object *object, double x, double y)
{
  double dx, dy;

  dx = object->position.x - x;
  dy = object->position.y - y;

  return sqrt (dx*dx + dy*dy);
}

static Object *
model_find_nearest (Model *model, double x, double y)
{
  Object *object = &model->objects[0];
  double distance, min_distance = 0.0;
  int i;

  for (i = 0; i < model->num_objects; i++) {
    distance = object_distance (&model->objects[i], x, y);
    if (i == 0 || distance < min_distance) {
      min_distance = distance;
      object = &model->objects[i];
    }
  }

  return object;
}

static void
on_begin_move (LmcToplevel *toplevel,
	       Model	   *model)
{
  int x, y;

  lmc_toplevel_get_mouse_position (toplevel, &x, &y);

  if (model->anchor_object)
    model->anchor_object->immobile = 0;

  model->anchor_object = model_find_nearest (model, x, y);
  model->anchor_offset.x = x - model->anchor_object->position.x;
  model->anchor_offset.y = y - model->anchor_object->position.y;

  model->anchor_object->immobile = 1;
}

static void
on_update_move (LmcToplevel *toplevel,
		Model	    *model)
{
  int x, y;

  lmc_toplevel_get_mouse_position (toplevel, &x, &y);

  model->anchor_object->position.x = x - model->anchor_offset.x;
  model->anchor_object->position.y = y - model->anchor_offset.y;
}

static void
on_end_move (LmcToplevel *toplevel,
	     Model	 *model)
{
  if (model->anchor_object) {
    model->anchor_object->immobile = 0;
    model->anchor_object = NULL;
  }
}

static void
on_size_changed (LmcToplevel *toplevel,
		 Model	     *model)
{
  int x, y, width, height;

  lmc_toplevel_get_position (toplevel, &x, &y);
  lmc_toplevel_get_size (toplevel, &width, &height);
  model_init_springs (model, x, y, width, height);
}

void
lmc_wobble_setup (LmcToplevel *toplevel,
		  LmcGui      *gui)
{
  Model *model;
  int x, y, width, height;
  LmcBorderInfo border_info;

  lmc_toplevel_get_set_position (toplevel, &x, &y);
  lmc_toplevel_get_size (toplevel, &width, &height);
  lmc_toplevel_get_border_info (toplevel, &border_info);

  width += border_info.left + border_info.right;
  height += border_info.top + border_info.bottom;

  model = model_new (x, y, width, height);

  g_signal_connect (toplevel, "begin-move",
		    G_CALLBACK (on_begin_move), model);
  g_signal_connect (toplevel, "update-move",
		    G_CALLBACK (on_update_move), model);
  g_signal_connect (toplevel, "end-move",
		    G_CALLBACK (on_end_move), model);
  g_signal_connect (toplevel, "size-changed",
		    G_CALLBACK (on_size_changed), model);

  lmc_animation_new (gui, toplevel, wobbly_callback, model);
}
