#ifndef __LMC_ROOT_H__
#define __LMC_ROOT_H__

#include "bits.h"
#include "toplevel.h"
#include "window.h"

G_BEGIN_DECLS

#define LMC_TYPE_ROOT            (lmc_root_get_type ())
#define LMC_ROOT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_ROOT, LmcRoot))
#define LMC_ROOT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_ROOT, LmcRootClass))
#define LMC_IS_ROOT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_ROOT))
#define LMC_IS_ROOT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_ROOT))
#define LMC_ROOT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_ROOT, LmcRootClass))

typedef struct _LmcRoot       LmcRoot;

GType lmc_root_get_type  (void) G_GNUC_CONST;

LmcRoot *lmc_root_new (LmcDisplay *ldisplay,
		       int         screen,
		       gboolean    update_root);

void   lmc_root_get_size      (LmcRoot  *root,
			       int      *width,
			       int      *height);
GList *lmc_root_get_toplevels (LmcRoot  *root);
int    lmc_root_get_screen    (LmcRoot  *root);
void   lmc_root_get_cursor    (LmcRoot  *root,
			       LmcBits **bits,
			       int      *hot_x,
			       int      *hot_y);

LmcToplevel *lmc_root_get_focus_toplevel      (LmcRoot *root);
LmcToplevel *lmc_root_get_grab_focus_toplevel (LmcRoot *root);

G_END_DECLS

#endif /* __LMC_ROOT_H__ */
