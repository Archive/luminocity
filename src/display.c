#include "display.h"
#include "window.h"
#include "lmc-marshal.h"

#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/XTest.h>
#include <X11/extensions/sync.h>
#include <X11/keysym.h>

static guint event_signal;
     
G_DEFINE_TYPE (LmcDisplay, lmc_display, G_TYPE_OBJECT)

static void
lmc_display_finalize (GObject *object)
{
  LmcDisplay *ldisplay = LMC_DISPLAY (object);

  g_main_context_unref (ldisplay->main_context);
  g_hash_table_destroy (ldisplay->windows);
  
  G_OBJECT_CLASS (lmc_display_parent_class)->finalize (object);
}

static void
lmc_display_class_init (LmcDisplayClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_display_finalize;

  
  event_signal =  g_signal_new ("event",
				G_OBJECT_CLASS_TYPE (object_class),
				G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
				0,
				g_signal_accumulator_true_handled, NULL,
				lmc_marshal_BOOLEAN__POINTER,
				G_TYPE_BOOLEAN, 1, G_TYPE_POINTER);
}

static void
lmc_display_init (LmcDisplay *ldisplay)
{
  ldisplay->windows = g_hash_table_new (NULL, NULL);
  ldisplay->super_modifier = (guint)-1;
}

void
lmc_display_add_window (LmcDisplay *ldisplay,
			LmcWindow  *lwindow)
{
  g_return_if_fail (lmc_display_lookup (ldisplay, lwindow->xwindow) == NULL);
  
  g_hash_table_insert (ldisplay->windows, (gpointer)lwindow->xwindow, lwindow);
}

void
lmc_display_remove_window (LmcDisplay *ldisplay,
			   LmcWindow  *lwindow)
{
  g_return_if_fail (lmc_display_lookup (ldisplay, lwindow->xwindow) != NULL);
  
  g_hash_table_remove (ldisplay->windows, (gpointer)lwindow->xwindow);
}

LmcWindow *
lmc_display_lookup (LmcDisplay *ldisplay,
		    Window      xwindow)
{
  LmcWindow *lwindow = g_hash_table_lookup (ldisplay->windows, (gpointer)xwindow);

  return lwindow;
}

LmcExtension
lmc_display_get_extensions (LmcDisplay *ldisplay)
{
  return ldisplay->extensions;
}

static gboolean
on_mapping_notify (LmcDisplay     *ldisplay,
		   XMappingEvent  *xev)
{
  ldisplay->super_modifier = (guint)-1;
  
  return FALSE;
}

LmcDisplay *
lmc_display_new (Display  *xdisplay)
{
  LmcDisplay *ldisplay;
  int major, minor;

  g_type_init ();

  ldisplay = g_object_new (LMC_TYPE_DISPLAY, NULL);
  
  ldisplay->xdisplay = xdisplay;
  ldisplay->extensions = 0;

  if (XFixesQueryExtension (xdisplay, 
			    &ldisplay->xfixes_event_base,
			    &ldisplay->xfixes_error_base))
    ldisplay->extensions |= LMC_EXTENSION_FIXES;
  if (XDamageQueryExtension (xdisplay, 
			     &ldisplay->damage_event_base,
			     &ldisplay->damage_error_base))
    ldisplay->extensions |= LMC_EXTENSION_DAMAGE;
  if (XCompositeQueryExtension (xdisplay,
				&ldisplay->composite_event_base,
				&ldisplay->composite_error_base))
    ldisplay->extensions |= LMC_EXTENSION_COMPOSITE;
  if (XTestQueryExtension (xdisplay,
			   &ldisplay->test_event_base,
			   &ldisplay->test_error_base,
			   &major, &minor))
    ldisplay->extensions |= LMC_EXTENSION_TEST;
  if (XSyncQueryExtension (xdisplay,
			   &ldisplay->sync_event_base,
			   &ldisplay->sync_error_base))
    ldisplay->extensions |= LMC_EXTENSION_SYNC;

  g_signal_connect (ldisplay, "event::MappingNotify",
		    G_CALLBACK (on_mapping_notify), NULL);
  
  return ldisplay;
}

typedef struct {
  LmcDisplay *display;
  int status;
} LmcErrorHandler;

static int (*old_error_handler) (Display *, XErrorEvent *);
static GSList *error_handlers;

static int
error_handler (Display     *display,
	       XErrorEvent *xevent)
{
  GSList *l;
  
  for (l = error_handlers; l; l = l->next)
    {
      LmcErrorHandler *handler = l->data;
      
      if (handler->display->xdisplay == display)
	{
	  handler->status = xevent->error_code;
	  return 0;
	}
    }

  g_assert_not_reached ();

  return 0;
}

void
lmc_display_error_trap_push (LmcDisplay *display)
{
  LmcErrorHandler *handler;
  
  g_return_if_fail (LMC_IS_DISPLAY (display));

  handler = g_new (LmcErrorHandler, 1);
  handler->status = 0;
  handler->display = display;

  if (!error_handlers)
    old_error_handler = XSetErrorHandler (error_handler);
        
  error_handlers = g_slist_prepend (error_handlers, handler);
}

int
lmc_display_error_trap_pop (LmcDisplay *display)
{
  GSList *l;
  int status = 0;
  
  g_return_val_if_fail (LMC_IS_DISPLAY (display), 0);

  for (l = error_handlers; l; l = l->next)
    {
      LmcErrorHandler *handler = l->data;
      
      if (handler->display == display)
	{
	  status = handler->status;
	  
	  error_handlers = g_slist_delete_link (error_handlers, l);

	  if (!error_handlers)
	    {
	      XSetErrorHandler (old_error_handler);
	      old_error_handler = NULL;
	    }

	  return status;
	}
    }

  g_critical ("lmc_display_error_trap_pop() called without an active error handler");

  return status;
}

guint
lmc_display_get_super_modifier (LmcDisplay *ldisplay)
{
  XModifierKeymap *modmap;
  KeySym *keymap;
  int min_keycode, max_keycode, keysyms_per_keycode;
  int i, j, k;

  if (ldisplay->super_modifier != (guint)-1)
    return ldisplay->super_modifier;

  XDisplayKeycodes (ldisplay->xdisplay, &min_keycode, &max_keycode);
  
  modmap = XGetModifierMapping (ldisplay->xdisplay);
  keymap = XGetKeyboardMapping (ldisplay->xdisplay, min_keycode, max_keycode - min_keycode + 1,
				&keysyms_per_keycode);
  for (i = 0; i < 8; i++)
    {
      for (j = 0; j < modmap->max_keypermod; j++)
	{
	  KeyCode keycode = modmap->modifiermap[i * modmap->max_keypermod + j];
	  KeySym *keysyms = keymap + (keycode - min_keycode) * keysyms_per_keycode;

	  for (k = 0; k < keysyms_per_keycode; k++)
	    {
	      KeySym keysym = keysyms[k];
	      
	      if (keysym == XK_Super_L || keysym == XK_Super_R)
		{
		  ldisplay->super_modifier = (1 << i);
		  return ldisplay->super_modifier;
		}
	    }
	}
    }

  XFree (keymap);
  XFreeModifiermap (modmap);

  g_warning ("Warning: could not find any key mapped to Super_L or Super_R\n");
  ldisplay->super_modifier = 0;
  return ldisplay->super_modifier;
}

void
lmc_display_do_event (LmcDisplay  *ldisplay,
		      XEvent      *xev,
		      const char  *detail)
{
  gboolean result;
  
  g_signal_emit (ldisplay, event_signal, g_quark_from_string (detail), xev, &result);
}
