#ifndef __LMC_GUI_H__
#define __LMC_GUI_H__

#include "root.h"
#include "output.h"

#define GUI_MAX_DESKTOPS 4

G_BEGIN_DECLS

#define LMC_TYPE_GUI            (lmc_gui_get_type ())
#define LMC_GUI(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_GUI, LmcGui))
#define LMC_GUI_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_GUI, LmcGuiClass))
#define LMC_IS_GUI(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_GUI))
#define LMC_IS_GUI_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_GUI))
#define LMC_GUI_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_GUI, LmcGuiClass))

typedef struct _LmcGui       LmcGui;

GType lmc_gui_get_type  (void) G_GNUC_CONST;

LmcGui *lmc_gui_new (LmcRoot   *root,
		     LmcOutput *output,
		     int        n_desktops);

void lmc_gui_toggle_fullscreen (LmcGui *gui);

void lmc_gui_set_background (LmcGui     *gui,
			     const char *filename);
void lmc_gui_cleanup        (LmcGui     *gui);


typedef struct _LmcAnimation LmcAnimation;
typedef gboolean (*LmcAnimationFunc) (LmcAnimation *animation,
				      gpointer      user_data);

struct _LmcAnimation
{
  LmcGui *gui;
  LmcToplevel *toplevel;
  LmcOutput *output;
  LmcOutputWindow *owindow;
  GTimer *timer;
  guint ref_count;
  LmcAnimationFunc animation_func;
  gpointer user_data;
};

LmcAnimation *lmc_animation_new   (LmcGui           *gui,
				   LmcToplevel      *toplevel,
				   LmcAnimationFunc  animation_func,
				   gpointer          user_data);
void          lmc_animation_ref   (LmcAnimation *animation);
void          lmc_animation_unref (LmcAnimation *animation);
void          lmc_animation_reset (LmcAnimation *animation);

G_END_DECLS

#endif /* __LMC_GUI_H__ */
