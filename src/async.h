#ifndef __LMC_ASYNC_H__
#define __LMC_ASYNC_H__

#include <glib.h>
#include <X11/Xlib.h>

G_BEGIN_DECLS

typedef struct _LmcAsyncErrorTrap LmcAsyncErrorTrap;

typedef Bool (*LmcAsyncErrorCallback)       (int            type,
					     void          *user_data);
typedef void (*LmcAsyncGetPropertyCallback) (Atom           type,
					     int            format,
					     unsigned long  n_items,
					     unsigned char *data,
					     void          *user_data);

void LmcAsyncGetProperty (Display                    *dpy,
			  Window                      window,
			  Atom                        property,
			  LmcAsyncGetPropertyCallback callback,
			  LmcAsyncErrorCallback       error_callback,
			  void                       *user_data);

LmcAsyncErrorTrap *lmc_async_error_trap_set   (Display *dpy);
void               lmc_async_error_trap_unset (LmcAsyncErrorTrap *trap);

G_END_DECLS

#endif /* __LMC_ASYNC_H__ */
