#ifndef __LMC_TEXTURE_H__
#define __LMC_TEXTURE_H__

#include "bits.h"
#include "types.h"

#include <X11/Xutil.h>		/* For Region */
#include <GL/gl.h>		/* For GLuint */

G_BEGIN_DECLS

typedef struct _LmcTexture LmcTexture;

struct _LmcTexture
{
  /*< private >*/
  int ref_count;
  
  /*< public >*/
  int width, height;
  LmcBits *bits;

  /* This is modified by output.c */
  Region update;

  /*< private >*/
  GLuint name;
  int pad_width, pad_height;

  int n_x_tiles, n_y_tiles;
  int *tile_x_position, *tile_x_size;
  int *tile_y_position, *tile_y_size;
  GLuint *tiles;

  GMutex *grid_mutex;
  int grid_width, grid_height;
  LmcPoint *grid;

  LmcBits *quantum_bits;

  GLuint base_texture;
};

LmcTexture* lmc_texture_new    (LmcBits    *bits);
LmcTexture* lmc_texture_ref    (LmcTexture *texture);
void        lmc_texture_unref  (LmcTexture *texture);
void        lmc_texture_delete (LmcTexture *texture);

void lmc_texture_set_deformation (LmcTexture         *texture,
				  LmcDeformationFunc  func,
				  void               *data);
void lmc_texture_update_rect (LmcTexture    *texture,
			      XRectangle    *rect);
void lmc_texture_draw        (LmcTexture    *texture,
			      double         alpha,
			      int            x,
			      int            y);
void lmc_texture_draw_border (LmcTexture    *texture,
			      double         alpha,
			      XRectangle    *base_rect,
			      LmcBorderInfo *border_info);

G_END_DECLS

#endif /* __LMC_TEXTURE_H__ */
