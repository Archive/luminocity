#ifndef __LMC_OUTPUT_H__
#define __LMC_OUTPUT_H__

#include "bits.h"
#include "types.h"
#include "window.h"

G_BEGIN_DECLS

#define LMC_TYPE_OUTPUT            (lmc_output_get_type ())
#define LMC_OUTPUT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_OUTPUT, LmcOutput))
#define LMC_OUTPUT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_OUTPUT, LmcOutputClass))
#define LMC_IS_OUTPUT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_OUTPUT))
#define LMC_IS_OUTPUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_OUTPUT))
#define LMC_OUTPUT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_OUTPUT, LmcOutputClass))

typedef struct _LmcOutput         LmcOutput;
typedef struct _LmcOutputViewport LmcOutputViewport;
typedef struct _LmcOutputWindow   LmcOutputWindow;
typedef void (* LmcOutputBreadCrumbFunc) (gpointer data);

GType lmc_output_get_type (void);

LmcOutput *lmc_output_new (LmcDisplay *ldisplay,
			   int         screen,
			   int         width,
			   int         height);
void lmc_output_get_size (LmcOutput *output,
			  int	    *width,
			  int	    *height);
void lmc_output_destroy (LmcOutput *output);

void lmc_output_freeze (LmcOutput *output);
void lmc_output_thaw   (LmcOutput *output);

void lmc_output_queue_bread_crumb (LmcOutput                *output,
				   LmcOutputBreadCrumbFunc   func,
				   gpointer                  data);

LmcOutputViewport *lmc_output_get_main_viewport (LmcOutput *output);
LmcOutputViewport *lmc_output_add_viewport      (LmcOutput *output,
						 int        src_x,
						 int        src_y,
						 int        src_width,
						 int        src_height,
						 int        dest_x,
						 int        dest_y,
						 int        dest_width,
						 int        dest_height);

void		 lmc_output_set_visible_rect (LmcOutput *output,
					      XRectangle *rect);
void		 lmc_output_get_visible_rect (LmcOutput *output,
					      XRectangle *rect);

void             lmc_output_set_background (LmcOutput *output,
					    LmcBits   *bits);
void             lmc_output_set_cursor     (LmcOutput *output,
					    LmcBits   *bits,
					    int        x,
					    int        y);
/* Set the pager border */
void             lmc_output_set_pager (LmcOutput           *output,
				       int                  x,
				       int                  y,
				       int                  width,
				       int                  height,
				       LmcBits             *bits,
				       const LmcBorderInfo *border_info);

LmcOutputWindow *lmc_output_create_window  (LmcOutput *output);

void lmc_output_viewport_offset (LmcOutputViewport *viewport,
				 int                x_offset,
				 int                y_offset);
void lmc_output_viewport_set_bgcolor (LmcOutputViewport *viewport,
				      double             red,
				      double             green,
				      double             blue);

void lmc_output_window_set_bits        (LmcOutputWindow *owindow,
					LmcBits         *bits);
void lmc_output_window_set_alpha       (LmcOutputWindow *owindow,
					double           alpha);
void lmc_output_window_set_deformation (LmcOutputWindow    *owindow,
					LmcDeformationFunc  func,
					void               *data);

void lmc_output_window_destroy  (LmcOutputWindow *owindow);

void lmc_output_window_resize  (LmcOutputWindow *owindow,
				int              width,
				int              height);
void lmc_output_window_move    (LmcOutputWindow *owindow,
				int              x,
				int              y);
void lmc_output_window_restack (LmcOutputWindow *owindow,
				LmcOutputWindow *above);
void lmc_output_window_modify  (LmcOutputWindow *owindow,
				int              x,
				int              y,
				int              width,
				int              height);

void lmc_output_set_show_pager (LmcOutput *output,
				gboolean show_pager);

G_END_DECLS

#endif /* __LMC_OUTPUT_H__ */
