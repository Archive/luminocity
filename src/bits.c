#include "bits.h"

LmcBits *
lmc_bits_new (LmcBitsFormat  format,
	      int            width,
	      int            height,
	      guchar        *data,
	      int            rowstride,
	      GDestroyNotify notify,
	      gpointer       notify_data)
{
  LmcBits *bits = g_new0 (LmcBits, 1);
  
  bits->ref_count = 1;
  bits->locked = FALSE;
  
  bits->format = format;
  bits->width = width;
  bits->height = height;
  bits->data_ = data;
  bits->rowstride = rowstride;
  bits->notify = notify;
  bits->notify_data = notify_data;

  bits->mutex = g_mutex_new ();
  bits->cond = g_cond_new ();

  return bits;
}

LmcBits *
lmc_bits_new_from_pixbuf (GdkPixbuf *pixbuf)
{
  return lmc_bits_new (gdk_pixbuf_get_has_alpha (pixbuf) ? LMC_BITS_RGBA_MSB_32 : LMC_BITS_RGB_24,
		       gdk_pixbuf_get_width (pixbuf),
		       gdk_pixbuf_get_height (pixbuf),
		       gdk_pixbuf_get_pixels (pixbuf),
		       gdk_pixbuf_get_rowstride (pixbuf),
		       (GDestroyNotify)g_object_unref,
		       g_object_ref (pixbuf));
}

LmcBits *
lmc_bits_ref (LmcBits *bits)
{
  g_atomic_int_inc (&bits->ref_count);

  return bits;
}

void
lmc_bits_unref (LmcBits *bits)
{
  if (g_atomic_int_dec_and_test (&bits->ref_count))
    {
      if (bits->notify)
	bits->notify (bits->notify_data);
      
      g_mutex_free (bits->mutex);
      g_cond_free (bits->cond);
      
      g_free (bits);
    }
}


guchar *
lmc_bits_lock (LmcBits *bits,
	       gboolean wait)
{
  guchar *result;
  
  g_mutex_lock (bits->mutex);

  while (wait && bits->locked)
    g_cond_wait (bits->cond, bits->mutex);

  if (bits->locked)
    result =  NULL;
  else
    result = bits->data_;

  bits->locked = TRUE;
  
  g_mutex_unlock (bits->mutex);

  return result;
}

void
lmc_bits_unlock (LmcBits *bits)
{
  g_mutex_lock (bits->mutex);

  bits->locked = FALSE;
  g_cond_broadcast (bits->cond);

  g_mutex_unlock (bits->mutex);
}

gboolean
lmc_bits_has_alpha (LmcBits *bits)
{
  switch (bits->format)
    {
    case LMC_BITS_RGB_16:
      return FALSE;
    case LMC_BITS_RGB_24:
      return FALSE;
    case LMC_BITS_RGB_32:
      return FALSE;
    case LMC_BITS_RGBA_MSB_32:
      return TRUE;
    case LMC_BITS_ARGB_32:
      return TRUE;
    default:
      return FALSE;
    }
}

void
lmc_bits_get_pixel (LmcBits   *bits,
		    int       x,
		    int       y,
		    LmcPixel *pixel)
{
  gushort s;
  gulong l;
  guchar *p;

  switch (bits->format) {
  case LMC_BITS_RGB_16:
    s = * (gushort *) (bits->data_ + y * bits->rowstride + x * 2);
    pixel->red = ((s >> 11) & 0x1f) << 3;
    pixel->green = ((s >> 5) & 0x3f) << 2;
    pixel->blue = ((s >> 0) & 0x1f) << 3;
    pixel->alpha = 0xff;
    break;
  case LMC_BITS_RGB_24:
    p = (bits->data_ + y * bits->rowstride + x * 3);
    pixel->red = p[0];
    pixel->green = p[1];
    pixel->blue = p[2];
    break;
  case LMC_BITS_RGB_32:
    l = * (gulong *) (bits->data_ + y * bits->rowstride + x * 4);
    pixel->red = (l >> 16) & 0xff;
    pixel->green = (l >> 8) & 0xff;
    pixel->blue = (l >> 0) & 0xff;
    pixel->alpha = 0xff;
    break;
  case LMC_BITS_RGBA_MSB_32:
    l = * (gulong *) (bits->data_ + y * bits->rowstride + x * 4);
    pixel->red = (l >> 0) & 0xff;
    pixel->green = (l >> 8) & 0xff;
    pixel->blue = (l >> 16) & 0xff;
    pixel->alpha = (l >> 24) & 0xff;
    break;
  case LMC_BITS_ARGB_32:
    l = * (gulong *) (bits->data_ + y * bits->rowstride + x * 4);
    pixel->alpha = (l >> 24) & 0xff;
    pixel->red = (l >> 16) & 0xff;
    pixel->green = (l >> 8) & 0xff;
    pixel->blue = (l >> 0) & 0xff;
    break;
  }
}

void
lmc_bits_set_pixel (LmcBits   *bits,
		    int       x,
		    int       y,
		    const LmcPixel *pixel)
{
  gushort s;
  gulong l;
  guchar *p;

  switch (bits->format) {
  case LMC_BITS_RGB_16:
    s = ((pixel->red >> 3) << 11) |
      ((pixel->green >> 2) << 5) |
      ((pixel->blue >> 3) << 0);
    * (gushort *) (bits->data_ + y * bits->rowstride + x * 2) = s;
    break;
  case LMC_BITS_RGB_24:
    p = (bits->data_ + y * bits->rowstride + x * 3);
    p[0] = pixel->red;
    p[1] = pixel->green;
    p[2] = pixel->blue;
    break;
  case LMC_BITS_RGB_32:
    l = (pixel->red << 16) | (pixel->green << 8) | (pixel->blue << 0);
    * (gulong *) (bits->data_ + y * bits->rowstride + x * 4) = l;
    break;
  case LMC_BITS_RGBA_MSB_32:
    l = (pixel->red << 0) | (pixel->green << 8) |
      (pixel->blue << 16) | (pixel->alpha << 24);
    * (gulong *) (bits->data_ + y * bits->rowstride + x * 4) = l;
    break;
  case LMC_BITS_ARGB_32:
    l = (pixel->alpha << 24) | (pixel->red << 16) |
      (pixel->green << 8) | (pixel->blue << 0);
    * (gulong *) (bits->data_ + y * bits->rowstride + x * 4) = l;
    break;
  }
}

