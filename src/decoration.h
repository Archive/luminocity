#ifndef __LMC_DECORATION_H__
#define __LMC_DECORATION_H__

#include "bits.h"
#include "types.h"

G_BEGIN_DECLS

#define LMC_TYPE_DECORATION_FACTORY            (lmc_decoration_factory_get_type ())
#define LMC_DECORATION_FACTORY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_DECORATION_FACTORY, LmcDecorationFactory))
#define LMC_DECORATION_FACTORY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_DECORATION_FACTORY, LmcDecorationFactoryClass))
#define LMC_IS_DECORATION_FACTORY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_DECORATION_FACTORY))
#define LMC_IS_DECORATION_FACTORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_DECORATION_FACTORY))
#define LMC_DECORATION_FACTORY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_DECORATION_FACTORY, LmcDecorationFactoryClass))

#define LMC_TYPE_DECORATION            (lmc_decoration_get_type ())
#define LMC_DECORATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LMC_TYPE_DECORATION, LmcDecoration))
#define LMC_DECORATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  LMC_TYPE_DECORATION, LmcDecorationClass))
#define LMC_IS_DECORATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LMC_TYPE_DECORATION))
#define LMC_IS_DECORATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  LMC_TYPE_DECORATION))
#define LMC_DECORATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  LMC_TYPE_DECORATION, LmcDecorationClass))

typedef struct _LmcDecorationFactory LmcDecorationFactory;
typedef struct _LmcDecoration        LmcDecoration;

typedef enum {
  LMC_ACTION_NONE,
  LMC_ACTION_NOOP,
  LMC_ACTION_MOVE,
  LMC_ACTION_MENU,
  LMC_ACTION_MINIMIZE,
  LMC_ACTION_MAXIMIZE,
  LMC_ACTION_CLOSE,
  LMC_ACTION_RESIZE_LEFT,
  LMC_ACTION_RESIZE_RIGHT,
  LMC_ACTION_RESIZE_TOP,
  LMC_ACTION_RESIZE_TOP_LEFT,
  LMC_ACTION_RESIZE_TOP_RIGHT,
  LMC_ACTION_RESIZE_BOTTOM,
  LMC_ACTION_RESIZE_BOTTOM_LEFT,
  LMC_ACTION_RESIZE_BOTTOM_RIGHT,
} LmcAction;

GType lmc_decoration_factory_get_type  (void) G_GNUC_CONST;

LmcDecorationFactory *lmc_decoration_factory_new               (void);
LmcDecoration        *lmc_decoration_factory_create_decoration (LmcDecorationFactory *factory);

GType lmc_decoration_get_type  (void) G_GNUC_CONST;

void                 lmc_decoration_set_title       (LmcDecoration *decoration,
						     const char    *title);
void                 lmc_decoration_set_window_size (LmcDecoration *decoration,
						     int            width,
						     int            height);
const LmcBorderInfo *lmc_decoration_get_border      (LmcDecoration *decoration);
gboolean             lmc_decoration_contains        (LmcDecoration *decoration,
						     int            x,
						     int            y,
						     int            width,
						     int            height);

LmcAction            lmc_decoration_get_action      (LmcDecoration *decoration,
						     int            x,
						     int            y);

void                 lmc_decoration_draw (LmcDecoration *decoration,
					  LmcBits       *bits);

G_END_DECLS

#endif /* __LMC_DECORATION_H__ */
