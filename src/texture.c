#include "texture.h"

#include <string.h>

#include <GL/glu.h>

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#define PIXEL_TYPE GL_UNSIGNED_BYTE
#else
#define PIXEL_TYPE GL_UNSIGNED_INT_8_8_8_8_REV
#endif

/*
 * OVERLAP: when we divide the full-resolution image into
 *          tiles to deal with hardware limitations, we overlap
 *          tiles by this much. This means that we can scale
 *          down by up to OVERLAP before we start getting
 *          seems.
 */

#define OVERLAP 32
#define OVERLAP_LEVEL 5

/* BASE_SCALE: when we've scaled down (in all directions) by at least
 *       this amount, we switch to using an untiled version
 *       of the source texture. Using a value of BASE_SCALE that
 *       is smaller than OVERLAP allows handling of asymetric
 *       scaling without showing seams. (For strongly asymetic
 *       scaling we'll get artifacts anyways with linear 
 *       mipmapping, so there is no point in making this much
 *       bigger)
 *
 *       A BASE_SCALE value of 0 would mean "create an untiled
 *       pixmap as soon as hw limits allow".
 */
#define BASE_SCALE 16
#define BASE_LEVEL 4

/* QUANTUM: we don't want to store all mipmapped levels, but
 *       if we don't, then doing incremental updates is hard,
 *       since the area we have to update gets larger and
 *       larger on smaller textures.
 *
 *       We save a single mipmap level scaled down by QUANTUM
 *       (untiled). Then to recompute, we quantize the update
 *       area to units of QUANTUM, compute and update all
 *       higher levels incrementally. Once we hit the QUANTUM
 *       level we compute and store full mipmaps.
 *
 *       Larger quantum values cause more overhead for incremental
 *       updates, but less extra memory. We pick QUANTUM the
 *       same as BASE_SCALE by default
 */
#define QUANTUM BASE_SCALE
#define QUANTUM_LEVEL BASE_LEVEL

/*  MAX_WASTE: The maximum dimension of blank area we'll accept
 *       in a pixmap. Bigger values use less textures, smaller
 *       values less texture memory. The current value of 
 *       256 means that the smallest texture we'll split to
 *       save texture memory is 513x512. (That will be split into
 *       a 512x512 and, if overlap is 32, a 64x512 texture)
 */
#define MAX_WASTE 256

#define TILE_NAME(texture,x,y) ((texture)->tiles[(texture)->n_x_tiles * (y) + (x)])

/* TEXTURE_COORD_OFFSET: A mysterious, empirically determined, offset to
 * add to the window coordinates when computing texture coordinates.
 * This is probably covering up for an off-by-one somewhere.
 */
#define TEXTURE_COORD_OFFSET 0.12

/* We're assuming tile sizes are always a multiple of this amount */
#define WOBBLE_TILE_SIZE 32


#undef DEBUG

static void
dump_error (const char *context)
{
  /* glGetError() is a server roundtrip */
#if 0
  GLenum err;

  err = glGetError ();
  if (err != GL_NO_ERROR)
    {
      const GLubyte *message = gluErrorString (err);
      g_printerr ("GL Error: %s [at %s]\n", message, context);
    }
#endif
}


LmcTexture *
lmc_texture_new (LmcBits *bits)
{
  LmcTexture *texture = g_new0 (LmcTexture, 1);

  texture->ref_count = 1;

  texture->bits = lmc_bits_ref (bits);

  texture->width = bits->width;
  texture->height = bits->height;
  texture->pad_width = 4;
  texture->pad_height = 4;
  texture->update = XCreateRegion ();

  texture->grid_mutex = g_mutex_new ();

  while (texture->pad_width < bits->width)
    texture->pad_width <<= 1;
  while (texture->pad_height < bits->height)
    texture->pad_height <<= 1;

  return texture;
}

LmcTexture *
lmc_texture_ref (LmcTexture *texture)
{
  g_atomic_int_inc (&texture->ref_count);

  return texture;
}

void
lmc_texture_unref (LmcTexture *texture)
{
  if (g_atomic_int_dec_and_test (&texture->ref_count))
    {
      lmc_bits_unref (texture->bits);

      XDestroyRegion (texture->update);
      g_assert (!texture->tiles);

      if (texture->quantum_bits)
	lmc_bits_unref (texture->quantum_bits);
      
      g_free (texture->grid);
      g_mutex_free (texture->grid_mutex);

      g_free (texture->tile_x_position);
      g_free (texture->tile_x_size);
      g_free (texture->tile_y_position);
      g_free (texture->tile_y_size);
      
      g_free (texture);
    }
}

void
lmc_texture_set_deformation (LmcTexture         *texture,
			     LmcDeformationFunc  func,
			     void               *data)
{
  int x, y, deformed_x, deformed_y;
  LmcPoint *grid;
  LmcBits *bits;

  if (func != NULL)
    {
      bits = texture->bits;

      texture->grid_width =
	(bits->width + WOBBLE_TILE_SIZE - 1) / WOBBLE_TILE_SIZE + 1;
      texture->grid_height =
	(bits->height + WOBBLE_TILE_SIZE - 1) / WOBBLE_TILE_SIZE + 1;
      grid = g_new0 (LmcPoint, texture->grid_width * texture->grid_height);

      for (y = 0; y < texture->grid_height; y++)
	for (x = 0; x < texture->grid_width; x++)
	  {
	    func (x * WOBBLE_TILE_SIZE, y * WOBBLE_TILE_SIZE,
		  0, 0, bits->width, bits->height,
		  &deformed_x, &deformed_y,
		  data);
	    grid[x + y * texture->grid_width].x = deformed_x;
	    grid[x + y * texture->grid_width].y = deformed_y;
	  }
    }
  else
    grid = NULL;

  g_mutex_lock (texture->grid_mutex);

  if (texture->grid)
    g_free (texture->grid);
  texture->grid = grid;

  g_mutex_unlock (texture->grid_mutex);
}

void
lmc_texture_delete (LmcTexture *texture)
{
  glDeleteTextures (texture->n_x_tiles * texture->n_y_tiles,
		    texture->tiles);

  if (texture->base_texture)
    glDeleteTextures (1, &texture->base_texture);
  
  g_free (texture->tiles);
  texture->tiles = NULL;
}


static int
power_of_two_greater_than (int v)
{
  int t = 1;
  while (t < v)
    t *= 2;

  return t;
}

static int
tile_dimension (int  to_fill,
		int  start_size,
		int *positions,
		int *sizes)
		
{
  int pos = 0;
  int n_tiles = 0;
  int size = start_size;

  while (TRUE)
    {
      if (positions)
	positions[n_tiles] = pos;
      if (sizes)
	sizes[n_tiles] = size;
      n_tiles++;
	
      if (to_fill <= size)
	break;
      else
	{
	  to_fill -= (size - OVERLAP);
	  pos += size - OVERLAP;
	  while (size >= 2 * to_fill || size - to_fill > MAX_WASTE)
	    size /= 2;
	}
    }

  return n_tiles;
}

static gboolean
can_create (int width,
	    int height)
{
  GLint new_width;
  
  glTexImage2D (GL_PROXY_TEXTURE_2D, 0, GL_RGBA,
		width, height, 0 /* border */,
		GL_BGRA, PIXEL_TYPE, NULL);

  glGetTexLevelParameteriv (GL_PROXY_TEXTURE_2D, 0,
			    GL_TEXTURE_WIDTH, &new_width);

  return new_width != 0;
}

static void
init_tiles (LmcTexture *texture)
{
  int x_pot = power_of_two_greater_than (texture->width);
  int y_pot = power_of_two_greater_than (texture->height);

  while (!(can_create (x_pot, y_pot) &&
	   (x_pot - texture->width < MAX_WASTE) &&
	   (y_pot - texture->height < MAX_WASTE)))
    {
      if (x_pot > y_pot)
	x_pot /= 2;
      else
	y_pot /= 2;
    }

  texture->n_x_tiles = tile_dimension (texture->width, x_pot, NULL, NULL);
  texture->tile_x_position = g_new (int, texture->n_x_tiles);
  texture->tile_x_size = g_new (int, texture->n_x_tiles);
  tile_dimension (texture->width, x_pot,
		  texture->tile_x_position, texture->tile_x_size);
  
  texture->n_y_tiles = tile_dimension (texture->height, y_pot, NULL, NULL);
  texture->tile_y_position = g_new (int, texture->n_y_tiles);
  texture->tile_y_size = g_new (int, texture->n_y_tiles);
  tile_dimension (texture->height, y_pot,
		  texture->tile_y_position, texture->tile_y_size);
  
  texture->tiles = g_new (GLuint, texture->n_x_tiles * texture->n_y_tiles);
  glGenTextures (texture->n_x_tiles * texture->n_y_tiles, texture->tiles);

#ifdef DEBUG
 {
   int i;

   g_print ("Tiled %d x %d texture as [", texture->width, texture->height);
   for (i = 0; i < texture->n_x_tiles; i++)
     {
       if (i != 0)
	 g_print (",");
       g_print ("%d(%d)", texture->tile_x_size[i], texture->tile_x_position[i]);
     }
   g_print ("]x[");
   for (i = 0; i < texture->n_y_tiles; i++)
     {
       if (i != 0)
	 g_print (",");
       g_print ("%d(%d)", texture->tile_y_size[i], texture->tile_y_position[i]);
     }
   g_print ("]\n");
 }
#endif /* DEBUG */
}

static void
quantize_rect (XRectangle *rect)
{
  int x1 = rect->x + rect->width;
  int y1 = rect->y + rect->height;

  rect->x = QUANTUM * (rect->x / QUANTUM);
  rect->y = QUANTUM * (rect->y / QUANTUM);

  x1 = QUANTUM * ((x1 + QUANTUM - 1) / QUANTUM);
  y1 = QUANTUM * ((y1 + QUANTUM - 1) / QUANTUM);

  rect->width = x1 - rect->x;
  rect->height = y1 - rect->y;
}

static guchar *
create_buffer (LmcBits    *bits,
	       XRectangle *rect)
{
  guchar *buffer = g_malloc (4 * rect->width * rect->height);
  int i, j;
  int bpp;

#ifdef DEBUG
  g_print ("Creating buffer from +%d+%dx%dx%d\n",
	   rect->x, rect->y, rect->width, rect->height);
#endif

  switch (bits->format)
    {
    case LMC_BITS_RGB_16:
      bpp = 2;
      break;
    case LMC_BITS_RGB_24:
      bpp = 3;
      break;
    case LMC_BITS_RGB_32:
    case LMC_BITS_RGBA_MSB_32:
    case LMC_BITS_ARGB_32:
      bpp = 4;
      break;
    default:
      g_assert_not_reached ();
      bpp = 4;
      break;
    }

  for (j = 0; j < rect->height; j++)
    {
      guchar *src;
      guint32 *dest;
      guint src_max;
      
      dest = (guint32 *)buffer + rect->width * j;

      if (j + rect->y >= bits->height)
	{
	  memset (dest, 0,  rect->width * 4);
	  continue;
	}

      src = bits->data_ + bits->rowstride * (j + rect->y) + bpp * rect->x;
      src_max = MIN (rect->width, bits->width - rect->x);

      for (i = 0; i < src_max; i++)
	{
	  guchar r,g,b,a;

	  switch (bits->format)
	    {
	    case LMC_BITS_RGB_16:
	      {
		guint16 t = *(guint16 *)src;
		guint tr, tg, tb;

		tr = t & 0xf800;
		r = (tr >> 8) + (tr >> 13);
		
		tg = t & 0x07e0;
		g = (tg >> 3) + (tg >> 9);
		
		tb = t & 0x001f;
		b = (tb << 3) + (tb >> 2);

		a = 0xff;
	      }
	      break;
	    case LMC_BITS_RGB_24:
	      r = src[0];
	      g = src[1];
	      b = src[2];
	      a = 0xff;
	      break;
	    case LMC_BITS_RGB_32:
	      {
		guint32 t = *(guint32 *)src;
		r = (t >> 16) & 0xff;
		g = (t >> 8) & 0xff;
		b = t & 0xff;
	        a = 0xff;
	      }
	      break;
	    case LMC_BITS_RGBA_MSB_32:
	      {
		guint tr, tg, tb;
		
		a = src[3];
		tr = src[0] * a + 0x80;
		r = (tr + (tr >> 8)) >> 8;
		tg = src[1] * a + 0x80;
		g = (tg + (tg >> 8)) >> 8;
		tb = src[2] * a + 0x80;
		b = (tb + (tb >> 8)) >> 8;
	      }
	      break;
	    case LMC_BITS_ARGB_32:
	      {
		guint32 t = *(guint32 *)src;
		r = (t >> 16) & 0xff;
		g = (t >> 8) & 0xff;
		b = t & 0xff;
		a = t >> 24;
	      }
	      break;
	    default:
	      g_assert_not_reached();
	      r = g = b = a = 0; /* Quiet GCC */
	      break;
	    }
	  
	  *dest = (a << 24) | (r << 16) | (g << 8) | b;

	  src += bpp;
	  dest++;
	}
      
      for (; i < rect->width; i++)
	{
	  *dest = 0;
	  dest++;
	}
    }

  return buffer;
}

static void
init_mipmap (GLuint      name,
	     XRectangle *tile_rect,
	     int         level,
	     XRectangle *rect,
	     guchar     *buffer)
{
  guchar *tmp_data = NULL;;

#ifdef DEBUG
  g_print ("Initing tile +%d+%dx%dx%d from rectangle +%d+%dx%dx%d\n",
	   tile_rect->x, tile_rect->y, tile_rect->width, tile_rect->height,
	   rect->x, rect->y, rect->width, rect->height);
#endif  

  if (rect->x != tile_rect->x || rect->width != tile_rect->width ||
      tile_rect->y < rect->y || tile_rect->y + tile_rect->height > rect->y + rect->height)
    {
      int j;
      int copy_width = MIN (tile_rect->width, rect->y + rect->width - tile_rect->x);
      int copy_height = MIN (tile_rect->height, rect->y + rect->height - tile_rect->y);

      tmp_data = g_malloc0 (tile_rect->width * tile_rect->height * 4);
      
      for (j = 0; j < copy_height; j++)
	{
	  guchar *src = buffer + (j + tile_rect->y - rect->y) * rect->width * 4 + (tile_rect->x - rect->x) * 4;
	  guchar *dest = tmp_data + j * tile_rect->width * 4;
	  
	  memcpy (dest, src, copy_width * 4);
	}

      buffer = tmp_data;
    }
  else
    {
      if (rect->y != tile_rect->y)
	buffer += (tile_rect->y - rect->y) * rect->width * 4;
    }
      
  glBindTexture (GL_TEXTURE_2D, name);

  if (level == 0)
    {
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

      glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
  
  glPixelStorei (GL_UNPACK_ROW_LENGTH, tile_rect->width);
  glPixelStorei (GL_UNPACK_ALIGNMENT, 4);

  glTexImage2D (GL_TEXTURE_2D, level, GL_RGBA,
		tile_rect->width, tile_rect->height, 0 /* border */,
		GL_BGRA, PIXEL_TYPE, buffer);
  dump_error ("glTexImage2D");

  if (tmp_data)
    g_free (tmp_data);
}

static void
update_mipmap (GLuint      name,
	       XRectangle *tile_rect,
	       int         level,
	       XRectangle *rect,
	       guchar     *buffer)
{
  XRectangle tmp_rect;
  int x1, y1;
  guchar *data;

#ifdef DEBUG
  g_print ("Updating tile +%d+%dx%dx%d from rectangle +%d+%dx%dx%d\n",
	   tile_rect->x, tile_rect->y, tile_rect->width, tile_rect->height,
	   rect->x, rect->y, rect->width, rect->height);
#endif  

  glBindTexture (GL_TEXTURE_2D, name);

  glPixelStorei (GL_UNPACK_ROW_LENGTH, rect->width);
  glPixelStorei (GL_UNPACK_ALIGNMENT, 4);

  y1 = rect->y + rect->height;

  tmp_rect.x = MAX (rect->x, tile_rect->x);
  tmp_rect.y = MAX (rect->y, tile_rect->y);
  x1 = MIN (rect->x + rect->width, tile_rect->x + tile_rect->width);
  y1 = MIN (rect->y + rect->height, tile_rect->y + tile_rect->height);
  if (x1 <= tmp_rect.x || y1 <= tmp_rect.y)
    return;
  tmp_rect.width = x1 - tmp_rect.x;
  tmp_rect.height = y1 - tmp_rect.y;

  data = buffer + 4 * rect->width * (tmp_rect.y - rect->y) + 4 * (tmp_rect.x - rect->x);

  glTexSubImage2D (GL_TEXTURE_2D, level,
		   tmp_rect.x - tile_rect->x, tmp_rect.y - tile_rect->y,
		   tmp_rect.width, tmp_rect.height,
		   GL_BGRA, PIXEL_TYPE,
		   data);
}
	       
static void
update_tile_mipmaps (LmcTexture *texture,
		     int         level,
		     XRectangle *rect,
		     guchar     *buffer,
		     gboolean    init)
{
  int i, j;
  
  for (j = 0; j < texture->n_y_tiles; j++)
    for (i = 0; i < texture->n_x_tiles; i++)
      {
	XRectangle tile_rect;
	
	tile_rect.x = texture->tile_x_position[i] >> level;
	tile_rect.y = texture->tile_y_position[j] >> level;
	tile_rect.width = MAX (texture->tile_x_size[i] >> level, 1);
	tile_rect.height = MAX (texture->tile_y_size[j] >> level, 1);
	
	if (init)
	  init_mipmap (TILE_NAME(texture, i, j), &tile_rect,
		       level, rect, buffer);
	else
	  update_mipmap (TILE_NAME(texture, i, j), &tile_rect,
			 level, rect, buffer);
      }
}

static void
update_base_mipmap (LmcTexture *texture,
		    int         level,
		    XRectangle *rect,
		    guchar     *buffer,
		    gboolean    init)
{
#if 0
  XRectangle tile_rect;

  g_assert (level >= BASE_LEVEL);
  
  if (texture->base_texture == 0)
    glGenTextures (1, &texture->base_texture);
  
  tile_rect.x = 0;
  tile_rect.y = 0;
  tile_rect.width = MAX (power_of_two_greater_than (texture->width) >> level, 1);
  tile_rect.height = MAX (power_of_two_greater_than (texture->height) >> level, 1);

  if (init)
    init_mipmap (texture->base_texture, &tile_rect,
		 level - BASE_LEVEL, rect, buffer);
  else
    update_mipmap (texture->base_texture, &tile_rect,
		   level - BASE_LEVEL, rect, buffer);
#endif  
}

static void
scale_by (XRectangle *rect,
	  guchar     *buffer,
	  int         x_scale,
	  int         y_scale)
{
  int dest_height = rect->height / y_scale;
  int dest_width = rect->width / x_scale;
  int i, j;

  for (j = 0; j < dest_height; j++)
    {
      guint32 *dest = (guint32 *)buffer + j * dest_width;
      guint32 *src1, *src2;

      src1 = (guint32 *)buffer + (j * y_scale) * rect->width;
      if (y_scale == 1)
	src2 = src1;
      else
	src2 = src1 + rect->width;

      for (i = 0; i < dest_width; i++)
	{
	  guint32 r, g, b, a;
	  guint32 t;
	  
	  t = src1[0];
	  
	  a = t >> 24;
	  r = (t >> 16) & 0xff;
	  g = (t >> 8) & 0xff;
	  b = t & 0xff;
	  
	  t = src2[0];
	  
	  a += t >> 24;
	  r += (t >> 16) & 0xff;
	  g += (t >> 8) & 0xff;
	  b += t & 0xff;
	      
	  if (x_scale == 2)
	    {
	      t = src1[1];
	      
	      a += t >> 24;
	      r += (t >> 16) & 0xff;
	      g += (t >> 8) & 0xff;
	      b += t & 0xff;

	      t = src2[1];
	      
	      a += t >> 24;
	      r += (t >> 16) & 0xff;
	      g += (t >> 8) & 0xff;
	      b += t & 0xff;

	      a /= 4;
	      r /= 4;
	      g /= 4;
	      b /= 4;
	    }
	  else
	    {
	      a /= 2;
	      r /= 2;
	      g /= 2;
	      b /= 2;
	    }

	  *dest = (a << 24) | (r << 16) | (g << 8) | b;
	  
	  src1 += x_scale;
	  src2 += x_scale;
	  dest++;
	}
    }

  rect->x /= x_scale;
  rect->y /= y_scale;
  rect->width /= x_scale;
  rect->height /= y_scale;
}

static void
update_quantum_bits (LmcTexture  *texture,
		     XRectangle  *rect,
		     guchar      *buffer)
{
  int j;

#ifdef DEBUG  
  g_print ("%s quantum bits at size %d %d\n",
	   texture->quantum_bits ? "Initing" : "Updating",
	   rect->width, rect->height);
#endif
  
  if (!texture->quantum_bits)
    {
      guchar *tmp_bits;
      
      g_assert (rect->x == 0 && rect->y == 0);

      tmp_bits = g_malloc (rect->width * rect->height * 4);
      texture->quantum_bits = lmc_bits_new (LMC_BITS_ARGB_32,
					    rect->width, rect->height,
					    tmp_bits, rect->width * 4,
					    (GDestroyNotify)g_free, tmp_bits);
    }

  for (j = 0; j < rect->height; j++)
    {
      guchar *src = buffer + j * rect->width * 4;
      guchar *dest = texture->quantum_bits->data_ + (j + rect->y) * rect->width * 4 + rect->x * 4;

      memcpy (dest, src, rect->width * 4);
    }
}

void
lmc_texture_update_rect (LmcTexture *texture,
			 XRectangle *rect)
{
  XRectangle tmp_rect;
  int level;
  int x_pot, y_pot;
  gboolean init = FALSE;
  guchar *buffer;

  if (!texture->tiles)
    {
      init_tiles (texture);
      
      init = TRUE;
      
      tmp_rect.x = 0;
      tmp_rect.y = 0;
	  
      if (texture->n_x_tiles == 1)
	{
	  tmp_rect.width = texture->tile_x_size[0];
	  tmp_rect.height = texture->height;
	}
      else
	{
	  tmp_rect.width = texture->width;
	  tmp_rect.height = texture->height;
	}
    }
  else
    {
      tmp_rect = *rect;
    }

  rect = &tmp_rect;

  quantize_rect (rect);
  buffer = create_buffer (texture->bits, rect);

  x_pot = power_of_two_greater_than (texture->width);
  y_pot = power_of_two_greater_than (texture->height);

  level = 0;
  while (TRUE)
    {
      int x_scale, y_scale;

      update_tile_mipmaps (texture, level, rect, buffer, init);
      if (level >= BASE_LEVEL)
	update_base_mipmap (texture, level, rect, buffer, init);

      if (level == QUANTUM_LEVEL)
	{
	  update_quantum_bits (texture, rect, buffer);
	  g_free (buffer);
	  
	  rect->x = 0;
	  rect->y = 0;
	  rect->width = x_pot;
	  rect->height = y_pot;
	  
	  buffer = create_buffer (texture->quantum_bits, rect);
	}
      
      x_scale = MIN (x_pot, 2);
      y_scale = MIN (y_pot, 2);

      if (x_scale > 1 || y_scale > 1)
	{
	  scale_by (rect, buffer, x_scale, y_scale);
	  
	  x_pot /= x_scale;
	  y_pot /= y_scale;

	  level++;
	}
      else
	break;
    }

  g_free (buffer);
}

#undef WIREFRAME

static void
setup_alpha_state (LmcTexture *texture,
		     double      alpha)
{
#ifdef WIREFRAME
  glDisable (GL_TEXTURE_2D);
  glPolygonMode (GL_FRONT, GL_LINE);
#else
  glEnable (GL_TEXTURE_2D);
#endif

  if (alpha == 1.0)
    {
      glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

      glEnable (GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if 0
      if (lmc_bits_has_alpha (texture->bits))
	{
	  glEnable (GL_BLEND);
	  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
      else
	glDisable (GL_BLEND);
#endif      
    }
  else
    {
      glColor4f (1.0, 1.0, 1.0, alpha);

      glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
      
#if 0      
      if (lmc_bits_has_alpha (texture->bits))
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
      else
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
#endif      
      
      glEnable (GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

}

static void
texture_emit_quad (LmcTexture *texture,
		   int         tile,
		   GLuint     *current_texture,
		   double      texture_x0,
		   double      texture_x1,
		   double      texture_y0,
		   double      texture_y1,
		   double      window_x0,
		   double      window_x1,
		   double      window_x2,
		   double      window_x3,
		   double      window_y0,
		   double      window_y1,
		   double      window_y2,
		   double      window_y3)
{
  int y_tile = tile / texture->n_x_tiles;
  int x_tile = tile % texture->n_x_tiles;
  double tile_x0, tile_x1, tile_y0, tile_y1;
  double clamp_x0, clamp_x1, clamp_y0, clamp_y1;

  clamp_x0 = tile_x0 = texture->tile_x_position[x_tile];
  clamp_x1 = tile_x1 = tile_x0 + texture->tile_x_size[x_tile];
  if (x_tile != 0)
    clamp_x0 += OVERLAP / 2;
  if (x_tile != texture->n_x_tiles - 1)
    clamp_x1 -= OVERLAP / 2;
  
  clamp_y0 = tile_y0 = texture->tile_y_position[y_tile];
  clamp_y1 = tile_y1 = tile_y0 + texture->tile_y_size[y_tile];
  if (y_tile != 0)
    clamp_y0 += OVERLAP / 2;
  if (y_tile != texture->n_y_tiles - 1)
    clamp_y1 -= OVERLAP / 2;
  
  if (texture_x0 < clamp_x1 && texture_x1 > clamp_x0 &&
      texture_y0 < clamp_y1 && texture_y1 > clamp_y0)
    {
      double normalized_x0, normalized_x1, normalized_y0, normalized_y1;
      double clamped_x0, clamped_x1, clamped_y0, clamped_y1;

      clamped_x0 = CLAMP (texture_x0, clamp_x0, clamp_x1);
      clamped_x1 = CLAMP (texture_x1, clamp_x0, clamp_x1);
      clamped_y0 = CLAMP (texture_y0, clamp_y0, clamp_y1);
      clamped_y1 = CLAMP (texture_y1, clamp_y0, clamp_y1);

      normalized_x0 = (TEXTURE_COORD_OFFSET + clamped_x0 - tile_x0) / (tile_x1 - tile_x0);
      normalized_x1 = (TEXTURE_COORD_OFFSET + clamped_x1 - tile_x0) / (tile_x1 - tile_x0);
      normalized_y0 = (TEXTURE_COORD_OFFSET + clamped_y0 - tile_y0) / (tile_y1 - tile_y0);
      normalized_y1 = (TEXTURE_COORD_OFFSET + clamped_y1 - tile_y0) / (tile_y1 - tile_y0);

#if 1
      if (texture_x1 != texture_x0)
	{
	  double left_fraction = (clamped_x0 - texture_x0) / (texture_x1 - texture_x0);
	  double right_fraction = (clamped_x1 - texture_x1) / (texture_x1 - texture_x0);

	  window_x0 += left_fraction * (window_x3 - window_x0);
	  window_x3 += right_fraction * (window_x3 - window_x0);
	  window_x1 += left_fraction * (window_x2 - window_x1);
	  window_x2 += right_fraction * (window_x2 - window_x1);

	  window_y0 += left_fraction * (window_y3 - window_y0);
	  window_y3 += right_fraction * (window_y3 - window_y0);
	  window_y1 += left_fraction * (window_y2 - window_y1);
	  window_y2 += right_fraction * (window_y2 - window_y1);
	}
      
      if (texture_y1 != texture_y0)
	{
	  double top_fraction = (clamped_y0 - texture_y0) / (texture_y1 - texture_y0);
	  double bottom_fraction = (clamped_y1 - texture_y1) / (texture_y1 - texture_y0);

	  window_x0 += top_fraction * (window_x1 - window_x0);
	  window_x1 += bottom_fraction * (window_x1 - window_x0);
	  window_x3 += top_fraction * (window_x2 - window_x3);
	  window_x2 += bottom_fraction * (window_x2 - window_x3);

	  window_y0 += top_fraction * (window_y1 - window_y0);
	  window_y1 += bottom_fraction * (window_y1 - window_y0);
	  window_y3 += top_fraction * (window_y2 - window_y3);
	  window_y2 += bottom_fraction * (window_y2 - window_y3);
	}
#endif

      if (*current_texture != texture->tiles[tile])
	{
	  if (*current_texture)
	    glEnd ();

	  *current_texture = texture->tiles[tile];

#ifdef WIREFRAME
	  glColor3f (1, 1, 0);
#else
	  glBindTexture (GL_TEXTURE_2D, *current_texture);
#endif
	  
	  glBegin (GL_QUADS);
	}

#ifdef DEBUG
      g_print ("Drawing +%g+%gx%gx%g from +%g+%gx%gx%g in [%d,%d] (%d)\n",
	       window_x0, window_y0, window_x1 - window_x0, window_y1 - window_y0,
	       normalized_x0, normalized_y0, normalized_x1 - normalized_x0, normalized_y1 - normalized_y0,
	       x_tile, y_tile, tile);
#endif      

      glTexCoord2f (normalized_x0, normalized_y0);  glVertex3i (window_x0, window_y0, 0);
      glTexCoord2f (normalized_x0, normalized_y1);  glVertex3i (window_x1, window_y1, 0);
      glTexCoord2f (normalized_x1, normalized_y1);  glVertex3i (window_x2, window_y2, 0);
      glTexCoord2f (normalized_x1, normalized_y0);  glVertex3i (window_x3, window_y3, 0);
    }
}

static void
texture_emit_end (LmcTexture *texture,
		  GLuint     *current_texture)
{
  if (*current_texture != 0)
    glEnd ();
}


static void
split_tile (LmcTexture *texture,
	    int         tile,
	    GLuint     *current_texture,
	    double      texture_x0,
	    double      texture_x1,
	    double      texture_y0,
	    double      texture_y1,
	    double      window_x0,
	    double      window_x1,
	    double      window_y0,
	    double      window_y1)
{
  int y_tile_index = tile / texture->n_x_tiles;
  int x_tile_index = tile % texture->n_x_tiles;
  int grid_x, grid_y;
  double tile_x, tile_width, tile_y, tile_height;
  double x, y;
  LmcPoint *g;
  
  tile_x	= texture->tile_x_position[x_tile_index];
  tile_width	= texture->tile_x_size[x_tile_index];
  tile_y	= texture->tile_y_position[y_tile_index];
  tile_height	= texture->tile_y_size[y_tile_index];

  for (y = 0; y < tile_height - WOBBLE_TILE_SIZE / 2; y += WOBBLE_TILE_SIZE)
    for (x = 0; x < tile_width - WOBBLE_TILE_SIZE / 2; x += WOBBLE_TILE_SIZE)
      {
	grid_x = (tile_x + x) / WOBBLE_TILE_SIZE;
	grid_y = (tile_y + y) / WOBBLE_TILE_SIZE;
	g = &texture->grid[grid_x + grid_y * texture->grid_width];
  
	if (grid_x >= texture->grid_width - 1)
	    continue;
	if (grid_y >= texture->grid_height - 1)
	    continue;

	texture_emit_quad (texture, tile, current_texture,
			   tile_x + x, tile_x + x + WOBBLE_TILE_SIZE,
			   tile_y + y, tile_y + y + WOBBLE_TILE_SIZE,
			   g[0].x,
			   g[texture->grid_width].x,
			   g[texture->grid_width + 1].x,
			   g[1].x,
			   g[0].y,
			   g[texture->grid_width].y,
			   g[texture->grid_width + 1].y,
			   g[1].y);
      }
}

void
lmc_texture_draw (LmcTexture *texture,
		  double      alpha,
		  int         x,
		  int         y)
{
  int tile;
  GLuint current_texture = 0;
  
  setup_alpha_state (texture, alpha);

#ifdef DEBUG
  g_print ("Texture +%d+%dx%dx%d\n", x, y, texture->width, texture->height);
#endif  

  if (texture->grid != NULL)
    {
      g_mutex_lock (texture->grid_mutex);

      for (tile = 0; tile < texture->n_x_tiles * texture->n_y_tiles; tile++)
	split_tile (texture, tile, &current_texture,
		    0, texture->width,     0, texture->height,
		    x, x + texture->width, y, y + texture->height);

      g_mutex_unlock (texture->grid_mutex);
    }
  else
    {
      for (tile = 0; tile < texture->n_x_tiles * texture->n_y_tiles; tile++)
	texture_emit_quad (texture, tile, &current_texture,
			   0, texture->width,     0, texture->height,
			   x, x, x + texture->width, x + texture->width,
			   y, y + texture->height, y + texture->height, y);
    }
  
  texture_emit_end (texture, &current_texture);

  dump_error ("texture_draw");
 }

void
lmc_texture_draw_border (LmcTexture    *texture,
			 double         alpha,
			 XRectangle    *base_rect,
			 LmcBorderInfo *border_info)
{
  GLint window_x[6];
  GLint window_y[6];
  GLfloat texture_x[6];
  GLfloat texture_y[6];
  int tile;
  GLuint current_texture = 0;
  int i,j;

  /*
  0  1    2   3   4   5
0 +-------+---+-------+
  |       |   |       |
1 |  +----+---+---+   |
  |  |            |   |
2 +--+            +---+
  |  |            |   |
  |  |            |   |
3 +--+            +---+
  |  |            |   |
4 |  +----+---+---+   |
  |       |   |       |
5 +--+----+---+---+---+
  */
  
  window_x[0] = base_rect->x - border_info->left;
  window_x[1] = base_rect->x;
  window_x[2] = base_rect->x - border_info->left + border_info->left_unscaled;
  window_x[3] = base_rect->x + base_rect->width - border_info->right_unscaled + border_info->right;
  window_x[4] = base_rect->x + base_rect->width;
  window_x[5] = base_rect->x + base_rect->width + border_info->right;

  window_y[0] = base_rect->y - border_info->top;
  window_y[1] = base_rect->y;
  window_y[2] = base_rect->y - border_info->top + border_info->top_unscaled;
  window_y[3] = base_rect->y + base_rect->height - border_info->bottom_unscaled + border_info->bottom;
  window_y[4] = base_rect->y + base_rect->height;
  window_y[5] = base_rect->y + base_rect->height + border_info->bottom;
  
  texture_x[0] = 0;
  texture_x[1] = border_info->left;
  texture_x[2] = border_info->left_unscaled;
  texture_x[3] = texture->width - border_info->right_unscaled;
  texture_x[4] = texture->width - border_info->right;
  texture_x[5] = texture->width;

  texture_y[0] = 0;
  texture_y[1] = border_info->top;
  texture_y[2] = border_info->top_unscaled;
  texture_y[3] = texture->height - border_info->bottom_unscaled;
  texture_y[4] = texture->height - border_info->bottom;
  texture_y[5] = texture->height;

  setup_alpha_state (texture, alpha);

  /* We could reduce the number of vertices a bit using GL_QUAD_STRIP */

  for (tile = 0; tile < texture->n_x_tiles * texture->n_y_tiles; tile++)
    for (i = 0; i < 5; i++)
      for (j = 0; j < 5; j++)
	if (i == 0 || i == 4 || j == 0 || j == 4)
 	  {
	    texture_emit_quad (texture, tile, &current_texture,
			       texture_x[j], texture_x[j + 1], texture_y[i], texture_y[i + 1],
			       window_x[j],  window_x[j], window_x[j + 1],  window_x[j + 1],
			       window_y[i],  window_y[i + 1],  window_y[i + 1],  window_y[i]);
	  }

  texture_emit_end (texture, &current_texture);

  dump_error ("texture_draw_border");
}

