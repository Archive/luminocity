#include <X11/Xlib.h>
#include <X11/extensions/Xcomposite.h>

#include "async.h"
#include "lmc-marshal.h"
#include "root.h"
#include "toplevel.h"

typedef struct _LmcRootClass  LmcRootClass;

struct _LmcRoot
{
  LmcWindow parent_instance;

  int screen;
  int width, height;

  LmcBits *cursor_bits;
  int cursor_hot_x;
  int cursor_hot_y;
  
  GList *children;

  gboolean need_focus;
  LmcToplevel *focus_toplevel;
  LmcToplevel *grab_focus_toplevel;
};

struct _LmcRootClass
{
  LmcWindowClass parent_class;
};

static guint toplevel_added_signal;
static guint toplevel_restacked_signal;
static guint cursor_changed_signal;
static guint focus_changed_signal;
     
/**********************************************************************/

G_DEFINE_TYPE (LmcRoot, lmc_root, LMC_TYPE_WINDOW)

static void
lmc_root_finalize (GObject *object)
{
  LmcRoot *root = LMC_ROOT (object);
  
  lmc_bits_unref (root->cursor_bits);
  
  G_OBJECT_CLASS (lmc_root_parent_class)->finalize (object);
}

static void
lmc_root_class_init (LmcRootClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  object_class->finalize = lmc_root_finalize;

  toplevel_added_signal =  g_signal_new ("toplevel-added",
					 G_OBJECT_CLASS_TYPE (object_class),
					 G_SIGNAL_RUN_LAST,
					 0,
					 NULL, NULL,
					 lmc_marshal_NONE__OBJECT,
					 G_TYPE_NONE, 1, LMC_TYPE_TOPLEVEL);
  toplevel_restacked_signal =  g_signal_new ("toplevel-restacked",
					     G_OBJECT_CLASS_TYPE (object_class),
					     G_SIGNAL_RUN_LAST,
					     0,
					     NULL, NULL,
					     lmc_marshal_NONE__OBJECT_OBJECT,
					     G_TYPE_NONE, 2, LMC_TYPE_TOPLEVEL, LMC_TYPE_TOPLEVEL);
  cursor_changed_signal =  g_signal_new ("cursor-changed",
					 G_OBJECT_CLASS_TYPE (object_class),
					 G_SIGNAL_RUN_LAST,
					 0,
					 NULL, NULL,
					 lmc_marshal_NONE__NONE,
					 G_TYPE_NONE, 0);
  focus_changed_signal =  g_signal_new ("focus-changed",
					G_OBJECT_CLASS_TYPE (object_class),
					G_SIGNAL_RUN_LAST,
					0,
					NULL, NULL,
					lmc_marshal_NONE__NONE,
					G_TYPE_NONE, 0);
}

static void
lmc_root_init (LmcRoot *root)
{
}

static gboolean
on_configure_request (LmcToplevel            *toplevel,
		      XConfigureRequestEvent *xev,
		      LmcRoot                *root)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  
  XWindowChanges values;
  int x, y, width, height;
  int new_x, new_y, new_width, new_height;
  gboolean real_configure = FALSE;
  gulong stacking_mask;

  /* First handle the size and position
   */
  lmc_toplevel_get_set_position (toplevel, &x, &y);
  new_x = x;  new_y = y;
  lmc_toplevel_get_set_size (toplevel, &width, &height);
  new_width = width;  new_height = height;

  if (xev->value_mask & CWX)
    new_x = xev->x;
  if (xev->value_mask & CWY)
    new_y = xev->y;
  if (xev->value_mask & CWWidth)
    new_width = xev->width;
  if (xev->value_mask & CWHeight)
    new_height = xev->height;

  lmc_toplevel_constrain_size (toplevel, new_width, new_height,
			       &new_width, &new_height);

  if (new_x != x || new_y != y ||
      new_width != width || new_height != height)
    {
      lmc_toplevel_move_resize (toplevel, new_x, new_y,
				new_width, new_height);
      real_configure = TRUE;
    }
  
  /* Now handle stacking (border width we ignore, always)
   */
  stacking_mask = xev->value_mask & (CWSibling | CWStackMode);
  if (stacking_mask != 0)
    {
      Display *xdisplay = LMC_WINDOW_XDISPLAY (lwindow);
      Window xwindow = LMC_WINDOW_XWINDOW (lwindow);
      LmcAsyncErrorTrap *trap = lmc_async_error_trap_set (xdisplay);

      values.sibling = xev->above;
      values.stack_mode = xev->detail;

      XConfigureWindow (xdisplay, xwindow, stacking_mask, &values);

      lmc_async_error_trap_unset (trap);
      
      real_configure = TRUE;
    }

  if (xev->value_mask != 0 && !real_configure)
    lmc_toplevel_send_configure_notify (toplevel);

  return TRUE;
}

static gboolean
on_circulate_request (LmcToplevel            *toplevel,
		      XCirculateRequestEvent *xev,
		      LmcRoot                *root)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  
  if (xev->place == PlaceOnTop)
    XRaiseWindow (lwindow->display->xdisplay, lwindow->xwindow);
  else
    XLowerWindow (lwindow->display->xdisplay, lwindow->xwindow);
  
  return TRUE;
}

static gboolean
on_resize_request (LmcToplevel         *toplevel,
		   XResizeRequestEvent *xev,
		   LmcRoot             *root)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);

  XResizeWindow (lwindow->display->xdisplay, lwindow->xwindow,
		 xev->width, xev->height);
  
  return TRUE;
}

static gboolean
on_configure_notify (LmcToplevel     *toplevel,
		     XConfigureEvent *xev,
		     LmcRoot         *root)
{
  LmcWindow *lwindow = LMC_WINDOW (toplevel);
  GList *l;

  if (xev->send_event)
    return FALSE;

  l = g_list_find (root->children, toplevel);
  if (l)
    {
      LmcWindow *above = NULL;
      gboolean stacking_changed = FALSE;
      
      if (xev->above == None)
	{
	  if (l->next)
	    {
	      root->children = g_list_delete_link (root->children, l);
	      root->children = g_list_append (root->children, toplevel);

	      stacking_changed = TRUE;
	    }
	  
	}
      else
	{
	  GList *l_above;

	  above = lmc_display_lookup (lwindow->display, xev->above);
	  l_above = g_list_find (root->children, above);

	  if (l_above && l->next != l_above)
	    {
	      root->children = g_list_delete_link (root->children, l);
	      root->children = g_list_insert_before (root->children, l_above, toplevel);

	      stacking_changed = TRUE;
	    }
	}

      if (stacking_changed)
	g_signal_emit (root, toplevel_restacked_signal, 0, toplevel, above);
    }

  return FALSE;
}

static gboolean
on_circulate_notify (LmcToplevel     *toplevel,
		     XCirculateEvent *xev,
		     LmcRoot         *root)
{
  GList *l = g_list_find (root->children, toplevel);

  if (l)
    {
      LmcWindow *above = NULL;
      gboolean stacking_changed = FALSE;
      
      if (xev->place == PlaceOnTop)
	{
	  if (l->prev)
	    {
	      root->children = g_list_delete_link (root->children, l);
	      root->children = g_list_prepend (root->children, toplevel);
	      above = root->children->next->data;
	      stacking_changed = TRUE;
	    }
	}
      else
	{
	  if (l->next)
	    {
	      root->children = g_list_delete_link (root->children, l);
	      root->children = g_list_append (root->children, toplevel);
	      above = NULL;
	      stacking_changed = TRUE;
	    }
	}
      
      if (stacking_changed)
	g_signal_emit (root, toplevel_restacked_signal, 0, toplevel, above);
    }
  
  return FALSE;
}

static void
on_map_request (LmcToplevel      *toplevel,
		LmcRoot          *root)
{
  lmc_toplevel_map (toplevel);

  if (root->need_focus)
    lmc_toplevel_grab_focus (toplevel);
}

static void
root_set_focus_toplevel (LmcRoot     *root,
			 LmcToplevel *toplevel)
{
  root->focus_toplevel = toplevel;
  if (toplevel)
    root->need_focus = FALSE;
  
  g_signal_emit (root, focus_changed_signal, 0);
}

static void
root_set_grab_focus_toplevel (LmcRoot     *root,
			      LmcToplevel *toplevel)
{
  root->grab_focus_toplevel = toplevel;
  g_signal_emit (root, focus_changed_signal, 0);
}

static gboolean
on_focus_in (LmcToplevel       *toplevel,
	     XFocusChangeEvent *xev,
	     LmcRoot           *root)
{
  switch (xev->mode)
    {
    case NotifyNormal:
    case NotifyWhileGrabbed:
      switch (xev->detail)
	{
	case NotifyAncestor:
	case NotifyVirtual:
	case NotifyNonlinear:
	case NotifyNonlinearVirtual:
	  root_set_focus_toplevel (root, toplevel);
	  break;
	case NotifyPointer:
	case NotifyInferior:
	case NotifyPointerRoot:
	case NotifyDetailNone:
	  break;
	}
      break;
    case NotifyGrab:
      switch (xev->detail)
	{
	case NotifyAncestor:
	case NotifyVirtual:
	case NotifyNonlinear:
	case NotifyNonlinearVirtual:
	  root_set_grab_focus_toplevel (root, toplevel);
	  break;
	case NotifyPointer:
	case NotifyInferior:
	case NotifyPointerRoot:
	case NotifyDetailNone:
	  break;
	}
      break;
    case NotifyUngrab:
       break;
    }
  
  return FALSE;
}

static gboolean
on_focus_out (LmcToplevel       *toplevel,
	      XFocusChangeEvent *xev,
	      LmcRoot           *root)
{
  switch (xev->mode)
    {
    case NotifyNormal:
    case NotifyWhileGrabbed:
      switch (xev->detail)
	{
	case NotifyAncestor:
	case NotifyVirtual:
	case NotifyNonlinear:
	case NotifyNonlinearVirtual:
	  if (root->focus_toplevel == toplevel)
	    root_set_focus_toplevel (root, NULL);
	  break;
	case NotifyPointer:
	case NotifyInferior:
	case NotifyPointerRoot:
	case NotifyDetailNone:
	  break;
	}
      break;
    case NotifyGrab:
      break;
    case NotifyUngrab:
      switch (xev->detail)
	{
	case NotifyAncestor:
	case NotifyVirtual:
	case NotifyNonlinear:
	case NotifyNonlinearVirtual:
	  if (root->grab_focus_toplevel == toplevel)
	    root_set_grab_focus_toplevel (root, NULL);
	  break;
	case NotifyPointer:
	case NotifyInferior:
	case NotifyPointerRoot:
	case NotifyDetailNone:
	  break;
	}
      break;
       break;
    }
  
  return FALSE;
}

static gboolean
on_destroy_notify (LmcToplevel         *toplevel,
		   XDestroyWindowEvent *xev,
		   LmcRoot             *root)
{
  root->children = g_list_remove (root->children, toplevel);

  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_map_request, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_configure_request, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_circulate_request, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_resize_request, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_configure_notify, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_circulate_notify, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_focus_in, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_focus_out, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_destroy_notify, root);
  g_signal_handlers_disconnect_by_func (toplevel, (gpointer)on_map_request, root);

  return FALSE;
}

static void
add_new_window (LmcRoot              *root,
		Window                xwindow)
{
  LmcWindow *lwindow = LMC_WINDOW (root);

  LmcToplevel *toplevel = lmc_toplevel_new (lwindow->display, xwindow);

  if (!toplevel)		/* Might have been InputOnly */
    return;

  lmc_window_select_input (LMC_WINDOW (toplevel), FocusChangeMask);
  
  g_signal_connect (toplevel, "event::ConfigureRequest", G_CALLBACK (on_configure_request), root);
  g_signal_connect (toplevel, "event::CirculateRequest", G_CALLBACK (on_circulate_request), root);
  g_signal_connect (toplevel, "event::ResizeRequest",    G_CALLBACK (on_resize_request), root);
  g_signal_connect (toplevel, "event::ConfigureNotify",  G_CALLBACK (on_configure_notify), root);
  g_signal_connect (toplevel, "event::CirculateNotify",  G_CALLBACK (on_circulate_notify), root);
  g_signal_connect (toplevel, "event::FocusIn",          G_CALLBACK (on_focus_in), root);
  g_signal_connect (toplevel, "event::FocusOut",         G_CALLBACK (on_focus_out), root);
  g_signal_connect (toplevel, "event::DestroyNotify",    G_CALLBACK (on_destroy_notify), root);
  g_signal_connect (toplevel, "map-request",             G_CALLBACK (on_map_request), root);

  root->children = g_list_prepend (root->children, toplevel);

  g_signal_emit (root, toplevel_added_signal, 0, toplevel);
}

static gboolean
on_create_notify (LmcRoot             *root,
		  XCreateWindowEvent  *xev)
{
  add_new_window (root, xev->window);
  return TRUE;
}

static void
root_focus_top_window (LmcRoot *root)
{
  GList *l;
  for (l = root->children; l; l = l->next)
    {
      LmcToplevel *toplevel = l->data;
      if (lmc_toplevel_is_mapped (toplevel) &&
	  !lmc_toplevel_is_override_redirect (toplevel) &&
	  lmc_toplevel_grab_focus (toplevel))
	{
	  root->need_focus = FALSE;
	  break;
	}
    }

  /* If we didn't find a window to focus, we'll suffer for now,
   * and set the focus when the first window is mapped
   */
}

static gboolean
on_root_focus_in (LmcRoot           *root,
		  XFocusChangeEvent *xev)
{
  if ((xev->mode == NotifyNormal || xev->mode == NotifyWhileGrabbed) &&
      (xev->detail == NotifyPointerRoot || xev->detail == NotifyDetailNone || xev->detail == NotifyInferior))
    {
      /* Focus vanished, probably because the focus window was unmapped,
       * find a window to focus
       */
      root->need_focus = TRUE;
      root_focus_top_window (root);
    }
  
  return FALSE;
}
 
/* Takes ownership or frees image */
static LmcBits *
bits_from_cursor_image (XFixesCursorImage *image)
{
  LmcBits *result;
  
  if (sizeof (unsigned long) == sizeof (guint32))
    {
      result = lmc_bits_new (LMC_BITS_ARGB_32,
			     image->width, image->height,
			     (guchar *)image->pixels, image->width * 4,
			     (GDestroyNotify) XFree, image);
    }
  else
    {
      guint32 *pixels = g_new (guint32, image->width * image->height);
      guint i;

      for (i = 0; i < image->width * image->height; i++)
	pixels[i] = image->pixels[i];

      result = lmc_bits_new (LMC_BITS_ARGB_32,
			     image->width, image->height,
			     (guchar *)pixels, image->width * 4,
			     (GDestroyNotify) g_free, pixels);

      XFree (image);
    }

  return result;
}

static void
root_update_cursor (LmcRoot *root)
{
  Display *xdisplay = LMC_WINDOW_XDISPLAY (root);
  XFixesCursorImage *image;

  image = XFixesGetCursorImage (xdisplay);

  if (root->cursor_bits)
    lmc_bits_unref (root->cursor_bits);

  root->cursor_bits = bits_from_cursor_image (image);
  root->cursor_hot_x = image->xhot;
  root->cursor_hot_y = image->yhot;

  g_signal_emit (root, cursor_changed_signal, 0);
}

static gboolean
on_fixes_cursor_notify (LmcRoot                 *root,
			XFixesCursorNotifyEvent *xev)
{
  root_update_cursor (root);

  return FALSE;
}

#define REQUIRED_EXTENSIONS (LMC_EXTENSION_FIXES |	\
			     LMC_EXTENSION_DAMAGE |	\
			     LMC_EXTENSION_COMPOSITE)

LmcRoot *
lmc_root_new (LmcDisplay *ldisplay,
	      int         screen,
	      gboolean    update_root)
{
  Display *xdisplay = ldisplay->xdisplay;
  Window xwindow = RootWindow (xdisplay, screen);
  Window root_window, parent;
  Window *children;
  guint n_children;
  guint i;
  LmcRoot *root;

  if ((lmc_display_get_extensions (ldisplay) & REQUIRED_EXTENSIONS) != REQUIRED_EXTENSIONS)
    {
      g_printerr ("Fixes, Damage, and Composite extensions are required\n");
      return NULL;
    }

  root = g_object_new (LMC_TYPE_ROOT,
		       "display", ldisplay,
		       "xid",     xwindow,
		       NULL);

  root->screen = screen;

  g_signal_connect (root, "event::CreateNotify", G_CALLBACK (on_create_notify), NULL);
  g_signal_connect (root, "event::FocusIn", G_CALLBACK (on_root_focus_in), NULL);

  root->width = WidthOfScreen (ScreenOfDisplay (xdisplay, screen));
  root->height = HeightOfScreen (ScreenOfDisplay (xdisplay, screen));

  XCompositeRedirectSubwindows (xdisplay, xwindow,
				update_root ? CompositeRedirectAutomatic : CompositeRedirectManual);

  lmc_window_select_input (LMC_WINDOW (root), SubstructureRedirectMask | SubstructureNotifyMask | FocusChangeMask);
  XFixesSelectCursorInput (xdisplay, xwindow, XFixesDisplayCursorNotifyMask);

  g_signal_connect (root, "event::FixesCursorNotify", G_CALLBACK (on_fixes_cursor_notify), NULL);

  XQueryTree (xdisplay, xwindow, &root_window, &parent, &children, &n_children);
  g_assert (root_window == xwindow);

  for (i = 0; i < n_children; i++)
    add_new_window (root, children[i]);

  XFree (children);

  root->need_focus = TRUE;
  root_focus_top_window (root);

  return root;
}

void
lmc_root_get_size (LmcRoot *root,
		   int     *width,
		   int     *height)
{
  if (width)
    *width = root->width;
  if (height)
    *height = root->height;
}

GList *
lmc_root_get_toplevels (LmcRoot *root)
{
  return root->children;
}

int
lmc_root_get_screen (LmcRoot *root)
{
  return root->screen;
}

void
lmc_root_get_cursor (LmcRoot  *root,
		     LmcBits **bits,
		     int      *hot_x,
		     int      *hot_y)
{
  if (!root->cursor_bits)
    root_update_cursor (root);

  if (bits)
    *bits = root->cursor_bits;
  if (hot_x)
    *hot_x = root->cursor_hot_x;
  if (hot_y)
    *hot_y = root->cursor_hot_y;
}

LmcToplevel *
lmc_root_get_focus_toplevel (LmcRoot *root)
{
  return root->focus_toplevel;
}

LmcToplevel *
lmc_root_get_grab_focus_toplevel (LmcRoot *root)
{
  return root->grab_focus_toplevel;
}
