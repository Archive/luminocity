#include "display.h"
#include "output.h"
#include "root.h"
#include "gui.h"
#include "stdio.h"
#include "stdarg.h"
#include "stdlib.h"

static void
die (char *msg, ...)
{
  va_list vap;
  
  va_start (vap, msg);
  vfprintf (stderr, msg, vap);
  va_end (vap);

  exit (1);
}

static void
on_delete_window (LmcOutput  *output,
		  GMainLoop  *loop)
{
  g_main_loop_quit (loop);
}

static gboolean option_fullscreen = FALSE;
static gboolean option_update_root = FALSE;
static int option_n_desktops = 4;

static GOptionEntry entries[] = 
  {
    { 
      "fullscreen", 'f', 0, G_OPTION_ARG_NONE,
      &option_fullscreen, "Start in full screen mode", NULL
    },
    {
      "num-desktops", 'd', 0, G_OPTION_ARG_INT,
      &option_n_desktops, "Number of desktops", NULL
    },
    { 
      "update-root", 'u', 0, G_OPTION_ARG_NONE,
      &option_update_root, "Update the real root window", NULL
    },
    { NULL }
  };

int main (int argc, char **argv)
{
  Display *xsource, *xtarget;
  LmcDisplay *lsource, *ltarget;
  LmcRoot *root;
  LmcOutput *output;
  LmcGui *gui;
  GMainLoop *loop;
  int width, height;
  GError *error = NULL;
  GOptionContext *context;

  g_thread_init (NULL);

  XInitThreads ();

  context = g_option_context_new ("SOURCE_DISPLAY [BACKGROUND_IMAGE]");
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_parse (context, &argc, &argv, &error);

  if (error != NULL)
      die ("luminocity: %s\n", error->message);

  if (argc != 2 && argc != 3)
      die ("Usage: luminocity SOURCE_DISPLAY [BACKGROUND_IMAGE]\n");

  xsource = XOpenDisplay (argv[1]);
  if (!xsource)
    die ("Cannot open source display %s\n", XDisplayName (argv[1]));
  /* XSynchronize (xsource, 1); */

  xtarget = XOpenDisplay (NULL);
  if (!xtarget)
    die ("Cannot open target display %s\n", XDisplayName (NULL));

  option_n_desktops = CLAMP (option_n_desktops, 1, GUI_MAX_DESKTOPS);

  lsource = lmc_display_new (xsource);
  if (!lsource)
    return 1;
  
  lmc_display_attach (lsource, NULL);

  root = lmc_root_new (lsource, DefaultScreen (xsource),
		       option_update_root);
  if (!root)
    return 1;

  ltarget = lmc_display_new (xtarget);
  if (!ltarget)
    return 1;

  lmc_display_attach (ltarget, NULL);
  
  lmc_root_get_size (root, &width, &height);

  if (!(height % option_n_desktops == 0))
    die ("Height of source X server root window must be a multiple of %d\n",
	 option_n_desktops);
  
  height /= option_n_desktops;
  
  output = lmc_output_new (ltarget, DefaultScreen (xtarget),
			   width, height);

  gui = lmc_gui_new (root, output, option_n_desktops);
  if (!gui)
    return 1;

  if (argc == 3)
    lmc_gui_set_background (gui, argv[2]);
  
  loop = g_main_loop_new (NULL, FALSE);
  g_signal_connect (output, "delete-window", G_CALLBACK (on_delete_window), loop);

  XMapWindow (LMC_WINDOW_XDISPLAY (output), LMC_WINDOW_XWINDOW (output));

  if (option_fullscreen)
    lmc_gui_toggle_fullscreen (gui);

  g_main_loop_run (loop);

  lmc_gui_cleanup (gui);

  return 0;
}
