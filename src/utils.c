#include "utils.h"

GdkPixbuf *
lmc_load_datadir_pixbuf (const char *filename)
{
  GError *error = NULL;
  GdkPixbuf *pixbuf;
  char *path;
  
  if (g_file_test (filename, G_FILE_TEST_EXISTS))
    path = g_strdup (filename);
  else
    path = g_build_filename (PKGDATADIR, filename, NULL);

  pixbuf = gdk_pixbuf_new_from_file (path, &error);
  if (!pixbuf)
    {
      g_warning ("Unable to read '%s': %s\n",
		 filename, error->message);
      g_error_free (error);
    }

  g_free (path);

  return pixbuf;
}
